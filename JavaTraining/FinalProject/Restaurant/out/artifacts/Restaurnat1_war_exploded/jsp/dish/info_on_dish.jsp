<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
    <%@include file='../../css/style.css' %>
</style>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<html>
<head>
    <script src="../../js/jquery.min.js"type="text/javascript"></script>
    <script language="javascript" type="text/javascript" src="../../js/vtip.js"></script>
    <title><fmt:message key="menu.info.dish" bundle="${ rb }"/></title>
</head>
<body>
    <c:import url="/jsp/header/header.jsp"></c:import>
    <div id="wrapper">
        <h2><fmt:message key="id.id" bundle="${ rb }"/></h2>
        <div class="accordion">
            <div class="block">
                <span>${dish.get(0).id}</span>
            </div>
        </div>
        <h2><fmt:message key="menu.composition" bundle="${ rb }"/></h2>
        <div class="accordion">
            <div class="block">
                <span>${dish.get(0).composition}</span>
            </div>
        </div>
        <h2><fmt:message key="menu.price" bundle="${ rb }"/></h2>
        <div class="accordion">
            <div class="block">
                <span> ${dish.get(0).price}</span>
            </div>
        </div>
        <h2><fmt:message key="menu.weight" bundle="${ rb }"/></h2>
        <div class="accordion">
            <div class="block">
                <span> ${dish.get(0).weight}</span>
            </div>
        </div>
    </div>
    <img src="${dish.get(0).image}">
    <c:import url="/jsp/footer/footer.jsp"></c:import>
</body>
</html>
