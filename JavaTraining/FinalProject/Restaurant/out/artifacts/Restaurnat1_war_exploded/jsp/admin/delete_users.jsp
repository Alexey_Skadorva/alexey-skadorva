<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
    <%@include file='../../css/style.css' %>
</style>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<html>
<head>
    <title>  <fmt:message key="user.users" bundle="${ rb }"/></title>
</head>
<body>
    <c:import url="/jsp/header/header.jsp"></c:import>
    <form name="delete_users" method="POST" action="controller">
        <input type="hidden" name="command"class="form" value="delete_users" />
        <table class="check">
            <caption><fmt:message key="user.users" bundle="${ rb }"/></caption>
            <tr>
                <td><fmt:message key="admin.delete.delete" bundle="${ rb }"/></td>
                <td> <fmt:message key="id.id" bundle="${ rb }"/></td>
                <td><fmt:message key="user.name" bundle="${ rb }"/></td>
                <td><fmt:message key="user.role" bundle="${ rb }"/></td>
            </tr>
            <c:forEach var="i" begin="0" end="${size-1}">
                <c:choose>
                    <c:when test="${users.get(i).role=='user'}">
                        <tr>
                            <td> <input type="checkbox" name="users"  value="${users.get(i).id}" ></td>
                            <td><c:out value="${users.get(i).id}"/></td>
                            <td><c:out value="${users.get(i).name}"/></td>
                            <td><c:out value="${users.get(i).role}"/></td>
                        </tr>
                    </c:when>
                </c:choose>
            </c:forEach>
        </table>
        <input type="submit" class="button button-primary" value="<fmt:message key="admin.delete.users" bundle="${ rb }"/>">
    </form>
    <c:import url="/jsp/footer/footer.jsp"></c:import>
</body>
</html>
