<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<style>
    <%@include file='../../css/style.css' %>
</style>
<html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../css/style.css" media="all">
    <title><fmt:message key="login.registration" bundle="${ rb }"/></title>
</head>
<body>
    <c:import url="/jsp/header/header.jsp"></c:import>
    <form name="registration" method="POST" action="/controller">
        <input type="hidden" name="command" class="form" value="register" />
        <div class="main-signin">
            <div class="main-signin__head">
                <p><fmt:message key="login.registration" bundle="${ rb }"/></p>
            </div>
            <div class="main-signin__middle">
                <div class="middle__form">
                    <input type="text" class="form" name="login" placeholder="<fmt:message key="login.login" bundle="${ rb }"/>">
                    <input type="password" class="form" name="passwordfirst" placeholder="<fmt:message key="login.password" bundle="${ rb }"/>">
                    <input type="password" class="form" name="passwordsecond"  placeholder="<fmt:message key="login.repeat_password" bundle="${ rb }"/>">
                    <br/> ${errorRegistrationMessage}
                    <input type="submit" class="buttom" value="<fmt:message key="login.registration" bundle="${ rb }"/>"/>
                </div>
            </div>
        </div>
    </form>
    <c:import url="/jsp/footer/footer.jsp"></c:import>
</body>

</html>