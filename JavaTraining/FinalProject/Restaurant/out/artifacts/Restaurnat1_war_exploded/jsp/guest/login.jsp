<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
    <%@include file='../../css/style.css' %>
</style>
<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="login.login" bundle="${ rb }"/></title>
</head>
<body>
    <c:import url="/jsp/header/header.jsp"></c:import>
    <div class="main-signin">
        <div class="main-signin__head">
            <p><fmt:message key="login.autorisation" bundle="${ rb }"/></p>
        </div>
        <div class="main-signin__middle">
            <div class="middle__form">
                <form name="loginForm" method="POST" action="controller">
                    <input type="hidden" name="command"class="form" value="login" />
                    <input type="text"  name="login"  placeholder="<fmt:message key="login.login" bundle="${ rb }"/>">
                    <input type="text"  name="password" placeholder="<fmt:message key="login.password" bundle="${ rb }"/>">
                    <br/>${errorLoginPassMessage}
                    <br/>${successfulRegistration}
                    <input type="submit"class="buttom" value="<fmt:message key="login.enter" bundle="${ rb }"/>">
                </form>
                <form name="loginForm" method="POST" action="controller">
                    <input type="hidden" name="command"class="form" value="links" />
                    <input type="hidden" name="link"class="form" value="registration" />
                    <input type="submit"  value="<fmt:message key="login.registration" bundle="${ rb }"/>">
                </form>
            </div>
        </div>
    </div>
    <c:import url="/jsp/footer/footer.jsp"></c:import>
</body>
</html>