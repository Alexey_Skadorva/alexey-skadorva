<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
    <%@include file='../../css/style.css' %>
</style>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<html>
<head>
     <title><fmt:message key="order.order" bundle="${ rb }"/></title>
</head>
<body>
    <c:import url="/jsp/header/header.jsp"></c:import>
    <c:choose>
        <c:when test="${status == null}">
            <table class="check">
                <caption><fmt:message key="order.order" bundle="${ rb }"/></caption>
                <tr>
                    <th><fmt:message key="menu.name" bundle="${ rb }"/></th>
                    <th><fmt:message key="menu.price" bundle="${ rb }"/></th>
                </tr>
                <c:forEach var="i" begin="0" end="${sizeOfOrder-1}">
                <tr>
                    <td><a href="controller?command=info_of_dishes&id=${menu.get(i).id}"><c:out value="${menu.get(i).name}"/></a></td>
                    <th><c:out value="${menu.get(i).price}"/></th>
                </tr>
                </c:forEach>
                 <tr>
                    <td><c:out value="${menu.get(sizeOfOrder).name}"/></td>
                    <th><c:out value="${menu.get(i).price}"/></th>
                 </tr>
            </table>
            <hr>
            <div class="message">
                <span>${result}</span></br>
                <fmt:message key="order.number" bundle="${ rb }"/>:${idOfOrder}
            </div>
            <form name="pay_order" method="POST" action="controller"/>
                <input type="hidden" name="command"class="form" value="pay_order"/>
                <input type="hidden" name="id"class="form" value="${idOfOrder}"/>
                <input type="submit"class="button"  value="<fmt:message key="user.order.pay_order" bundle="${ rb }"/>">
            </form>
        </c:when>
    </c:choose>
    <c:choose>
         <c:when test="${status == 'paid'}">
             ${result}
            <form name="orders_of_user" method="POST" action="controller"/>
                <input type="hidden" name="command"class="form" value="orders_of_user" />
                <input type="submit"class="button"  value="<fmt:message key="admin.order.see_orders" bundle="${ rb }"/>">
            </form>
          </c:when>

    </c:choose>
    <c:choose>
        <c:when test="${status == 'not_paid'}">
            ${result}
            <form name="menu" method="POST" action="controller"/>
                <input type="hidden" name="command"class="form" value="menu" />
                <input type="submit"class="button"  value="<fmt:message key="user.order.new_order" bundle="${ rb }"/>">
            </form>
        </c:when>
    </c:choose>
    <c:import url="/jsp/footer/footer.jsp"></c:import>
</body>
</html>
