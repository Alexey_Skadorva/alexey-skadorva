<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<style>
    <%@include file='../../css/style.css' %>
</style>
<html>
<head>
    <title><fmt:message key="order.user" bundle="${ rb }"/></title>
</head>
<body>
    <c:import url="/jsp/header/header.jsp"></c:import>
    <table class="check">
        <caption><fmt:message key="order.all_orders" bundle="${ rb }"/></caption>
        <tr>
            <th><fmt:message key="menu.name" bundle="${ rb }"/></th>
            <th><fmt:message key="menu.price" bundle="${ rb }"/></th>
            <th><fmt:message key="order.status" bundle="${ rb }"/></th>
        </tr>
        <c:forEach var="i" begin="0" end="${size-1}">
            <tr>
                <td><c:out value="${orders.get(i).dishes}"/></td>
                <th><c:out value="${orders.get(i).price}"/></th>
                <c:if test="${orders.get(i).status==0}">
                    <th><a href="controller?command=pay_order&id=${orders.get(i).id}"><fmt:message key="user.order.pay_order" bundle="${ rb }"/></a></th>
                </c:if>
                <c:if test="${orders.get(i).status==1}">
                    <th><fmt:message key="user.order.wait" bundle="${ rb }"/></th>
                </c:if>
                <c:if test="${orders.get(i).status==2}">
                    <th><fmt:message key="user.order.end" bundle="${ rb }"/></th>
                </c:if>
            </tr>
        </c:forEach>
    </table>
    <hr>
    <c:import url="/jsp/footer/footer.jsp"></c:import>
</body>
</html>
