package by.bsu.mmf.session;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionListener implements HttpSessionListener {
    public void sessionCreated(HttpSessionEvent se){
        se.getSession().setAttribute("locale", "en");
    }
    public void sessionDestroyed(HttpSessionEvent se){
    }
}
