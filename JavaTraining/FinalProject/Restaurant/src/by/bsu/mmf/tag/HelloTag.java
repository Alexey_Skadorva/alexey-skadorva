package by.bsu.mmf.tag;

import by.bsu.mmf.manager.Message;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
@SuppressWarnings("serial")
public class HelloTag extends TagSupport {
    private String login;
    public void setLogin(String login) {
        this.login = login;
    }
    @Override
    public int doStartTag() throws JspException {
        HttpSession session= pageContext.getSession();
        String locale = session.getAttribute("locale").toString().toUpperCase();
        try {
            String message = null;
            if (login.isEmpty()) {
                message = Message.valueOf(locale).getMessage("message.welcome")+","+Message.valueOf(locale).getMessage("message.welcome.guest");
            } else {
                message = Message.valueOf(locale).getMessage("message.welcome")+"," + login;
            }
            pageContext.getOut().write(message + "<hr/>");
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }
}