package by.bsu.mmf.expression;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularExpression {
    public static boolean checkLogin(String login){
        final Pattern p = Pattern.compile("\\w{3,16}");
        Matcher m = p.matcher(login);
        return m.matches();
    }
    public static boolean checkPassword(String password){
        final Pattern p = Pattern.compile("\\w{6,16}");
        Matcher m = p.matcher(password);
        return m.matches();
    }
    public static boolean checkFormatOfInputNumber(String number){
        final Pattern p = Pattern.compile("\\d{1,20}");
        Matcher m = p.matcher(number);
        return m.matches();
    }

}
