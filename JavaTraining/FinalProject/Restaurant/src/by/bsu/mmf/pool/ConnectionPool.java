package by.bsu.mmf.pool;

import by.bsu.mmf.exception.ConnectionPoolException;
import by.bsu.mmf.manager.Database;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {
    private static Logger logger = Logger.getLogger(ConnectionPool.class);
    private final static int POOL_SIZE = Integer.parseInt(Database.getProperty("pool.size"));
    private static ConnectionPool pool = null;
    private static ReentrantLock lock = new ReentrantLock();
    private static AtomicBoolean poolCreated = new AtomicBoolean(false);
    private BlockingQueue<Connection> resources = new ArrayBlockingQueue<Connection>(POOL_SIZE);

    private ConnectionPool() throws ConnectionPoolException {
        String dbPath = Database.getProperty("db.path");
        String login =  Database.getProperty("db.login");
        String pass =  Database.getProperty("db.password");
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            for (int i = 0; i < POOL_SIZE; i++) {
                Connection cn = DriverManager.getConnection(dbPath, login, pass);
                resources.put(cn);
            }
        }catch (SQLException | InterruptedException e ) {
            throw new ExceptionInInitializerError(e);
        }
    }


    public void returnConnection(Connection cn) {
        try {
            resources.put(cn);
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }

    public Connection getConnection() {
        Connection cn = null;
        try {
            cn = resources.take();
        } catch (InterruptedException e) {
            logger.error(e);
        }
        return cn;
    }

    public  static ConnectionPool getPool() throws ConnectionPoolException{
        if(!poolCreated.get()) {
            lock.lock();
            try {
                if(pool == null) {
                    pool = new ConnectionPool();
                    poolCreated.getAndSet(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return pool;
    }

    public void closePool() throws ConnectionPoolException {
        Connection cn = null;
        try {
            for (int i = 0; i < POOL_SIZE; i++) {
                cn = resources.take();
                if(cn != null) {
                    cn.close();
                }
            }
        } catch (SQLException | InterruptedException e) {
            throw new ConnectionPoolException(e);
        }
    }
}
