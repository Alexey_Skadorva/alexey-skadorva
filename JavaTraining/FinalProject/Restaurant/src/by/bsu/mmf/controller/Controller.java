package by.bsu.mmf.controller;

import by.bsu.mmf.command.ActionFactory;
import by.bsu.mmf.command.Command;
import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.exception.ConnectionPoolException;
import by.bsu.mmf.manager.ConfigManager;
import by.bsu.mmf.manager.Message;
import by.bsu.mmf.pool.ConnectionPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import org.apache.log4j.Logger;

@WebServlet("/controller")
public class Controller extends HttpServlet {
    static Logger logger= Logger.getLogger(Controller.class);
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       processRequest(request, response);
    }
    @Override
    public void init() throws ServletException {
        super.init();
        String prefix = getServletContext().getRealPath("/");
        new DOMConfigurator().doConfigure(prefix + "/log4j.xml", LogManager.getLoggerRepository());
    }

    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page = null;
        Command command = ActionFactory.defineCommand(request);
       try {
           page = command.execute(request);
       } catch (CommandException e) {
           logger.error(e.getMessage(), e.getCause());
           request.setAttribute("exception",e.getCause());
           page = ConfigManager.getProperty("path.page.error");
       }
        if (page != null) {
            RequestDispatcher dispatcher = request.getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect(request.getContextPath() + ConfigManager.getProperty("path.page.index"));
        }
    }
    @Override
    public void destroy() {
        super.destroy();
        try {
            ConnectionPool.getPool().closePool();
        } catch (ConnectionPoolException e) {
            logger.error(e);
        }
    }
}
