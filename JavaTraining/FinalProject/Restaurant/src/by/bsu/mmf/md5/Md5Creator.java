package by.bsu.mmf.md5;

import org.apache.log4j.Logger;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5Creator {
    static Logger logger = Logger.getLogger(Md5Creator.class);
    public static String getMd5(String value) {
        String md5Str = null;
        try{
            MessageDigest md = MessageDigest.getInstance("md5");
            md.update(value.getBytes(), 0, value.length());
            md5Str = new BigInteger(1, md.digest()).toString(16);
        }
        catch(NoSuchAlgorithmException e) {
            logger.error(e);
        }
        return md5Str;
    }
}
