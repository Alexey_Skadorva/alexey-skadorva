package by.bsu.mmf.dao;

import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.info.Dish;
import by.bsu.mmf.info.Menu;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DishesDao extends AbstractDao {
    private static Connection cn;
    private final static String FIND_DISH_BY_ID = "Select * From informationofdishes Where idDish=?";
    private final static String FIND_NAME_BY_ID = "Select * From menu Where idDish=?";
    private final static String DELETE_DISH = "DELETE menu,informationofdishes FROM menu,informationofdishes WHERE menu.idDish=informationofdishes.idDish AND menu.idDish=?";
    private final static String OUTPUT_ALL_MENU = "SELECT * FROM  menu ";
    private final static String FIND_DISH_BY_PRICE = "Select * From menu Where price BETWEEN ? AND ?";
    private final static String FIND_DISH_BY_NAME = "Select * From menu Where nameOfDish=?";
    private final static String CHANGE_DISH = " UPDATE informationofdishes SET composition=?, price=?, weight=? WHERE idDish=?";
    private final static String CHANGE_PRICE = " UPDATE menu SET price=? WHERE idDish=?";
    private final static String CREATE_DISH_ON_MENU = "Insert into menu(nameOfDish, price,type) values(?,?,?)";
    private final static String CREATE_DISH_ON_INFO = "Insert into informationofdishes(composition, price,weight,type,image) values(?,?,?,?,?)";
    private final static String CHANGE_NAME = " UPDATE menu SET nameOfDish=? WHERE idDish=?";
    private final static String CHECK_FOR_POSSIBILITY_OF_REMOVING = "Select * From orders Where status=? ";

    public DishesDao(Connection cn) {
        this.cn = cn;
    }

    public DishesDao() {
    }

    public ArrayList<Menu> outputAllMenu() throws DAOException {
        PreparedStatement pst = null;
        ArrayList<Menu> menu = new ArrayList<>();
        try {
            pst = cn.prepareStatement(OUTPUT_ALL_MENU);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                menu.add(new Menu(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4)));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closePreparedStatement(pst);
        }
        return menu;
    }

    public ArrayList<Dish> outputInfoOfDish(int id) throws DAOException {
        PreparedStatement pst = null;
        ArrayList<Dish> dish = new ArrayList<>();
        try {
            pst = cn.prepareStatement(FIND_DISH_BY_ID);
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                dish.add(new Dish(id, rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getString(6)));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closePreparedStatement(pst);
        }
        return dish;

    }


    public String nameOfDish(int id) throws DAOException {
        PreparedStatement pst = null;
        String name = null;
        try {
            pst = cn.prepareStatement(FIND_NAME_BY_ID);
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                name = rs.getString(2);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closePreparedStatement(pst);
        }
        return name;

    }


    public void deleteDishes(ArrayList idOfDishes) throws DAOException {
        PreparedStatement pst = null;
        try {
            for (int i = 0; i < idOfDishes.size(); i++) {
                pst = cn.prepareStatement(DELETE_DISH);
                pst.setInt(1, (Integer) idOfDishes.get(i));
                pst.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closePreparedStatement(pst);
        }
    }

    public void changeDish(int id, String composition, int price, int weight) throws DAOException {
        PreparedStatement pst = null;
        try {
            pst = cn.prepareStatement(CHANGE_DISH);
            pst.setInt(4, id);
            pst.setString(1, composition);
            pst.setInt(2, price);
            pst.setInt(3, weight);
            pst.executeUpdate();
            pst = cn.prepareStatement(CHANGE_PRICE);
            pst.setInt(2, id);
            pst.setInt(1, price);
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closePreparedStatement(pst);
        }
    }

    public void changeName(String name, int id) throws DAOException {
        PreparedStatement pst = null;
        try {
            pst = cn.prepareStatement(CHANGE_NAME);
            pst.setInt(2, id);
            pst.setString(1, name);
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closePreparedStatement(pst);
        }
    }

    public ArrayList<Dish> findDishByName(String nameOfDish) throws DAOException {
        PreparedStatement pst = null;
        ArrayList<Dish> dish = new ArrayList<>();
        try {
            pst = cn.prepareStatement(FIND_DISH_BY_NAME);
            pst.setString(1, nameOfDish);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                dish.add(new Dish(rs.getInt(1), nameOfDish, rs.getInt(3), rs.getString(4)));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closePreparedStatement(pst);
        }
        return dish;

    }

    public ArrayList<Dish> findDishByPrice(int priceFrom, int priceTo) throws DAOException {
        PreparedStatement pst = null;
        ArrayList<Dish> dish = new ArrayList<>();
        try {
            pst = cn.prepareStatement(FIND_DISH_BY_PRICE);
            pst.setInt(1, priceFrom);
            pst.setInt(2, priceTo);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                dish.add(new Dish(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4)));
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closePreparedStatement(pst);
        }
        return dish;
    }

    public void addDish(String name, String composition, int price, int weight, String type, String image) throws DAOException {
        PreparedStatement pst = null;
        try {
            pst = cn.prepareStatement(CREATE_DISH_ON_MENU);
            pst.setString(1, name);
            pst.setInt(2, price);
            pst.setString(3, type);
            pst.executeUpdate();
            pst = cn.prepareStatement(CREATE_DISH_ON_INFO);
            pst.setString(1, composition);
            pst.setInt(2, price);
            pst.setInt(3, weight);
            pst.setString(4, type);
            pst.setString(5, image);
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closePreparedStatement(pst);
        }
    }

    public boolean checkForPossibilityOfRemoving(ArrayList idOfDishes) throws DAOException {
        PreparedStatement pst = null;
        try {
            pst = cn.prepareStatement(CHECK_FOR_POSSIBILITY_OF_REMOVING);
            int status = 1;
            pst.setInt(1, status);
            ResultSet rs = pst.executeQuery();
            String dishes =" ";
            while (rs.next()) {
                dishes += rs.getString(6) + " ";
            }
            String[] mass = dishes.split(" ");
            for (int i = 0; i < idOfDishes.size(); i++) {
                for (int j = 0; j < mass.length; j++) {
                    if (mass[j].equals(String.valueOf(idOfDishes.get(i)))){
                        return false;
                    }
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closePreparedStatement(pst);
        }
        return true;
    }
}
