package by.bsu.mmf.dao;

import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.info.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.log4j.Logger;

public class UserDao extends AbstractDao {
    public static Connection cn;
    private final static String FIND_USER_BY_LOGIN="Select * From users Where login=?";
    private final static String CHECK_USER_BY_LOGIN_AND_PASSWORD="Select * From users Where login=? AND password=?";
    private final static String All_USERS="SELECT * FROM  users ";
    private final static String CREATE_USER="Insert into users(login, password,role) values(?,?,?)";
    private final static String DELETE_USERS= "DELETE FROM users WHERE idUser=?";
    private final static String CHANGE_PASSWORD= " UPDATE users SET password=? WHERE idUser=?";
    private final static String CHANGE_LOGIN= " UPDATE users SET login=? WHERE idUser=?";

    public User checkUserByLoginAndPassword(String name,String pass) throws DAOException {
        User user = null;
        PreparedStatement pst=null;
        try{
             pst = cn.prepareStatement(CHECK_USER_BY_LOGIN_AND_PASSWORD);
            pst.setString(1, name);
            pst.setString(2, pass);
            ResultSet rs = pst.executeQuery();
            if(rs.next()) {
                user = new User(rs.getInt(1),rs.getString(2), rs.getString(3),rs.getString(4));
            }
        }catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(pst);
        }
        return user;
    }

    public boolean findUserByLogin(String name)  throws DAOException{
        PreparedStatement pst=null;
        try{
            pst = cn.prepareStatement(FIND_USER_BY_LOGIN);
            pst.setString(1, name);
            ResultSet rs = pst.executeQuery();
            if(rs.next()) {
                return true;
            }
        }catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(pst);
        }
        return false;
    }

    public void create(User user) throws DAOException{
        PreparedStatement pst = null;
        try{
            pst = cn.prepareStatement(CREATE_USER);
            pst.setString(1, user.getName());
            pst.setString(2, user.getPass());
            pst.setString(3, user.getRole());
            pst.executeUpdate();
        }catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(pst);
        }
    }

    public int info(String name)  throws DAOException{
        PreparedStatement pst = null;
        int id=0;
        try {
            pst = cn.prepareStatement(CREATE_USER);
            pst.setString(1, name);
            ResultSet rs = pst.executeQuery();
             id = rs.getInt(1);
         }catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(pst);
        }
        return id;
    }

    public ArrayList<User> allUsers() throws DAOException{
        PreparedStatement pst = null;
        ArrayList<User> users = new ArrayList<>();
        try{
            pst = cn.prepareStatement(All_USERS);
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
                users.add(new User( rs.getString(2),rs.getInt(1),rs.getString(4)));
            }

        }catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(pst);
        }
        return users;
    }

    public  void deleteUsers(ArrayList idOfUsers) throws DAOException {
        PreparedStatement pst = null;
        try{
        for (int i = 0; i < idOfUsers.size(); i++) {
            pst = cn.prepareStatement(DELETE_USERS);
            pst.setInt(1, (Integer) idOfUsers.get(i));
           pst.executeUpdate();
         }
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(pst);
        }
    }

    public   void changePassword(int id, String newPassword) throws DAOException{
        PreparedStatement pst = null;
        try {
            pst = cn.prepareStatement(CHANGE_PASSWORD);
            pst.setInt(2, id);
            pst.setString(1, newPassword);
            pst.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(pst);
        }
    }

    public  void changeLogin(int id, String newLogin) throws DAOException {
        PreparedStatement pst = null;
        try {
            pst = cn.prepareStatement(CHANGE_LOGIN);
            pst.setString(1, newLogin);
            pst.setInt(2, id);
            pst.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(pst);
        }

    }
    public UserDao(Connection cn) {
        this.cn = cn;
    }
}
