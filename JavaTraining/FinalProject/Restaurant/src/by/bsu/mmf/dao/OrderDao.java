package by.bsu.mmf.dao;

import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.info.Menu;
import by.bsu.mmf.info.Order;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class OrderDao extends AbstractDao{
    private static Connection cn;
    private final static String CALCULATION_PRICE = "Select * From menu Where idDish=? ";
    private final static String CREATE_ORDER = "Insert into orders(idUser, price,dishes,status,idDishes) values(?,?,?,?,?)";
    private final static String ORDERS_OF_USER="Select * From orders Where idUser=?";
    private final static String PAY_ORDER=" UPDATE orders SET status=? WHERE idOrder=?";
    private final static String SEE_ORDERS="Select * From orders Where status=? ";
    private final static String DISHES_IN_ORDER="Select * From orders Where idOrder=?";
    private final static String CHECK_AVAILABILITY_OF_DISHES="Select * From menu Where idDish=?";
    private final static String DELETE_ORDER= "DELETE FROM orders WHERE idOrder=?";
    private final static String LAST_ID = "SELECT LAST_INSERT_ID()";

    public  ArrayList<Menu> order(ArrayList orderedDishes) throws DAOException {
        PreparedStatement pst = null;
        int endPrice = 0;
        ArrayList<Menu> menu = new ArrayList<Menu>();
        try {
            int price;
            for (int i = 0; i < orderedDishes.size(); i++) {
                pst = cn.prepareStatement(CALCULATION_PRICE);
                pst.setInt(1, (Integer) orderedDishes.get(i));
                ResultSet rs = pst.executeQuery();
                if (rs.next()) {
                    price = rs.getInt(3);
                    menu.add(new Menu(rs.getInt(1),rs.getString(2), price));
                    endPrice += price;
                }
            }
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(pst);
        }
        menu.add(new Menu("Total",endPrice));
        return menu;
    }

    public  int createOrder(ArrayList<Menu> order, int price, int idOfUser, ArrayList orderedDishes)  throws DAOException {
        PreparedStatement pst = null;
        int status=0;
        int autoIncKeyFromApi = -1;
       String idDishes=orderedDishes.get(0)+" ";
       for(int i=1;i<orderedDishes.size();i++){
            idDishes+=orderedDishes.get(i)+" ";
        }
        try {
            pst = cn.prepareStatement(CREATE_ORDER);
            pst.setInt(1, idOfUser);
            pst.setInt(2, price);
            pst.setString(3, String.valueOf(order));
            pst.setInt(4,status);
            pst.setString(5, idDishes);
            pst.executeUpdate();
            pst = cn.prepareStatement(LAST_ID);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                autoIncKeyFromApi = rs.getInt(1);
            }
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(pst);
        }
        return autoIncKeyFromApi;
    }

    public ArrayList<Menu> ordersOfUser(int idOfUser) throws DAOException{
        PreparedStatement pst = null;
        ArrayList<Menu> orders = new ArrayList<>();
        try{
            pst = cn.prepareStatement(ORDERS_OF_USER);
            pst.setInt(1,idOfUser);
            ResultSet rs = pst.executeQuery();
               while(rs.next()) {
                   orders.add(new Menu(rs.getInt(1), rs.getInt(3),rs.getString(4),rs.getInt(5)));
               }
        }catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(pst);
        }
        return orders;
    }

    public   void payOrder(int idOfOrder) throws DAOException{
        PreparedStatement pst = null;
        int status=1;
        try{
            pst = cn.prepareStatement(PAY_ORDER);
            pst.setInt(2, idOfOrder);
            pst.setInt(1, status);
            pst.executeUpdate();
        }catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(pst);
        }
    }

    public  ArrayList<Order> seeOrders()  throws DAOException {
        PreparedStatement pst = null;
        ArrayList<Order> order = new ArrayList<>();
        try{
            int status=1;
            pst = cn.prepareStatement(SEE_ORDERS);
            pst.setInt(1, status);
            ResultSet rs = pst.executeQuery();
            while(rs.next()) {
                order.add(new Order(rs.getInt(1),rs.getInt(2),rs.getString(4),status,rs.getInt(3)));
            }
            status=2;
            pst = cn.prepareStatement(SEE_ORDERS);
            pst.setInt(1, status);
            ResultSet rss = pst.executeQuery();
            while(rss.next()) {
                order.add(new Order(rss.getInt(1),rss.getInt(2),rss.getString(4),status,rss.getInt(3)));
            }
        }catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(pst);
        }
        return order;
    }

    public void sendOrder(ArrayList orders) throws DAOException{
        PreparedStatement pst = null;
        int status=2;
        try{
            for (int i = 0; i < orders.size(); i++) {
                pst = cn.prepareStatement(PAY_ORDER);
                pst.setInt(2, (Integer) orders.get(i));
                pst.setInt(1, status);
                pst.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(pst);
        }
    }
    public boolean checkAvailabilityOfDishes(int idOfOrder) throws DAOException {
        PreparedStatement pst=null;
        try{
            pst = cn.prepareStatement(DISHES_IN_ORDER);
            pst.setInt(1, idOfOrder);
            ResultSet rs = pst.executeQuery();
            String dishes = null;
            if(rs.next()) {
                dishes=rs.getString(6);
            }
            String[] idOfDishes = dishes.split(" ");
            pst = cn.prepareStatement(CHECK_AVAILABILITY_OF_DISHES);
            int count=0;
            for(int i=0;i<idOfDishes.length;i++){
                pst.setInt(1,Integer.parseInt(idOfDishes[i]));
                ResultSet res = pst.executeQuery();
                if(!res.next()) {
                    pst = cn.prepareStatement(DELETE_ORDER);
                    pst.setInt(1, idOfOrder);
                    pst.executeUpdate();
                    return false;
                }
              }
        }catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(pst);
        }
        return true;
    }
    public OrderDao(Connection cn) {
        this.cn = cn;
    }

    }
