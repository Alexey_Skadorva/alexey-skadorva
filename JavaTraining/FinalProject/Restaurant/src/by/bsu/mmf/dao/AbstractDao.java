package by.bsu.mmf.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.apache.log4j.Logger;

public abstract class AbstractDao  {
    static Logger logger = Logger.getLogger(AbstractDao.class);
    public void closePreparedStatement(PreparedStatement ps)  {
        try {
            if(ps!=null) ps.close();
        } catch (SQLException e) {
            logger.error("SQLException: " + e);
        }
    }

}
