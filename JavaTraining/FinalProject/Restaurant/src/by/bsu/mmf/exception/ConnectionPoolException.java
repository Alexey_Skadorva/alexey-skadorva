package by.bsu.mmf.exception;

public class ConnectionPoolException extends Exception {
    public ConnectionPoolException(String message) {
        super(message);
    }
    public ConnectionPoolException(Throwable e) {
        super(e.getMessage(),e);
    }
    public ConnectionPoolException(String message,Throwable e) {
        super(message,e);
    }
}