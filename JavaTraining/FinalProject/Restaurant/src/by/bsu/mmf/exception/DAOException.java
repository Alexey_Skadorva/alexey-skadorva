package by.bsu.mmf.exception;

public class DAOException extends Exception{
    public DAOException(String message) {
        super(message);
    }
    public DAOException(Throwable e) {
        super(e.getMessage(),e);
    }
    public DAOException(String message,Throwable e) {
        super(message,e);
    }
}
