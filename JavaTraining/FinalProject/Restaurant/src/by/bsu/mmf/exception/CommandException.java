package by.bsu.mmf.exception;

public class CommandException extends  Exception{
    public CommandException(String message) {
        super(message);
    }
    public CommandException(Throwable e) {
        super(e.getMessage(),e);
    }
    public CommandException(String message, Throwable e) {
        super(message,e);
    }
}
