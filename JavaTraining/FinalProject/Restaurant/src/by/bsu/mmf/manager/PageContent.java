package by.bsu.mmf.manager;

import java.util.Locale;
import java.util.ResourceBundle;

public enum PageContent {
    EN( ResourceBundle.getBundle("resources.pagecontent_en", new Locale("en", "EN"))),
    RU(ResourceBundle.getBundle("resources.pagecontent_ru",new Locale("ru","RU")));
    private ResourceBundle bundle;
    public String getPagecontent(String key){
        return bundle.getString(key);
    }
    private PageContent(ResourceBundle bundle){
        this.bundle=bundle;
    }
}