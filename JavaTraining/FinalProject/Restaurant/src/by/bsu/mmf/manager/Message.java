package by.bsu.mmf.manager;

import java.util.Locale;
import java.util.ResourceBundle;


public enum Message {
    EN(ResourceBundle.getBundle("resources.message_en",new Locale("en","EN"))),
    RU(ResourceBundle.getBundle("resources.message_ru",new Locale("ru","RU")));
    private ResourceBundle bundle;
    public String getMessage(String key){
        return bundle.getString(key);
    }
    private Message(ResourceBundle bundle){
        this.bundle=bundle;
    }

}
