package by.bsu.mmf.manager;

import java.util.ResourceBundle;

public class Database {
    private static ResourceBundle bundle = ResourceBundle.getBundle("resources.database");
    private Database(){}
    public static String getProperty(String key){
        return bundle.getString(key);
    }
}
