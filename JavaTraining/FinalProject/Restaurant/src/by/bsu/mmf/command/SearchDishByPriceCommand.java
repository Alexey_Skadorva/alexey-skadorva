package by.bsu.mmf.command;

import by.bsu.mmf.dao.DishesDao;
import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.exception.ConnectionPoolException;
import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.info.Dish;
import by.bsu.mmf.manager.ConfigManager;
import by.bsu.mmf.manager.Message;
import by.bsu.mmf.pool.ConnectionPool;
import by.bsu.mmf.expression.RegularExpression;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.util.ArrayList;

public class SearchDishByPriceCommand implements Command {
    private static Logger logger = Logger.getLogger(SearchDishByPriceCommand.class);
    private static final String PRICE_FROM = "priceFrom";
     private static final String PRICE_TO = "priceTo";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        Connection connection = null;
        String priceFrom = request.getParameter(PRICE_FROM);
        String priceTo=request.getParameter(PRICE_TO);
        HttpSession session=request.getSession();
        String locale = session.getAttribute("locale").toString().toUpperCase();
        try {
            connection = ConnectionPool.getPool().getConnection();
            if (priceFrom == null || priceFrom.isEmpty()||priceTo == null || priceTo.isEmpty() ) {
                request.setAttribute("result", Message.valueOf(locale).getMessage("message.form.empty"));
            }
            else {
                if (RegularExpression.checkFormatOfInputNumber(priceFrom) && RegularExpression.checkFormatOfInputNumber(priceTo)) {
                    DishesDao dishesDao = new DishesDao(connection);
                    ArrayList<Dish> dishes = dishesDao.findDishByPrice(Integer.valueOf(priceFrom), Integer.valueOf(priceTo));
                    if (dishes.size() != 0) {
                        request.setAttribute("dishes", dishes);
                        request.setAttribute("size", dishes.size());
                    } else {
                        request.setAttribute("result", Message.valueOf(locale).getMessage("message.search.empty"));
                    }
                } else {
                    request.setAttribute("result", Message.valueOf(locale).getMessage("message.change.format.price"));
                }
            }
            page = ConfigManager.getProperty("path.page.guest.search_dishes");
        } catch (DAOException | ConnectionPoolException e) {
            throw new CommandException(e);
        }finally {
            try {
                if (connection != null) {
                    ConnectionPool.getPool().returnConnection(connection);
                }
            } catch (ConnectionPoolException e) {
                logger.error(e);
            }
        }
        return page;
    }
}