package by.bsu.mmf.command;

import by.bsu.mmf.dao.OrderDao;
import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.exception.ConnectionPoolException;
import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.info.Order;
import by.bsu.mmf.manager.ConfigManager;
import by.bsu.mmf.manager.Message;
import by.bsu.mmf.pool.ConnectionPool;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.util.ArrayList;

public class SendOrderCommand implements Command {
    private static Logger logger = Logger.getLogger(SendOrderCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        Connection connection = null;
        HttpSession session=request.getSession();
        String locale = session.getAttribute("locale").toString().toUpperCase();

        try {
            connection = ConnectionPool.getPool().getConnection();
            String[] orders = request.getParameterValues("orders");
            if(orders==null){
                request.setAttribute("result",  Message.valueOf(locale).getMessage("message.order.empty"));
                MenuCommand obj=new MenuCommand();
                obj.execute(request);
                page = ConfigManager.getProperty("path.page.main");
            }
            else {
                ArrayList idOfOrders = new ArrayList();
                for (String i : orders) {
                    idOfOrders.add(Integer.valueOf(i));
                }
                OrderDao orderDao = new OrderDao(connection);
                orderDao.sendOrder(idOfOrders);
                ArrayList<Order> order;
                order = orderDao.seeOrders();
                request.setAttribute("order", order);
                request.setAttribute("size", order.size());
                page = ConfigManager.getProperty("path.page.admin.see_orders");
            }
        } catch (DAOException | ConnectionPoolException e) {
            throw new CommandException(e);
        }finally {
            try {
                if (connection != null) {
                    ConnectionPool.getPool().returnConnection(connection);
                }
            } catch (ConnectionPoolException e) {
                logger.error(e);
            }
        }
        return page;
    }
}

