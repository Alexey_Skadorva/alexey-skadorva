package by.bsu.mmf.command;

import by.bsu.mmf.dao.DishesDao;
import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.exception.ConnectionPoolException;
import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.info.Menu;
import by.bsu.mmf.manager.ConfigManager;
import by.bsu.mmf.pool.ConnectionPool;
import by.bsu.mmf.manager.Message;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.util.ArrayList;
import org.apache.log4j.Logger;

public class DeleteDishesCommand implements Command {
    private static Logger logger = Logger.getLogger(DeleteDishesCommand.class);
    private static final String DISHES = "dishes";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Connection connection = null;
        String page = null;
        String[] dishesIds = request.getParameterValues(DISHES);
        HttpSession session=request.getSession();
        String locale = session.getAttribute("locale").toString().toUpperCase();
        try {
            connection = ConnectionPool.getPool().getConnection();
            ArrayList idOfDishes = new ArrayList();
            for (String i : dishesIds) {
                idOfDishes.add(Integer.valueOf(i));
            }
            DishesDao dishesDao = new DishesDao(connection);
            if(dishesDao.checkForPossibilityOfRemoving(idOfDishes)){
                dishesDao.deleteDishes(idOfDishes);
                request.setAttribute("result", Message.valueOf(locale).getMessage("message.delete.dishes.successful"));
           }else{
               request.setAttribute("result", Message.valueOf(locale).getMessage("message.delete.dishes.unsuccessful"));
            }
            ArrayList<Menu> menu = dishesDao.outputAllMenu();
            request.setAttribute("menu", menu);
            request.setAttribute("sizeOfMenu", menu.size());
            page = ConfigManager.getProperty("path.page.admin.change_menu");
        } catch (DAOException | ConnectionPoolException e) {
            throw new CommandException(e);
        }finally {
            try {
                if (connection != null) {
                    ConnectionPool.getPool().returnConnection(connection);
                }
            } catch (ConnectionPoolException e) {
                logger.error(e);
            }
        }
        return page;
    }


}
