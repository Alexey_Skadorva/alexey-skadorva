package by.bsu.mmf.command;
import by.bsu.mmf.dao.DishesDao;
import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.exception.ConnectionPoolException;
import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.manager.ConfigManager;
import by.bsu.mmf.manager.Message;
import by.bsu.mmf.pool.ConnectionPool;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;

public class ChangeNameOfDishCommand implements Command {
    private static Logger logger = Logger.getLogger(ChangeNameOfDishCommand.class);
    private static final String NEW_NAME = "name";
    private static final String ID_OF_DISH = "id";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        Connection connection = null;
        String name = request.getParameter(NEW_NAME);
        int id = Integer.valueOf(request.getParameter(ID_OF_DISH));
        HttpSession session=request.getSession();
        String locale = session.getAttribute("locale").toString().toUpperCase();
        try {
            connection = ConnectionPool.getPool().getConnection();
            if (name == null || name.isEmpty()) {
                request.setAttribute("result",  Message.valueOf(locale).getMessage("message.form.empty"));
            }
            else {
                DishesDao dishesDao = new DishesDao(connection);
                dishesDao.changeName(name, id);
                request.setAttribute("result",  Message.valueOf(locale).getMessage("message.successful"));
            }
            ChangeMenuCommand obj = new ChangeMenuCommand();
            obj.execute(request);
            page = ConfigManager.getProperty("path.page.admin.change_dish");
        }  catch (DAOException | ConnectionPoolException e) {
            throw new CommandException(e);
        }finally {
            try {
                if (connection != null) {
                    ConnectionPool.getPool().returnConnection(connection);
                }
            } catch (ConnectionPoolException e) {
                logger.error(e);
            }
        }
        return page;
    }
}
