package by.bsu.mmf.command;

import by.bsu.mmf.manager.ConfigManager;
import javax.servlet.http.HttpServletRequest;

public class EmptyCommand implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        return ConfigManager.getProperty("path.page.index");
    }
}
