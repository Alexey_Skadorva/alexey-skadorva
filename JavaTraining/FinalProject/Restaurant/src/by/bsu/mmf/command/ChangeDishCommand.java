package by.bsu.mmf.command;

import by.bsu.mmf.dao.DishesDao;
import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.exception.ConnectionPoolException;
import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.manager.ConfigManager;
import by.bsu.mmf.manager.Message;
import by.bsu.mmf.pool.ConnectionPool;
import by.bsu.mmf.expression.RegularExpression;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;

public class ChangeDishCommand implements Command {
    private static Logger logger = Logger.getLogger(ChangeDishCommand.class);
    private static final String NEW_COMPOSITION = "composition";
    private static final String NEW_PRICE = "price";
    private static final String NEW_WEIGHT = "weight";
    private static final String ID_OF_DISH = "id";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        Connection connection = null;
        int id = Integer.valueOf(request.getParameter(ID_OF_DISH));
        String composition = request.getParameter(NEW_COMPOSITION);
        String price = request.getParameter(NEW_PRICE);
        String weight = request.getParameter(NEW_WEIGHT);
        HttpSession session=request.getSession();
        String locale = session.getAttribute("locale").toString().toUpperCase();
        try {
            connection = ConnectionPool.getPool().getConnection();
            if (composition == null || composition.isEmpty() || price == null || price.isEmpty()||weight == null || weight.isEmpty()) {
                request.setAttribute("result", Message.valueOf(locale).getMessage("message.form.empty"));
            }
            else {
                if (RegularExpression.checkFormatOfInputNumber(price) && RegularExpression.checkFormatOfInputNumber(weight)) {
                    DishesDao dishesDao = new DishesDao();
                    dishesDao.changeDish(id, composition, Integer.valueOf(price), Integer.valueOf(weight));
                    request.setAttribute("result", Message.valueOf(locale).getMessage("message.successful"));
                } else {
                    request.setAttribute("result", Message.valueOf(locale).getMessage("message.change.format.price_weight"));
                }
            }
            ChangeMenuCommand obj = new ChangeMenuCommand();
            obj.execute(request);
            page = ConfigManager.getProperty("path.page.admin.change_dish");
        }  catch (DAOException | ConnectionPoolException e) {
            throw new CommandException(e);
        }finally {
            try {
                if (connection != null) {
                    ConnectionPool.getPool().returnConnection(connection);
                }
            } catch (ConnectionPoolException e) {
                logger.error(e);
            }
        }


        return page;
    }
}
