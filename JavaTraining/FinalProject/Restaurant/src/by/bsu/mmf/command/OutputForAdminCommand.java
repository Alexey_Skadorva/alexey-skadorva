package by.bsu.mmf.command;

import by.bsu.mmf.dao.DishesDao;
import by.bsu.mmf.dao.OrderDao;
import by.bsu.mmf.dao.UserDao;
import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.exception.ConnectionPoolException;
import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.info.Menu;
import by.bsu.mmf.info.Order;
import by.bsu.mmf.info.User;
import by.bsu.mmf.manager.ConfigManager;
import by.bsu.mmf.pool.ConnectionPool;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.util.ArrayList;

public class OutputForAdminCommand implements Command {
    private static Logger logger = Logger.getLogger(OutputForAdminCommand.class);
    private static final String LINK = "link";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        Connection connection = null;
        String deleteUsers="deleteUsers";
        String changeMenu="changeMenu";
        String seeOrders="seeOrders";
        String ref = request.getParameter(LINK);
        try {
            connection = ConnectionPool.getPool().getConnection();
            if(ref.equals(deleteUsers)){
                 UserDao userDao = new UserDao(connection);
                 ArrayList<User> users = userDao.allUsers();
                 request.setAttribute("users", users);
                 request.setAttribute("size", users.size());
                 page = ConfigManager.getProperty("path.page.admin.delete_users");

             }
            else
             if(ref.equals(changeMenu)){
                 DishesDao dishesDao = new DishesDao(connection);
                 ArrayList<Menu> menu = dishesDao.outputAllMenu();
                 request.setAttribute("menu", menu);
                 request.setAttribute("sizeOfMenu", menu.size());
                 page = ConfigManager.getProperty("path.page.admin.change_menu");

             }
            else  if(ref.equals(seeOrders)){
                 OrderDao orderDao = new OrderDao(connection);
                 ArrayList<Order> order = orderDao.seeOrders();
                 request.setAttribute("order", order);
                 request.setAttribute("size", order.size());
                 page = ConfigManager.getProperty("path.page.admin.see_orders");
             }
        } catch (DAOException | ConnectionPoolException e) {
            throw new CommandException(e);
        }finally {
            try {
                if (connection != null) {
                    ConnectionPool.getPool().returnConnection(connection);
                }
            } catch (ConnectionPoolException e) {
                logger.error(e);
            }
        }
        return page;
    }
}
