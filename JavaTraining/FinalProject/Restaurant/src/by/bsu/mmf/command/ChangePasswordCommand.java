package by.bsu.mmf.command;
import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.exception.ConnectionPoolException;
import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.expression.RegularExpression;
import by.bsu.mmf.dao.UserDao;
import by.bsu.mmf.logic.RegisterLogic;
import by.bsu.mmf.manager.ConfigManager;
import by.bsu.mmf.manager.Message;
import by.bsu.mmf.md5.Md5Creator;
import by.bsu.mmf.pool.ConnectionPool;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;

public class ChangePasswordCommand implements Command {
     private static Logger logger = Logger.getLogger(ChangePasswordCommand.class);
     private static final String NEW_PASSWORDFIRST = "newPasswordFirst";
     private static final String NEW_PASSWORDSECOND = "newPasswordSecond";

    @Override
     public String execute(HttpServletRequest request) throws CommandException {
         Connection connection = null;
         String page = null;
         HttpSession session=request.getSession();
         int  id= (int) session.getAttribute("idOfUser");
         String newPassFirst = request.getParameter(NEW_PASSWORDFIRST);
         String newPassSecond = request.getParameter(NEW_PASSWORDSECOND);
         String locale = session.getAttribute("locale").toString().toUpperCase();
         try {
             connection = ConnectionPool.getPool().getConnection();
             if (newPassFirst == null || newPassFirst.isEmpty()|| newPassSecond == null || newPassSecond.isEmpty()) {
                 request.setAttribute("result",Message.valueOf(locale).getMessage("message.login.empty"));
             }
             else {
                 if (RegisterLogic.passwordsIdentical(newPassFirst, newPassSecond)) {
                     if (RegularExpression.checkPassword(newPassFirst)) {
                         UserDao userDao = new UserDao(connection);
                         userDao.changePassword(id, Md5Creator.getMd5(newPassFirst));
                         request.setAttribute("result", Message.valueOf(locale).getMessage("message.successful"));
                     } else {
                         request.setAttribute("result", Message.valueOf(locale).getMessage("message.register.format"));
                     }
                 } else {
                     request.setAttribute("result", Message.valueOf(locale).getMessage("message.login.not_identical_pass"));
                 }
             }
             page = ConfigManager.getProperty("path.page.user.info_on_user");
         } catch (DAOException | ConnectionPoolException e) {
             throw new CommandException(e);
         }finally {
             try {
                 if (connection != null) {
                     ConnectionPool.getPool().returnConnection(connection);
                 }
             } catch (ConnectionPoolException e) {
                 logger.error(e);
             }
         }
         return page;
     }

 }