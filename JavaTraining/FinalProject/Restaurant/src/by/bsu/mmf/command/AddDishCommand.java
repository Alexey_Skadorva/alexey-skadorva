package by.bsu.mmf.command;

import by.bsu.mmf.dao.DishesDao;
import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.exception.ConnectionPoolException;
import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.manager.ConfigManager;
import by.bsu.mmf.manager.Message;
import by.bsu.mmf.pool.ConnectionPool;
import by.bsu.mmf.expression.RegularExpression;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;

public class AddDishCommand implements Command {
    private static Logger logger = Logger.getLogger(AddDishCommand.class);
    private static final String NAME = "name";
    private static final String TYPE = "type";
    private static final String COMPOSITION = "composition";
    private static final String PRICE = "price";
    private static final String WEIGHT = "weight";
    private static final String IMAGE = "image";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        Connection connection = null;
        String name = request.getParameter(NAME);
        String composition = request.getParameter(COMPOSITION);
        String price = request.getParameter(PRICE);
        String weight = request.getParameter(WEIGHT);
        String type = request.getParameter(TYPE);
        String image = request.getParameter(IMAGE);
        HttpSession session=request.getSession();
        String locale = session.getAttribute("locale").toString().toUpperCase();
        try {
            connection = ConnectionPool.getPool().getConnection();
            if (name == null || name.isEmpty() || composition == null || composition.isEmpty() || price == null || price.isEmpty()||weight == null || weight.isEmpty()
                    ||type == null || type.isEmpty()) {
                request.setAttribute("result",Message.valueOf(locale).getMessage("message.form.empty"));
            } else {
                if (RegularExpression.checkFormatOfInputNumber(price) && RegularExpression.checkFormatOfInputNumber(weight)) {
                    DishesDao dishesDao=new DishesDao();
                    dishesDao.addDish(name, composition, Integer.parseInt(price), Integer.parseInt(weight), type, image);
                    request.setAttribute("result",Message.valueOf(locale).getMessage("message.successful"));
                } else {
                    request.setAttribute("result", Message.valueOf(locale).getMessage("message.change.format.price_weight"));
                }
            }
            page = ConfigManager.getProperty("path.page.admin.add_dish");
        }  catch (DAOException | ConnectionPoolException e) {
            throw new CommandException(e);
        }finally {
            try {
                if (connection != null) {
                    ConnectionPool.getPool().returnConnection(connection);
                }
            } catch (ConnectionPoolException e) {
                logger.error(e);
            }
        }

        return page;
    }
}
