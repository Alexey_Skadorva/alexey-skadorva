package by.bsu.mmf.command;

import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.info.User;
import by.bsu.mmf.logic.LoginLogic;
import by.bsu.mmf.manager.ConfigManager;
import by.bsu.mmf.manager.Message;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LoginCommand implements Command {
    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_PASSWORD = "password";

    @Override
    public String execute(HttpServletRequest request) throws  CommandException {
        String page = null;
        String loginValue = request.getParameter(PARAM_LOGIN);
        String passValue = request.getParameter(PARAM_PASSWORD);
        HttpSession session=request.getSession();
        String locale = session.getAttribute("locale").toString().toUpperCase();
        if (loginValue == null || loginValue.isEmpty() || passValue == null || passValue.isEmpty()) {
             request.setAttribute("errorLoginPassMessage", Message.valueOf(locale).getMessage("message.login.empty"));
             page = ConfigManager.getProperty("path.page.guest.login");
        }
        else {
            User user = LoginLogic.isLoginAndPasswordUnique(loginValue, passValue);
            if (user != null) {
                session.setAttribute("idOfUser", user.getId());
                session.setAttribute("loginOfUser", user.getName());
                session.setAttribute("passwordOfUser", user.getPass());
                session.setAttribute("roleOfUser", user.getRole());
                MenuCommand obj = new MenuCommand();
                obj.execute(request);
                page = ConfigManager.getProperty("path.page.main");
            } else {
                request.setAttribute("errorLoginPassMessage", Message.valueOf(locale).getMessage("message.login.error"));
                page = ConfigManager.getProperty("path.page.guest.login");
            }
        }
        return page;
    }
}