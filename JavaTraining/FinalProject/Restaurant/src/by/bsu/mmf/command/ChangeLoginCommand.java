package by.bsu.mmf.command;

import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.exception.ConnectionPoolException;
import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.expression.RegularExpression;
import by.bsu.mmf.dao.UserDao;
import by.bsu.mmf.manager.ConfigManager;
import by.bsu.mmf.manager.Message;
import by.bsu.mmf.pool.ConnectionPool;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;

public class ChangeLoginCommand implements Command {
    private static Logger logger = Logger.getLogger(ChangeLoginCommand.class);
    private static final String NEW_LOGIN = "newLogin";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Connection connection = null;
        String page = null;
        HttpSession session=request.getSession();
        int  id= (int) session.getAttribute("idOfUser");
        String newLogin = request.getParameter(NEW_LOGIN);
        String locale = session.getAttribute("locale").toString().toUpperCase();
        try {
            connection = ConnectionPool.getPool().getConnection();
            if (newLogin == null || newLogin.isEmpty()) {
                request.setAttribute("result", Message.valueOf(locale).getMessage("message.login.empty"));
            }
            else {
                if (RegularExpression.checkLogin(newLogin)) {
                    UserDao userDao = new UserDao(connection);
                    if(!userDao.findUserByLogin(newLogin)) {
                        userDao.changeLogin(id, newLogin);
                        session.setAttribute("loginOfUser", newLogin);
                        request.setAttribute("result", Message.valueOf(locale).getMessage("message.successful"));
                    }
                    else{
                        request.setAttribute("result", Message.valueOf(locale).getMessage("message.login.exist"));
                    }
                } else {
                    request.setAttribute("result", Message.valueOf(locale).getMessage("message.register.format"));
                }
            }
            page = ConfigManager.getProperty("path.page.user.info_on_user");
        }  catch (DAOException | ConnectionPoolException e) {
            throw new CommandException(e);
        }finally {
            try {
                if (connection != null) {
                    ConnectionPool.getPool().returnConnection(connection);
                }
            } catch (ConnectionPoolException e) {
                logger.error(e);
            }
        }

        return page;
    }

}