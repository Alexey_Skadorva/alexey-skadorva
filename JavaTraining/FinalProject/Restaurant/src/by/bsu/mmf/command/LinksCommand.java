package by.bsu.mmf.command;

import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.manager.ConfigManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LinksCommand implements Command {
    private static final String PARAM_LOGIN = "link";
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String link = request.getParameter(PARAM_LOGIN);
           if(link.equals("registration")){
               return ConfigManager.getProperty("path.page.guest.registration");
           }
           if(link.equals("info")){
               return ConfigManager.getProperty("path.page.user.info_on_user");
           }
          if(link.equals("search")) {
              return ConfigManager.getProperty("path.page.guest.search_dishes");
           }
          if (link.equals("login")){
              return  ConfigManager.getProperty("path.page.index");
          }
           if (link.equals("add_dish")){
              return  ConfigManager.getProperty("path.page.admin.add_dish");
           }
        return null;
    }
}
