package by.bsu.mmf.command;

import by.bsu.mmf.dao.DishesDao;
import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.exception.ConnectionPoolException;
import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.info.Dish;
import by.bsu.mmf.manager.ConfigManager;
import by.bsu.mmf.pool.ConnectionPool;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.util.ArrayList;

public class ChangeMenuCommand implements Command {
    private static Logger logger = Logger.getLogger(ChangeMenuCommand.class);
    private static final String ID = "id";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Connection connection = null;
        String page = null;
        int id=Integer.valueOf(request.getParameter(ID));

        try {
            connection = ConnectionPool.getPool().getConnection();
            ArrayList<Dish> menu;
            DishesDao obj=new DishesDao();
            menu = obj.outputInfoOfDish(id);
            String name = obj.nameOfDish(id);
            request.setAttribute("name", name);
            request.setAttribute("menu", menu);
            page = ConfigManager.getProperty("path.page.admin.change_dish");
        }  catch (DAOException | ConnectionPoolException e) {
            throw new CommandException(e);
        }finally {
            try {
                if (connection != null) {
                    ConnectionPool.getPool().returnConnection(connection);
                }
            } catch (ConnectionPoolException e) {
                logger.error(e);
            }
        }

        return page;
    }

}