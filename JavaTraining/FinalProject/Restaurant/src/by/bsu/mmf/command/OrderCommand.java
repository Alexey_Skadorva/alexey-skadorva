package by.bsu.mmf.command;

import by.bsu.mmf.dao.OrderDao;
import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.exception.ConnectionPoolException;
import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.info.Menu;
import by.bsu.mmf.manager.ConfigManager;
import by.bsu.mmf.manager.Message;
import by.bsu.mmf.pool.ConnectionPool;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.util.ArrayList;

public class OrderCommand implements Command {
    private static Logger logger = Logger.getLogger(OrderCommand.class);
    private static final String DISHES = "dishes";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
         Connection connection = null;
         String page = null;
         String[] orderedDishes = request.getParameterValues(DISHES);
         HttpSession session=request.getSession();
         String locale = session.getAttribute("locale").toString().toUpperCase();
         try {
            connection = ConnectionPool.getPool().getConnection();
             if(orderedDishes==null){
                request.setAttribute("result",Message.valueOf(locale).getMessage("message.order.empty"));
                MenuCommand obj=new MenuCommand();
                obj.execute(request);
                page = ConfigManager.getProperty("path.page.main");
            }
            else {
                 ArrayList idOfDishes = new ArrayList();
                 for (String i : orderedDishes) {
                     idOfDishes.add(Integer.valueOf(i));
                 }
                OrderDao orderDao = new OrderDao(connection);
                ArrayList<Menu>  menu = orderDao.order(idOfDishes);
                int id = (int) session.getAttribute("idOfUser");
                int  idOfOrder=orderDao.createOrder(menu, menu.get(menu.size() - 1).getPrice(),id,idOfDishes);
                request.setAttribute("idOfOrder", idOfOrder);
                request.setAttribute("menu", menu);
                request.setAttribute("result", Message.valueOf(locale).getMessage("message.order.successful"));
                request.setAttribute("sizeOfOrder", idOfDishes.size());
                request.setAttribute("price", menu.get(menu.size() - 1).getPrice());
                page = ConfigManager.getProperty("path.page.user.order");
            }
            } catch (DAOException | ConnectionPoolException e) {
             throw new CommandException(e);
         }finally {
             try {
                 if (connection != null) {
                     ConnectionPool.getPool().returnConnection(connection);
                 }
             } catch (ConnectionPoolException e) {
                 logger.error(e);
             }
         }
        return page;
    }

}
