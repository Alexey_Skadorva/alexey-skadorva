package by.bsu.mmf.command;
import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.expression.RegularExpression;
import by.bsu.mmf.logic.RegisterLogic;
import by.bsu.mmf.manager.ConfigManager;
import by.bsu.mmf.manager.Message;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class RegisterCommand  implements Command {
    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_PASSWORD_FIRST = "passwordfirst";
    private static final String PARAM_PASSWORD_SECOND = "passwordsecond";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page=null;
        String loginValue = request.getParameter(PARAM_LOGIN);
        String passValueFirst = request.getParameter(PARAM_PASSWORD_FIRST);
        String passValueSecond = request.getParameter(PARAM_PASSWORD_SECOND);
        HttpSession session=request.getSession();
        String locale = session.getAttribute("locale").toString().toUpperCase();
        if (loginValue == null || loginValue.isEmpty() || passValueFirst == null || passValueFirst.isEmpty()
                || passValueSecond == null || passValueSecond.isEmpty()) {
            request.setAttribute("errorRegistrationMessage",  Message.valueOf(locale).getMessage("message.login_or_password.empty"));
            page = ConfigManager.getProperty("path.page.guest.registration");
        }
        else {
            if (RegularExpression.checkLogin(loginValue) & RegularExpression.checkPassword(passValueFirst))
                if (RegisterLogic.passwordsIdentical(passValueFirst, passValueSecond)) {
                    if (RegisterLogic.isLoginUnique(loginValue, passValueFirst)) {
                        request.setAttribute("successfulRegistration", Message.valueOf(locale).getMessage("message.register.success"));
                        page = ConfigManager.getProperty("path.page.guest.login");
                    } else {
                        request.setAttribute("errorRegistrationMessage", Message.valueOf(locale).getMessage("message.login.exist"));
                        page = ConfigManager.getProperty("path.page.guest.registration");
                    }
                } else {
                    request.setAttribute("errorRegistrationMessage", Message.valueOf(locale).getMessage("message.login.not_identical_pass"));
                    page = ConfigManager.getProperty("path.page.guest.registration");
                }
            else {
                request.setAttribute("errorRegistrationMessage", Message.valueOf(locale).getMessage("message.register.format"));
                page = ConfigManager.getProperty("path.page.guest.registration");
            }
        }
        return page;
    }
}
