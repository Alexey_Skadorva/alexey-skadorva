package by.bsu.mmf.command;

public enum CommandType {
    LOGIN(new LoginCommand()),
    LOGOUT(new LogoutCommand()),
    REGISTER(new RegisterCommand()),
    INFO_OF_DISHES(new InfoOfDishesCommand()),
    ORDER(new OrderCommand()),
    DELETE_USERS(new DeleteUsersCommand()),
    DELETE_DISHES(new DeleteDishesCommand()),
    CHANGE_MENU(new ChangeMenuCommand()),
    CHANGE_DISH(new ChangeDishCommand()),
    CHANGE_PASSWORD(new ChangePasswordCommand()),
    CHANGE_NAME(new ChangeNameOfDishCommand()),
    CHANGE_LOGIN(new ChangeLoginCommand()),
    OUTPUT_FOR_ADMIN(new OutputForAdminCommand()),
    PAY_ORDER (new PayOrderCommand()),
    SEND_ORDER(new SendOrderCommand()),
    MENU(new MenuCommand()),
    SEARCH_DISH_BY_NAME (new SearchDishByNameCommand()),
    SEARCH_DISH_BY_PRICE (new SearchDishByPriceCommand()),
    ADD_DISH (new AddDishCommand()),
    ORDERS_OF_USER(new OrdersOfUserCommand()),
    LINKS(new LinksCommand()),
    CHANGE_LOCALE(new ChangeLocaleCommand());

    public Command command;
    CommandType(Command command) {
        this.command = command;
    }
    public Command getCommand() {
        return command;
    }
}
