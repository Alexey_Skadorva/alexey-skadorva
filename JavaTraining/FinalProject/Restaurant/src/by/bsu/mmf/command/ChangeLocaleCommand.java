package by.bsu.mmf.command;

import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.manager.ConfigManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ChangeLocaleCommand implements Command {
    private static final String PARAM_LOCALE = "locale";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String localeValue = request.getParameter(PARAM_LOCALE);
        HttpSession session = request.getSession(true);
        session.setAttribute("locale", localeValue);
        MenuCommand obj=new MenuCommand();
        obj.execute(request);
        String page = ConfigManager.getProperty("path.page.main");
        return page;
    }
}
