package by.bsu.mmf.command;

import by.bsu.mmf.dao.DishesDao;
import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.exception.ConnectionPoolException;
import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.info.Menu;
import by.bsu.mmf.manager.ConfigManager;
import by.bsu.mmf.pool.ConnectionPool;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.util.ArrayList;

public class MenuCommand implements Command {
    private static Logger logger = Logger.getLogger(MenuCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        Connection connection = null;
        try {
            connection = ConnectionPool.getPool().getConnection();
            DishesDao dishesDao = new DishesDao(connection);
            ArrayList<Menu> menu = dishesDao.outputAllMenu();
            ArrayList<Menu> salads = new ArrayList<>();
            ArrayList<Menu> coldSnacks = new ArrayList<>();
            ArrayList<Menu> hotSnacks = new ArrayList<>();
            ArrayList<Menu> sups = new ArrayList<>();
            ArrayList<Menu> meat = new ArrayList<>();
            ArrayList<Menu> fish = new ArrayList<>();
            ArrayList<Menu> garnish = new ArrayList<>();
            ArrayList<Menu> sauce = new ArrayList<>();
            ArrayList<Menu> dessert = new ArrayList<>();
            for (Menu i : menu) {
                String type = i.type;
                switch (type) {
                    case "салат":
                        salads.add(i);
                        break;
                    case "холодная закуска":
                        coldSnacks.add(i);
                        break;
                    case "горячая закуска":
                        hotSnacks.add(i);
                        break;
                    case "суп":
                        sups.add(i);
                        break;
                    case "мясо":
                        meat.add(i);
                        break;
                    case "рыба":
                        fish.add(i);
                        break;
                    case "гарнир":
                        garnish.add(i);
                        break;
                    case "соус":
                        sauce.add(i);
                        break;
                    case "десерт":
                        dessert.add(i);
                        break;
                    default:
                        break;
                }
            }
                    request.setAttribute("salads", salads);
                    request.setAttribute("coldSnacks", coldSnacks);
                    request.setAttribute("hotSnacks", hotSnacks);
                    request.setAttribute("sups", sups);
                    request.setAttribute("meat", meat);
                    request.setAttribute("fish", fish);
                    request.setAttribute("garnish", garnish);
                    request.setAttribute("sauce", sauce);
                    request.setAttribute("dessert", dessert);

                    request.setAttribute("sizeOfSalads", salads.size());
                    request.setAttribute("sizeOfColdSnack", coldSnacks.size());
                    request.setAttribute("sizeOfHotSnack", hotSnacks.size());
                    request.setAttribute("sizeOfSup", sups.size());
                    request.setAttribute("sizeOfMeat", meat.size());
                    request.setAttribute("sizeOfFish", fish.size());
                    request.setAttribute("sizeOfGarnish", garnish.size());
                    request.setAttribute("sizeOfSauce", sauce.size());
                    request.setAttribute("sizeOfDessert", dessert.size());
                    request.setAttribute("menu", menu);
             page = ConfigManager.getProperty("path.page.main");
        } catch (DAOException | ConnectionPoolException e) {
            throw new CommandException(e);
        }finally {
            try {
                if (connection != null) {
                    ConnectionPool.getPool().returnConnection(connection);
                }
            } catch (ConnectionPoolException e) {
                logger.error(e);
            }
        }
        return page;
    }
}