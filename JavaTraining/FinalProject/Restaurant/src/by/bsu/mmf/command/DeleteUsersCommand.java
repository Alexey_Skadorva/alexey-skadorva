package by.bsu.mmf.command;

import by.bsu.mmf.dao.UserDao;
import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.exception.ConnectionPoolException;
import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.info.User;
import by.bsu.mmf.manager.ConfigManager;
import by.bsu.mmf.pool.ConnectionPool;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.util.ArrayList;
import org.apache.log4j.Logger;

public class DeleteUsersCommand implements Command {
    private static Logger logger = Logger.getLogger(DeleteUsersCommand.class);
    private static final String USERS = "users";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Connection connection = null;
        String page = null;
        String[] userIds = request.getParameterValues(USERS);
        try {
            connection = ConnectionPool.getPool().getConnection();
            ArrayList idOfUsers = new ArrayList();
            for (String i : userIds) {
                idOfUsers.add(Integer.valueOf(i));
            }
            UserDao userDao = new UserDao(connection);
            userDao.deleteUsers(idOfUsers);
            ArrayList<User> users = userDao.allUsers();
            request.setAttribute("users", users);
            request.setAttribute("size", users.size());
            page = ConfigManager.getProperty("path.page.admin.delete_users");
        } catch (DAOException | ConnectionPoolException e) {
            throw new CommandException(e);
        }finally {
            try {
                if (connection != null) {
                    ConnectionPool.getPool().returnConnection(connection);
                }
            } catch (ConnectionPoolException e) {
                logger.error(e);
            }
        }
        return page;
    }


}
