package by.bsu.mmf.command;

import by.bsu.mmf.dao.OrderDao;
import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.exception.ConnectionPoolException;
import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.manager.ConfigManager;
import by.bsu.mmf.manager.Message;
import by.bsu.mmf.pool.ConnectionPool;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;

public class PayOrderCommand implements Command {
    private static Logger logger = Logger.getLogger(PayOrderCommand.class);
    private static final String ID_OF_ORDER = "id";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        Connection connection = null;
        int idOfOrder =Integer.valueOf(request.getParameter(ID_OF_ORDER));
        HttpSession session=request.getSession();
        String locale = session.getAttribute("locale").toString().toUpperCase();
        try {
            connection = ConnectionPool.getPool().getConnection();
            OrderDao orderDao = new OrderDao(connection);
            if(orderDao.checkAvailabilityOfDishes(idOfOrder)) {
                orderDao.payOrder(idOfOrder);
                request.setAttribute("status", "paid");
                request.setAttribute("result", Message.valueOf(locale).getMessage("message.order.successful_pay"));
            }
            else{
                request.setAttribute("status", "not_paid");
                request.setAttribute("result", Message.valueOf(locale).getMessage("message.order.unsuccessful_pay"));
            }
            page = ConfigManager.getProperty("path.page.user.order");
        }catch (DAOException | ConnectionPoolException e) {
            throw new CommandException(e);
        }finally {
            try {
                if (connection != null) {
                    ConnectionPool.getPool().returnConnection(connection);
                }
            } catch (ConnectionPoolException e) {
                logger.error(e);
            }
        }
        return page;
    }
}
