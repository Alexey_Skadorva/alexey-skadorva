package by.bsu.mmf.logic;

import by.bsu.mmf.dao.UserDao;
import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.exception.ConnectionPoolException;
import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.info.User;
import by.bsu.mmf.manager.Message;
import by.bsu.mmf.md5.Md5Creator;
import by.bsu.mmf.pool.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.Connection;

public class RegisterLogic {
    private static Logger logger = Logger.getLogger(RegisterLogic.class);

    public static boolean isLoginUnique(String loginValue, String passValueFirst) throws CommandException {
        Connection connection = null;
        User user = null;
        try {
            connection = ConnectionPool.getPool().getConnection();
            UserDao userDao = new UserDao(connection);
            if (!userDao.findUserByLogin(loginValue)) {
                String role = "user";
                user = new User(loginValue, Md5Creator.getMd5(passValueFirst), role);
                userDao.create(user);
            }
        }
        catch (DAOException | ConnectionPoolException e) {
            throw new CommandException(e);
        }
        finally {
            try {
                if (connection != null) {
                    ConnectionPool.getPool().returnConnection(connection);
                }
            } catch (ConnectionPoolException e) {
                logger.error(e);
            }
        }
        return user!=null;
    }

    public static boolean passwordsIdentical(String passValueFirst,String passValueSecond)  {
        return passValueFirst.equals(passValueSecond);
    }
}
