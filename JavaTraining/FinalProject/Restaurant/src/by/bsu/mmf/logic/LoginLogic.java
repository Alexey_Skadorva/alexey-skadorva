package by.bsu.mmf.logic;

import by.bsu.mmf.dao.UserDao;
import by.bsu.mmf.exception.CommandException;
import by.bsu.mmf.exception.ConnectionPoolException;
import by.bsu.mmf.exception.DAOException;
import by.bsu.mmf.info.User;
import by.bsu.mmf.md5.Md5Creator;
import by.bsu.mmf.pool.ConnectionPool;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;

public class LoginLogic {
    private static Logger logger = Logger.getLogger(LoginLogic.class);

    public static User isLoginAndPasswordUnique(String loginValue, String passValue)  throws  CommandException{
        Connection connection = null;
         try {
             connection = ConnectionPool.getPool().getConnection();
             UserDao userDAO = new UserDao(connection);
             User user = userDAO.checkUserByLoginAndPassword(loginValue, Md5Creator.getMd5(passValue));
             return user;
         }
         catch (DAOException | ConnectionPoolException e) {
             throw new CommandException(e);
         }
         finally {
             try {
                 if (connection != null) {
                     ConnectionPool.getPool().returnConnection(connection);
                 }
             } catch (ConnectionPoolException e) {
                 logger.error(e);
             }
         }
    }
}
