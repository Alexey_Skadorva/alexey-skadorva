package by.bsu.mmf.info;

import java.lang.String;

public class User {
    private int id;
    private String name;
    private String pass;
    private String role;

    public User(int id,String name, String pass,String role) {
        this.id = id;
        this.name = name;
        this.pass = pass;
        this.role = role;
    }
    public User(String name, String pass,String role) {

        this.name = name;
        this.pass = pass;
        this.role = role;
    }
    public User( String name,int id,String role) {
        this.id = id;
        this.name = name;
        this.role = role;
    }
    public User( String name, String pass) {
        this.name = name;
        this.pass = pass;
    }




    public int getId() {return id; }
    public String getName() {
        return name;
    }
    public String getPass() {
        return pass;
    }
    public String getRole() {
        return role;
    }

    @Override
    public String toString() {
        return
                 id +
                 name + pass;
    }
}
