package by.bsu.mmf.info;

public class Order {
    private int idOrder;
    private int idUser;
    private int  status;
    private int price;
    private String dishes;

    public Order(int idOrder,int idUser, String dishes,int status,int price) {
        this.idOrder = idOrder;
        this.idUser = idUser;
        this.price = price;
        this.dishes = dishes;
        this.status = status;
    }

    public int getIdOrder() {
        return idOrder;
    }
    public int getIdUser() {
        return idUser;
    }
    public int getStatus() {
        return status;
    }
    public int getPrice() {
        return price;
    }
    public String getDishes() {
        return dishes;
    }

    @Override
    public String toString() {
        return
                String.valueOf(idOrder);
    }
}
