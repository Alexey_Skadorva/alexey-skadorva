package by.bsu.mmf.info;

public class Menu {
    private int id;
    private int price;
    private int status;
    private String name;
    private String totalprice;
    private String dishes;
    public String type;

    public Menu(int id, String name,int price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public Menu(int id, String name, int price, String type) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.type = type;

    }

    public Menu(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public Menu(int id, int price, String dishes,int status) {
        this.id = id;
        this.dishes = dishes;
        this.price = price;
        this.status = status;
    }

    public int getId() {
        return id;
    }
    public int getPrice() {
        return price;
    }
    public int getStatus() {
        return status;
    }
    public String getName() {
        return name;
    }
    public String getType() {
        return type;
    }
    public String getDishes() {
        return dishes;
    }


    @Override
    public String toString() {
        return name+" "+ price;
    }
}
