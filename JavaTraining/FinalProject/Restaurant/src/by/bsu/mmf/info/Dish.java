package by.bsu.mmf.info;

public class Dish {
    private int price;
    private int weight;
    private int id;
    private String type;
    private String composition;
    private String nameOfDish;
    private String image;

    public Dish(int id,String composition,int price,int weight) {
        this.id=id;
        this.composition = composition;
        this.price = price;
        this.weight=weight;
    }

    public Dish(int id, String nameOfDish, int price, String type) {
        this.id=id;
        this.nameOfDish = nameOfDish;
        this.price = price;
        this.type=type;
    }

    public Dish(int id, String composition, int price, int weight, String image){
        this.id=id;
        this.composition = composition;
        this.price = price;
        this.weight=weight;
        this.image=image;
    }

    public int getId() {
        return id;
    }
    public int getPrice() {
        return price;
    }
    public int getWeight() {
        return weight;
    }
    public String getName() { return nameOfDish;}
    public String getType() {return type;}
    public String getComposition() {return composition;}
    public String getImage() {return image;}

    @Override
    public String toString() {
        return id+" "+
               composition+" "+price+" "+weight ;
    }
}
