<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
    <%@include file='../../css/style.css' %>
</style>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>

<html>
<head>
    <title><fmt:message key="guest.search.search" bundle="${ rb }"/></title>
</head>
<body>
    <c:import url="/jsp/header/header.jsp"></c:import>
    <div class="formsAndButtons"></div>
        <form name="search_dish_by_name"  id="search" method="POST" action="controller">
            <input type="hidden"  name="command"class="form" value="search_dish_by_name" />
            <input type="text"   name="nameofdish"  placeholder="<fmt:message key="guest.search.name" bundle="${ rb }"/>">
            <input type="submit" class="button button-primary"  value="<fmt:message key="guest.search.search_by_name" bundle="${ rb }"/>">
        </form>
    </div>
    <form name="loginForm" id="search"  method="POST" action="controller">
        <input type="hidden" name="command"class="form" value="search_dish_by_price" />
        <input type="text"  name="priceFrom"  placeholder="<fmt:message key="guest.search.price_from" bundle="${ rb }"/>">
        <input type="text"  name="priceTo"  placeholder="<fmt:message key="guest.search.price_to" bundle="${ rb }"/>">
        <input type="submit" class="button button-primary"value="<fmt:message key="guest.search.search_dish_by_price" bundle="${ rb }"/>">
    </form>
    ${result}
    <c:choose>
        <c:when test="${dishes!=null}">
            <table class="check" >
                <caption><fmt:message key="menu.dishes" bundle="${ rb }"/></caption>
                <tr>
                    <th><fmt:message key="id.id" bundle="${ rb }"/></th>
                    <th><fmt:message key="menu.name" bundle="${ rb }"/></th>
                    <th><fmt:message key="menu.price" bundle="${ rb }"/></th>
                    <th><fmt:message key="menu.type" bundle="${ rb }"/></th>
                </tr>
            <c:forEach var="i" begin="0" end="${size-1}">
                <tr>
                    <td><c:out value="${dishes.get(i).id}"/></td>
                    <td><a href="controller?command=info_of_dishes&id=${dishes.get(i).id}"><c:out value="${dishes.get(i).name}"/></a></td>
                    <td><c:out value="${dishes.get(i).price}"/></td>
                    <td><c:out value="${dishes.get(i).type}"/></td>
                </tr>
            </c:forEach>
            </table>
        </c:when>
    </c:choose>
    <hr>
    <c:import url="/jsp/footer/footer.jsp"></c:import>
</body>
</html>
