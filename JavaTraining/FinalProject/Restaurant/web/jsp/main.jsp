<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
    <%@include file='../css/style.css' %>
</style>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="main.main" bundle="${ rb }"/></title>
</head>
<body>
    <c:import url="/jsp/header/header.jsp"></c:import>
    <ctg:hello login="${loginOfUser}"/>
    <form name="loginForm" method="POST" action="controller">
        <input type="hidden" name="command"class="form" value="order"/>
        <table>
            <caption><fmt:message key="menu.type.salads" bundle="${ rb }"/></caption>
            <tr>
                <c:if test="${roleOfUser=='user'}"><th></th></c:if>
                <th><fmt:message key="menu.name" bundle="${ rb }"/></th>
                <th><fmt:message key="menu.price" bundle="${ rb }"/></th>
            </tr>
            <c:forEach var="i" begin="0" end="${sizeOfSalads-1}">
            <tr>
                <c:if test="${roleOfUser=='user'}">
                    <td><input type="checkbox" name="dishes" value="${salads.get(i).id}" ></td>
                </c:if>
                <td><a href="controller?command=info_of_dishes&id=${salads.get(i).id}"><c:out value="${salads.get(i).name}"/></a></td>
                <th><c:out value="${salads.get(i).price}"/></th>
            </tr>
            </c:forEach>
        </table>
        <table>
            <caption><fmt:message key="menu.type.cold_snacks" bundle="${ rb }"/></caption>
            <tr>
                <c:if test="${roleOfUser=='user'}"><th></th></c:if>
                <th><fmt:message key="menu.name" bundle="${ rb }"/></th>
                <th> <fmt:message key="menu.price" bundle="${ rb }"/></th>
            </tr>
            <c:forEach var="i" begin="0" end="${sizeOfColdSnack-1}">
                <tr>
                    <c:if test="${roleOfUser=='user'}">
                        <td><input type="checkbox" name="dishes" value="${coldSnacks.get(i).id}" ></td>
                    </c:if>
                    <td><a href="controller?command=info_of_dishes&id=${coldSnacks.get(i).id}"><c:out value="${coldSnacks.get(i).name}"/></a></td>
                    <th> <c:out value="${coldSnacks.get(i).price}"/> </th>
                </tr>
            </c:forEach>
        </table>
        <table>
            <caption><fmt:message key="menu.type.hot_snacks" bundle="${ rb }"/></caption>
            <tr>
                <c:if test="${roleOfUser=='user'}"><th></th></c:if>
                <th><fmt:message key="menu.name" bundle="${ rb }"/></th>
                <th> <fmt:message key="menu.price" bundle="${ rb }"/></th>
            </tr>
            <c:forEach var="i" begin="0" end="${sizeOfHotSnack-1}">
                <tr>
                    <c:if test="${roleOfUser=='user'}">
                        <td><input type="checkbox" name="dishes" value="${hotSnacks.get(i).id}" ></td>
                    </c:if>
                    <td><a href="controller?command=info_of_dishes&id=${hotSnacks.get(i).id}"><c:out value="${hotSnacks.get(i).name}"/></a></td>
                    <th><c:out value="${hotSnacks.get(i).price}"/> </th>
                </tr>
            </c:forEach>
        </table>
        <hr>
        <table>
            <caption><fmt:message key="menu.type.sups" bundle="${ rb }"/></caption>
            <tr>
                <c:if test="${roleOfUser=='user'}"><th></th></c:if>
                <th><fmt:message key="menu.name" bundle="${ rb }"/></th>
                <th><fmt:message key="menu.price" bundle="${ rb }"/></th>
            </tr>
            <c:forEach var="i" begin="0" end="${sizeOfSup-1}">
                <tr>
                    <c:if test="${roleOfUser=='user'}">
                        <td><input type="checkbox" name="dishes" value="${sups.get(i).id}" ></td>
                    </c:if>
                    <td><a href="controller?command=info_of_dishes&id=${sups.get(i).id}"><c:out value="${sups.get(i).name}"/></a></td>
                    <th><c:out value="${sups.get(i).price}"/> </th>
                </tr>
            </c:forEach>
        </table>
        <table >
            <caption><fmt:message key="menu.type.fish" bundle="${ rb }"/></caption>
            <tr>
                <c:if test="${roleOfUser=='user'}"><th></th></c:if>
                <th><fmt:message key="menu.name" bundle="${ rb }"/></th><th> <fmt:message key="menu.price" bundle="${ rb }"/></th>
            </tr>
            <c:forEach var="i" begin="0" end="${sizeOfFish-1}">
                <tr>
                    <c:if test="${roleOfUser=='user'}">
                        <td><input type="checkbox" name="dishes" value="${fish.get(i).id}" ></td>
                    </c:if>
                    <td><a href="controller?command=info_of_dishes&id=${fish.get(i).id}"><c:out value="${fish.get(i).name}"/></a></td>
                    <th> <c:out value="${fish.get(i).price}"/></th>
                </tr>
            </c:forEach>
        </table>
        <hr>
        <table >
            <caption><fmt:message key="menu.type.meat" bundle="${ rb }"/></caption>
            <tr>
                <c:if test="${roleOfUser=='user'}"><th></th></c:if>
                <th> <fmt:message key="menu.name" bundle="${ rb }"/></th>
                <th> <fmt:message key="menu.price" bundle="${ rb }"/></th>
            </tr>
            <c:forEach var="i" begin="0" end="${sizeOfMeat-1}">
                <tr>
                    <c:if test="${roleOfUser=='user'}">
                        <td> <input type="checkbox" name="dishes" value="${meat.get(i).id}" ></td>
                    </c:if>
                    <td><a href="controller?command=info_of_dishes&id=${meat.get(i).id}"><c:out value="${meat.get(i).name}"/></a></td>
                    <th> <c:out value="${meat.get(i).price}"/></th>
                </tr>
            </c:forEach>
        </table>
        <table >
            <caption><fmt:message key="menu.type.garnishes" bundle="${ rb }"/></caption>
            <tr>
                <c:if test="${roleOfUser=='user'}"><th></th></c:if>
                <th> <fmt:message key="menu.name" bundle="${ rb }"/></th>
                <th> <fmt:message key="menu.price" bundle="${ rb }"/></th>
            </tr>
            <c:forEach var="i" begin="0" end="${sizeOfGarnish-1}">
                <tr>
                    <c:if test="${roleOfUser=='user'}">
                        <td> <input type="checkbox" name="dishes" value="${garnish.get(i).id}" ></td>
                    </c:if>
                    <td><a href="controller?command=info_of_dishes&id=${garnish.get(i).id}"><c:out value="${garnish.get(i).name}"/></a></td>
                    <th> <c:out value="${garnish.get(i).price}"/></th>
                </tr>
            </c:forEach>
        </table>
        <hr>
        <table>
            <caption><fmt:message key="menu.type.souces" bundle="${ rb }"/></caption>
            <tr>
                <c:if test="${roleOfUser=='user'}"><th></th></c:if>
                <th><fmt:message key="menu.name" bundle="${ rb }"/></th>
                <th> <fmt:message key="menu.price" bundle="${ rb }"/></th>
            </tr>
            <c:forEach var="i" begin="0" end="${sizeOfSauce-1}">
                <tr>
                    <c:if test="${roleOfUser=='user'}">
                        <td> <input type="checkbox" name="dishes" value="${sauce.get(i).id}" ></td>
                    </c:if>
                    <td><a href="controller?command=info_of_dishes&id=${sauce.get(i).id}"><c:out value="${sauce.get(i).name}"/></a></td>
                    <th> <c:out value="${sauce.get(i).price}"/></th>
                </tr>
            </c:forEach>
        </table>
        <table>
            <caption><fmt:message key="menu.type.desserts" bundle="${ rb }"/></caption>
            <tr><c:if test="${roleOfUser=='user'}"><th></th></c:if>
                <th> <fmt:message key="menu.name" bundle="${ rb }"/></th>
                <th> <fmt:message key="menu.price" bundle="${ rb }"/></th>
            </tr>
            <c:forEach var="i" begin="0" end="${sizeOfDessert-1}">
                <tr>
                    <c:if test="${roleOfUser=='user'}">
                        <td><input type="checkbox" name="dishes" value="${dessert.get(i).id}" ></td>
                    </c:if>
                    <td><a href="controller?command=info_of_dishes&id=${dessert.get(i).id}"><c:out value="${dessert.get(i).name}"/></a></td>
                    <th> <c:out value="${dessert.get(i).price}"/></th>
                </tr>
            </c:forEach>
        </table>
        <hr>
       ${emptyOrder}
        <c:if test="${roleOfUser=='user'}"><input type="submit"class="button" value="<fmt:message key="order.order" bundle="${ rb }"/>"></c:if>
    </form>
<c:import url="/jsp/footer/footer.jsp"></c:import>
</body>
</html>