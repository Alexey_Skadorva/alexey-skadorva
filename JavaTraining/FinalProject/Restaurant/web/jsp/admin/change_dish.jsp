<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
    <%@include file='../../css/style.css' %>
</style>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/><html>
<html>
<head>
    <script src="js/jquery.min.js"type="text/javascript"></script>
    <script language="javascript" type="text/javascript" src="js/vtip.js"></script>
    <title> <fmt:message key="order.dishes" bundle="${ rb }"/></title>
</head>
<body>
    <c:import url="/jsp/header/header.jsp"></c:import>
    <form name="change_name" id="search"  method="POST" action="controller">
        <input type="hidden" name="command"class="form" value="change_name">
        <div id="wrapper">
            <h2><fmt:message key="id.id" bundle="${ rb }"/></h2>
            <div class="accordion">
                <div class="block">
                    <span> ${menu.get(0).id}</span>
                </div>
            </div>
            <h2><fmt:message key="menu.name" bundle="${ rb }"/></h2>
            <div class="accordion">
                <div class="block">
                    <span> ${name}</span>
                </div>
            </div>
           </div>
        <input type="text"  name="name"  placeholder="<fmt:message key="admin.change.name" bundle="${ rb }"/>">
        <input type="hidden" name="id"  value="${menu.get(0).id}">
        <input type="submit" class="button button-primary"  value="<fmt:message key="admin.change.change" bundle="${ rb }"/>">
    </form>
    ${emptyname}
    <div id="wrapper">
        <h2><fmt:message key="menu.composition" bundle="${ rb }"/></h2>
        <div class="accordion">
            <div class="block">
                <span> ${menu.get(0).composition}</span>
            </div>
        </div>
        <h2><fmt:message key="menu.price" bundle="${ rb }"/></h2>
        <div class="accordion">
            <div class="block">
                <span>  ${menu.get(0).price}</span>
            </div>
        </div>
        <h2><fmt:message key="menu.weight" bundle="${ rb }"/></h2>
        <div class="accordion">
            <div class="block">
                <span>  ${menu.get(0).weight}</span>
            </div>
        </div>
    </div>
    <form name="change_dish" id="search"  method="POST" action="controller">
        <input type="hidden" name="command"class="form" value="change_dish">
        <input type="hidden" name="id"  value="${menu.get(0).id}">
        <input type="text"  name="composition"  placeholder="<fmt:message key="admin.change.composition" bundle="${ rb }"/>">
        <input type="text"  name="price"  placeholder="<fmt:message key="admin.change.price" bundle="${ rb }"/>">
        <input type="text"  name="weight"  placeholder="<fmt:message key="admin.change.weight" bundle="${ rb }"/>">
        <input type="submit"class="button button-primary"  value="<fmt:message key="admin.change.change" bundle="${ rb }"/>">
    </form>
    ${result}
    <c:import url="/jsp/footer/footer.jsp"></c:import>
</body>
</html>
