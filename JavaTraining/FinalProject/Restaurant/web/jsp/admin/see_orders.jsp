<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
    <%@include file='../../css/style.css' %>
</style>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/><html>
<html>
<head>
    <title><fmt:message key="order.orders" bundle="${ rb }"/></title>
</head>
<body>
    <c:import url="/jsp/header/header.jsp"></c:import>
    <form name="send_order" method="POST" action="controller">
        <input type="hidden" name="command"class="form" value="send_order" />
        <table class="orders">
            <caption> <fmt:message key="order.all_orders" bundle="${ rb }"/></caption>
            <tr>
                <th><fmt:message key="user.order.send" bundle="${ rb }"/></th>
                <th><fmt:message key="id.id_of_order" bundle="${ rb }"/></th>
                <th> <fmt:message key="id.id_of_user" bundle="${ rb }"/></th>
                <th><fmt:message key="menu.price" bundle="${ rb }"/></th>
                <th><fmt:message key="order.dishes" bundle="${ rb }"/></th>
            </tr>
            <c:forEach var="i" begin="0" end="${size-1}">
                <tr>
                    <c:if test="${order.get(i).status == 1}">
                        <th><input type="checkbox" name="orders" value="${order.get(i).idOrder}" ></th>
                    </c:if>
                     <c:if test="${order.get(i).status == 2}">
                        <th><fmt:message key="user.order.end" bundle="${ rb }"/></th>
                     </c:if>
                    <th><c:out value="${order.get(i).idOrder}"/></th>
                    <th><c:out value="${order.get(i).idUser}"/></th>
                    <th><c:out value="${order.get(i).price}"/></th>
                    <th><c:out value="${order.get(i).dishes}"/></th>
                </tr>
            </c:forEach>
        </table>
        <input type="submit" class="button button-primary" value="<fmt:message key="user.order.send" bundle="${ rb }"/>">
    </form>
    ${result}
    <c:import url="/jsp/footer/footer.jsp"></c:import>
</body>
</html>
