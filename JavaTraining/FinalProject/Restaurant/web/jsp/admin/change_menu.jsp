<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
    <%@include file='../../css/style.css' %>
</style>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/><html>
<html>
<head>
    <title> <fmt:message key="admin.change.menu" bundle="${ rb }"/></title>
</head>
<body>
    <c:import url="/jsp/header/header.jsp"></c:import>
    <form name="delete_dishes" method="POST" action="controller">
        <input type="hidden" name="command"class="form" value="delete_dishes" />
        <table class="check">
            <caption> <fmt:message key="menu.dishes" bundle="${ rb }"/></caption>
            <tr>
                <th> <fmt:message key="id.id" bundle="${ rb }"/></th>
                <th> <fmt:message key="menu.name" bundle="${ rb }"/></th>
            </tr>
            <c:forEach var="i" begin="0" end="${sizeOfMenu-1}">
                <tr>
                    <td><c:out value="${menu.get(i).id}"/></td>
                    <td>
                        <input type="checkbox" name="dishes" value="${menu.get(i).id}">
                        <a href="controller?command=change_menu&id=${menu.get(i).id}"><c:out value="${menu.get(i).name}"/></a>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <input type="submit"class="button button-primary" value="<fmt:message key="admin.delete.dishes" bundle="${ rb }"/>">
    </form>
    <form name="links" method="POST" action="controller">
        <input type="hidden" name="command"class="form" value="links" />
        <input type="hidden" name="link"class="form" value="add_dish" />
        <input type="submit" class="button button-primary"  value="<fmt:message key="admin.add.dish" bundle="${ rb }"/>">
    </form>
    ${result}
    <c:import url="/jsp/footer/footer.jsp"></c:import>
</body>
</html>
