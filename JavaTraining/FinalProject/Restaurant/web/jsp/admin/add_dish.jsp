<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
    <%@include file='../../css/style.css' %>
</style>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/><html>
<html>
<head>
    <title> <fmt:message key="order.dishes" bundle="${ rb }"/></title>
</head>
<body>
    <c:import url="/jsp/header/header.jsp"></c:import>
    <form name="add_dish" id="search"  method="POST" action="controller">
        <input type="hidden" name="command"class="form" value="add_dish">
        <input type="text"  name="name"  placeholder="<fmt:message key="menu.name" bundle="${ rb }"/>">
        <input type="text"  name="composition"  placeholder="<fmt:message key="menu.composition" bundle="${ rb }"/>">
        <input type="text"  name="price"  placeholder="<fmt:message key="menu.price" bundle="${ rb }"/>">
        <input type="text"  name="weight"  placeholder="<fmt:message key="menu.weight" bundle="${ rb }"/>">
        <input type="text"  name="type"  placeholder="<fmt:message key="menu.type" bundle="${ rb }"/>">
        <input type="text"  name="image"  placeholder="<fmt:message key="menu.image" bundle="${ rb }"/>">
        <input type="submit" class="button button-primary"  value="<fmt:message key="admin.add.dish" bundle="${ rb }"/>">
    </form>
   ${result}
    <c:import url="/jsp/footer/footer.jsp"></c:import>
</body>
</html>
