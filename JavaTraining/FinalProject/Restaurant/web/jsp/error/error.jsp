<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
    <%@include file='../../css/style.css' %>
</style>
<%@page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
    <title><fmt:message key="error.error" bundle="${ rb }"/></title></head>
<body>
    <c:import url="/jsp/header/header.jsp"></c:import>
    <span>some error</span>
    Request from ${pageContext.errorData.requestURI} is failed
    <br/>
    Servlet name or type: ${pageContext.errorData.servletName}
    <br/>
    Status code: ${pageContext.errorData.statusCode}
    <br/>
    Exception: ${pageContext.errorData.throwable}
    <br/>${exception}
    <c:import url="/jsp/footer/footer.jsp"></c:import>
</body>
</html>