<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<html>
<head>
    <title></title>
</head>
<body>
<c:choose>
    <c:when test="${roleOfUser==null}">
        <nav>
            <ul class="css-menu-3">
                <li><a href="controller?command=menu"><fmt:message key="main.main" bundle="${ rb }"/></a></li>
                <li><a href="controller?command=Links&link=search"><fmt:message key="guest.header_menu.search" bundle="${ rb }"/></a></li>
                <li><a href="controller?command=Links&link=login"><fmt:message key="login.login" bundle="${ rb }"/></a></li>
                <div class="locale"> <li><a href="controller?command=change_locale&locale=ru">RU</a></li>
                    <li> <a href="controller?command=change_locale&locale=en">EN</a></li>
                </div>
            </ul>
        </nav>
    </c:when>
</c:choose>
<c:choose>
    <c:when test="${roleOfUser=='user'}">
        <nav>
            <ul class="css-menu-3">
                <li><a href="controller?command=menu"><fmt:message key="main.main" bundle="${ rb }"/></a></li>
                <li><a href="controller?command=Links&link=info"><fmt:message key="user.header_menu.info_of_user" bundle="${ rb }"/></a></li>
                <li><a href="controller?command=orders_of_user"><fmt:message key="user.header_menu.my_orders" bundle="${ rb }"/></a></li>
                <li><a href="controller?command=logout"><fmt:message key="headerMenu.logout" bundle="${ rb }"/></a></li>
                <div class="locale">
                    <li><a href="controller?command=change_locale&locale=ru">RU</a></li>
                    <li> <a href="controller?command=change_locale&locale=en">EN</a></li>
                </div>
            </ul>
        </nav>
    </c:when>
</c:choose>

<c:choose>
    <c:when test="${roleOfUser=='administrator'}">
        <nav>
            <ul class="css-menu-3">
                <li><a href="controller?command=menu"><fmt:message key="main.main" bundle="${ rb }"/></a></li>
                <li><a href="controller?command=Links&link=info"><fmt:message key="user.header_menu.info_of_user" bundle="${ rb }"/></a></li>
                <li><a href="controller?command=output_for_admin&link=changeMenu"><fmt:message key="admin.change.menu" bundle="${ rb }"/></a></li>
                <li><a href="controller?command=output_for_admin&link=deleteUsers"><fmt:message key="admin.header_menu.delete_users" bundle="${ rb }"/></a></li>
                <li><a href="controller?command=output_for_admin&link=seeOrders"><fmt:message key="admin.header_menu.see_orders" bundle="${ rb }"/></a></li>
                <li><a href="controller?command=logout"><fmt:message key="headerMenu.logout" bundle="${ rb }"/></a></li>
                <div class="locale">
                    <li><a href="controller?command=change_locale&locale=ru">RU</a></li>
                    <li> <a href="controller?command=change_locale&locale=en">EN</a></li>
                </div>
            </ul>
        </nav>
    </c:when>
</c:choose>
</body>
</html>
