
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
    <%@include file='../../css/style.css' %>
</style>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/><html>
<html>
<head>
    <script src="../../js/jquery.min.js"type="text/javascript"></script>
    <script language="javascript" type="text/javascript" src="../../js/vtip.js"></script>
       <title><fmt:message key="user.info" bundle="${ rb }"/></title>
</head>
<body>
    <c:import url="/jsp/header/header.jsp"></c:import>
    <div id="wrapper">
        <h2><fmt:message key="user.name" bundle="${ rb }"/></h2>
        <div class="accordion">
            <div class="block">
                <span> ${loginOfUser}</span>
            </div>
        </div>
        <h2><fmt:message key="id.id" bundle="${ rb }"/></h2>
        <div class="accordion">
            <div class="block">
                <span> ${idOfUser}</span>
            </div>
        </div>
        <h2><fmt:message key="user.role" bundle="${ rb }"/></h2>
        <div class="accordion">
            <div class="block">
                <span>${roleOfUser}</span>
            </div>
        </div>
        <h2><fmt:message key="user.change.password" bundle="${ rb }"/></h2>
        <div class="accordion">
            <div class="block">
                <form name="change_password" method="POST" action="controller">
                    <input type="hidden" name="command"class="form" value="change_password">
                    <input type="text"  name="newPasswordFirst"  placeholder="<fmt:message key="user.change.new_password_first" bundle="${ rb }"/>">
                    <input type="text"  name="newPasswordSecond"  placeholder="<fmt:message key="user.change.new_password_second" bundle="${ rb }"/>">
                    <input type="submit"class="buttom"  value="<fmt:message key="user.change.password" bundle="${ rb }"/>">
                </form>
            </div>
        </div>
        <h2><fmt:message key="user.change.login" bundle="${ rb }"/></h2>
        <div class="accordion">
            <div class="block">
                <form name="change_login" method="POST" action="controller">
                    <input type="hidden" name="command"class="form" value="change_login">
                    <input type="text"  name="newLogin"  placeholder="<fmt:message key="user.change.new_login" bundle="${ rb }"/>">
                    <input type="submit"class="buttom"  value="<fmt:message key="user.change.login" bundle="${ rb }"/>">
                </form>
            </div>
        </div>
    </div>
    <br/> ${result}
    <c:import url="/jsp/footer/footer.jsp"></c:import>
</body>
</html>
