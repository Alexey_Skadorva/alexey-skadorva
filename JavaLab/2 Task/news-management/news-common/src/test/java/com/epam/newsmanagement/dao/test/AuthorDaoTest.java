package com.epam.newsmanagement.dao.test;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-configuration-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:news.xml")
public class AuthorDaoTest {
    private final static String AUTHOR_NAME = "text";

    @Autowired
    private AuthorDao authorDao;

    public AuthorDaoTest() {
        super();
    }

    @Test
    public void testGet() throws Exception {
        Long authorId = 1L;
        Author author = authorDao.getAuthorById(authorId);
        Author expectedAuthor = new Author(authorId, AUTHOR_NAME);
        assertEquals(author, expectedAuthor);
    }

    @Test
    public void testCheckUniquenessAuthor() throws Exception {
        boolean unique = authorDao.checkUniquenessAuthor(AUTHOR_NAME);
        assertEquals(unique, false);
    }

    @Test
    public void testGetByNewsId() throws Exception {
        long newsId = 1L;
        Author author = new Author(1L, AUTHOR_NAME);
        Author expectedAuthor = authorDao.getByNewsId(newsId);
        assertEquals(author, expectedAuthor);
    }


    @Test
    public void testCreateAuthor() throws Exception {
        Long insertId = authorDao.create(AUTHOR_NAME);
        Author author = authorDao.getAuthorById(insertId);
        assertEquals(author, new Author(insertId, AUTHOR_NAME));
    }

    @Test
    public void testAddNewsAuthor() throws Exception {
        Long authorId = 1L;
        Long newsId = 2L;
        authorDao.addNewsAuthor(newsId, authorId);
        Author author = authorDao.getByNewsId(newsId);
        assertEquals(author.getAuthorId(), authorId);
    }

    @Test
    public void testDeleteAuthor() throws Exception {
        Long authorId = 1L;
        authorDao.deleteDependencyWithNews(authorId);
        authorDao.delete(authorId);
        Author author = authorDao.getAuthorById(authorId);
        assertNull(author);
    }

    @Test
    public void testDeleteDependencyWithNews() throws Exception {
        Long authorId = 1L;
        authorDao.deleteDependencyWithNews(authorId);
        Author author = authorDao.getByNewsId(1L);
        assertNull(author.getAuthorId());
    }

    @Test
    public void testGetAllAuthors() throws Exception {
        List<Author> authors = authorDao.getAllAuthors();
        assertEquals(authors.size(), 1);
    }

    @Test
    public void testEdit() throws Exception {
        Long authorId = 1L;
        Author author = new Author(authorId, AUTHOR_NAME + AUTHOR_NAME);
        authorDao.edit(author);
        Author editingAuthor = authorDao.getAuthorById(authorId);
        assertEquals(editingAuthor.getAuthorName(), AUTHOR_NAME + AUTHOR_NAME);
    }

    @Test
    public void testExpire() throws Exception {
        Long authorId = 1L;
        Timestamp time = new Timestamp(1427369733);
        authorDao.expire(authorId, time);
        List<Author> allNotExpiredAuthors = authorDao.getAllAuthors();
        Author author = authorDao.getAuthorById(authorId);
        assertFalse(allNotExpiredAuthors.contains(author));
    }
}