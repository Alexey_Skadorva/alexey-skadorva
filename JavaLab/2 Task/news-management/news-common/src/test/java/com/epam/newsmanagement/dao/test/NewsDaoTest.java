package com.epam.newsmanagement.dao.test;

import com.epam.newsmanagement.dao.impl.AuthorDaoImpl;
import com.epam.newsmanagement.dao.impl.CommentsDaoImpl;
import com.epam.newsmanagement.dao.impl.NewsDaoImpl;
import com.epam.newsmanagement.dao.impl.TagDaoImpl;
import com.epam.newsmanagement.entity.*;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-configuration-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:news.xml")
public class NewsDaoTest {
    private final static String TEXT = "text";
    @Autowired
    NewsDaoImpl newsDao;
    @Autowired
    CommentsDaoImpl commentsDao;
    @Autowired
    TagDaoImpl tagDao;
    @Autowired
    AuthorDaoImpl authorDao;

    public NewsDaoTest() {
        super();
    }


    @Test
    public void testAddNews() throws Exception {
        Long newsId = 1L;
        News news = new News(1L, TEXT, TEXT, TEXT, new Timestamp(1427369733), new Date(1427369733));
        Long insertId = newsDao.create(news);
        News actualNews = newsDao.get(insertId);
        actualNews.setNewsId(newsId);
        assertEquals(actualNews.toString(), news.toString());
    }

    @Test
    public void testEditNews() throws Exception {
        Long newsId = 1L;
        News news = new News(newsId, "www", "www", "www", new Timestamp(1427369733), new Date(1427369733));
        newsDao.edit(news);
        News actualNews = newsDao.get(newsId);
        assertEquals(actualNews.toString(), news.toString());
    }

    @Test
    public void testDeleteNews() throws Exception {
        Long newsId = 1L;
        commentsDao.deleteByNewsId(newsId);
        newsDao.deleteDependencyWithAuthor(newsId);
        newsDao.deleteDependencyWithTags(newsId);
        newsDao.delete(newsId);
        List<Tag> tags = tagDao.getByNewsId(newsId);
        Author author = authorDao.getByNewsId(newsId);
        List<Comment> comments = commentsDao.getByNewsId(newsId);
        News news = newsDao.get(newsId);
        assertTrue(tags.isEmpty());
        assertNull(author.getAuthorId());
        assertTrue(comments.isEmpty());
        assertEquals(news, null);
    }

    @Test
    public void testDeleteDependencyWithAuthor() throws Exception {
        Long newsId = 1L;
        newsDao.deleteDependencyWithAuthor(newsId);
        Author author = authorDao.getByNewsId(newsId);
        assertNull(author.getAuthorId());
    }

    @Test
    public void testDeleteDependencyWithTags() throws Exception {
        Long newsId = 1L;
        newsDao.deleteDependencyWithTags(newsId);
        List<Tag> tags = tagDao.getByNewsId(newsId);
        assertTrue(tags.isEmpty());
    }

    @Test
    public void testGet() throws Exception {
        Long newsId = 1L;
        News actualNews = newsDao.get(newsId);
        assertNotNull(actualNews);
    }

    @Test
    public void testEditAuthorForNews() throws Exception {
        Long newsId = 1L;
        Long authorId = 1L;
        newsDao.editAuthorForNews(newsId, authorId);
        Author author = authorDao.getByNewsId(newsId);
        assertEquals(author.getAuthorId(), authorId);
    }

    @Test
    public void testFilter() throws Exception {
        SearchCriteria searchCriteria = new SearchCriteria(1L, null);
        List<News> news = newsDao.findNewsByFilters(searchCriteria, 1, 4);
        assertEquals(news.size(), 1);
    }

    @Test
    public void testCountNews() throws Exception {
        SearchCriteria searchCriteria = new SearchCriteria(1L, null);
        Long number = newsDao.countNumNews(searchCriteria);
        assertEquals(number, (Long) 1L);
    }
}
