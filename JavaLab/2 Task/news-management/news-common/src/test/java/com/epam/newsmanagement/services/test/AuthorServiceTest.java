package com.epam.newsmanagement.services.test;

import com.epam.newsmanagement.dao.impl.AuthorDaoImpl;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.impl.AuthorServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Timestamp;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {
    private final static String TEXT = "author_name";
    @InjectMocks
    AuthorServiceImpl authorService;
    @Mock
    AuthorDaoImpl authorDao;
    @Mock
    Author author;

    @Test
    public void testAddNewsAuthor() throws ServiceException, DAOException {
        Long id = 1L;
        doNothing().when(authorDao).addNewsAuthor(id, id);
        authorService.addNewsAuthor(id, id);
        verify(authorDao).addNewsAuthor(id, id);
        verifyNoMoreInteractions(authorDao);
    }

    @Test
    public void testGetAuthorForNews() throws ServiceException, DAOException {
        Long newsId = 1L;
        when(authorDao.getByNewsId(newsId)).thenReturn(author);
        authorService.getAuthorForNews(newsId);
        verify(authorDao).getByNewsId(newsId);
        verifyNoMoreInteractions(authorDao);
    }

    @Test
    public void testGetAllAuthors() throws ServiceException, DAOException {
        when(authorDao.getAllAuthors()).thenReturn(null);
        authorService.getAllAuthors();
        verify(authorDao).getAllAuthors();
        verifyNoMoreInteractions(authorDao);
    }

    @Test
    public void testAddAuthor() throws ServiceException, DAOException {
        Author author = new Author(1L, TEXT);
        when(authorDao.checkUniquenessAuthor(author.getAuthorName())).thenReturn(true);
        when(authorDao.create(author.getAuthorName())).thenReturn(author.getAuthorId());
        authorService.addAuthor(author.getAuthorName());
        verify(authorDao).checkUniquenessAuthor(author.getAuthorName());
        verify(authorDao).create(author.getAuthorName());
        verifyNoMoreInteractions(authorDao);
    }

    @Test
    public void testEditAuthor() throws ServiceException, DAOException {
        doNothing().when(authorDao).edit(author);
        authorService.editAuthor(author);
        verify(authorDao).edit(author);
        verifyNoMoreInteractions(authorDao);
    }

    @Test
    public void testDeleteAuthor() throws ServiceException, DAOException {
        Long authorId = 1L;
        doNothing().when(authorDao).deleteDependencyWithNews(authorId);
        doNothing().when(authorDao).delete(authorId);
        authorService.deleteAuthor(authorId);
        verify(authorDao).delete(authorId);
        verify(authorDao).deleteDependencyWithNews(authorId);
        verifyNoMoreInteractions(authorDao);
    }

    @Test
    public void testExpireAuthor() throws ServiceException, DAOException {
        Long authorId = null;
        Timestamp expired = new Timestamp(new java.util.Date().getTime());
        doNothing().when(authorDao).expire(authorId, expired);
        authorService.expireAuthor(authorId);
        verify(authorDao).expire(authorId, expired);
        verifyNoMoreInteractions(authorDao);
    }

    @Test
    public void testGetAuthorById() throws ServiceException, DAOException {
        Author author = new Author(1L, TEXT);
        when(authorDao.getAuthorById(author.getAuthorId())).thenReturn(author);
        authorService.getAuthorById(author.getAuthorId());
        verify(authorDao).getAuthorById(author.getAuthorId());
        verifyNoMoreInteractions(authorDao);
    }
}
