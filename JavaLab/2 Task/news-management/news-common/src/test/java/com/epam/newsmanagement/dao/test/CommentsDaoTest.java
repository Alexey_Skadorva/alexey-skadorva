package com.epam.newsmanagement.dao.test;

import com.epam.newsmanagement.dao.impl.CommentsDaoImpl;
import com.epam.newsmanagement.entity.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-configuration-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:news.xml")
public class CommentsDaoTest {
    private final static String COMMENT_TEXT = "text";
    @Autowired
    CommentsDaoImpl commentsDao;

    public CommentsDaoTest() {
        super();
    }

    @Test
    public void testGet() throws Exception {
        Long commentId = 1L;
        Comment comment = commentsDao.get(commentId);
        Comment expectedComment = new Comment(1L, 1L, COMMENT_TEXT, new Timestamp(1427369733));
        assertEquals(expectedComment, comment);
    }

    @Test
    public void testGetByNewsId() throws Exception {
        long id = 1L;
        List<Comment> comment = commentsDao.getByNewsId(id);
        assertEquals(comment.size(), 1);
    }

    @Test
    public void testDeleteComment() throws Exception {
        Long commentId = 1L;
        commentsDao.delete(commentId);
        Comment comment = commentsDao.get(commentId);
        assertNull(comment);
    }

    @Test
    public void testDeleteByNewsIdComment() throws Exception {
        Long newsId = 1L;
        commentsDao.deleteByNewsId(newsId);
        List<Comment> comments = commentsDao.getByNewsId(newsId);
        assertTrue(comments.isEmpty());
    }

    @Test
    public void testCreateComment() throws Exception {
        Comment comment = new Comment(1L, 1L, COMMENT_TEXT, new Timestamp(1427369733));
        Long insertId = commentsDao.create(comment);
        Comment expectedComment = commentsDao.get(insertId);
        expectedComment.setCommentId(1L);
        assertEquals(expectedComment, comment);
    }
}