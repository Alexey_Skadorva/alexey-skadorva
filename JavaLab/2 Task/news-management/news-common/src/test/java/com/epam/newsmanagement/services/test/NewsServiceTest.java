package com.epam.newsmanagement.services.test;

import com.epam.newsmanagement.dao.impl.NewsDaoImpl;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.impl.NewsServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {
    private final static String TEXT = "text";
    @InjectMocks
    NewsServiceImpl newsService;
    @Mock
    NewsDaoImpl newsDao;
    @Mock
    News news;

    @Test
    public void testGetNewsMessage() throws ServiceException, DAOException {
        News news = new News(1L, null, null, null, null, null);
        when(newsDao.get(news.getNewsId())).thenReturn(news);
        newsService.get(news.getNewsId());
        verify(newsDao).get(news.getNewsId());
        verifyNoMoreInteractions(newsDao);
    }

    @Test
    public void testAddNews() throws ServiceException, DAOException {
        News news = new News(1L, null, null, null, null, null);
        when(newsDao.create(news)).thenReturn(news.getNewsId());
        newsService.addNews(news);
        verify(newsDao).create(news);
        verifyNoMoreInteractions(newsDao);
    }

    @Test
    public void testEditNews() throws ServiceException, DAOException {
        doNothing().when(newsDao).edit(news);
        newsService.editNews(news);
        verify(newsDao).edit(news);
        verifyNoMoreInteractions(newsDao);
    }

    @Test
    public void testEditAuthorForNews() throws ServiceException, DAOException {
        Long newsId = 1L;
        Long authorId = 1L;
        doNothing().when(newsDao).editAuthorForNews(newsId, authorId);
        newsService.editAuthorForNews(newsId, authorId);
        verify(newsDao).editAuthorForNews(newsId, authorId);
        verifyNoMoreInteractions(newsDao);
    }

    @Test
    public void testDeleteNews() throws ServiceException, DAOException {
        doNothing().when(newsDao).delete(1L);
        newsService.deleteNews(1L);
        verify(newsDao).delete(1L);
        verifyNoMoreInteractions(newsDao);
    }

    @Test
    public void testDeleteDependencyWithAuthor() throws ServiceException, DAOException {
        Long newsId = 1L;
        doNothing().when(newsDao).deleteDependencyWithAuthor(newsId);
        newsService.deleteDependencyWithAuthor(newsId);
        verify(newsDao).deleteDependencyWithAuthor(newsId);
        verifyNoMoreInteractions(newsDao);
    }

    @Test
    public void testDeleteDependencyWithTags() throws ServiceException, DAOException {
        Long newsId = 1L;
        doNothing().when(newsDao).deleteDependencyWithTags(newsId);
        newsService.deleteDependencyWithTags(newsId);
        verify(newsDao).deleteDependencyWithTags(newsId);
        verifyNoMoreInteractions(newsDao);
    }

    @Test
    public void testFindByFilters() throws ServiceException, DAOException {
        SearchCriteria searchCriteria = null;
        List<News> news = null;
        when(newsDao.findNewsByFilters(searchCriteria, 1, 1)).thenReturn(news);
        newsService.findByFilters(searchCriteria, 1, 1);
        verify(newsDao).findNewsByFilters(searchCriteria, 1, 1);
        verifyNoMoreInteractions(newsDao);
    }

    @Test
    public void testCountNumNews() throws ServiceException, DAOException {
        SearchCriteria searchCriteria = null;
        when(newsDao.countNumNews(searchCriteria)).thenReturn(1L);
        newsService.countNumNews(searchCriteria);
        verify(newsDao).countNumNews(searchCriteria);
        verifyNoMoreInteractions(newsDao);
    }
}
