package com.epam.newsmanagement.services.test;

import com.epam.newsmanagement.dao.impl.CommentsDaoImpl;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.impl.CommentsServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CommentsServiceTest {
    @InjectMocks
    CommentsServiceImpl commentsService;
    @Mock
    CommentsDaoImpl commentsDao;
    @Mock
    Comment comment;

    @Test
    public void testDeleteComment() throws ServiceException, DAOException {
        Long commentId = 1L;
        doNothing().when(commentsDao).delete(commentId);
        commentsService.deleteComment(commentId);
        verify(commentsDao).delete(commentId);
        verifyNoMoreInteractions(commentsDao);
    }

    @Test
    public void testDeleteCommentByNewsId() throws ServiceException, DAOException {
        Long newsId = 1L;
        doNothing().when(commentsDao).deleteByNewsId(newsId);
        commentsService.deleteCommentByNewsId(newsId);
        verify(commentsDao).deleteByNewsId(newsId);
        verifyNoMoreInteractions(commentsDao);
    }

    @Test
    public void testGetCommentsForNews() throws ServiceException, DAOException {
        List<Comment> comments = null;
        Long newsId = 1L;
        when(commentsDao.getByNewsId(newsId)).thenReturn(comments);
        commentsService.getCommentsForNews(newsId);
        verify(commentsDao).getByNewsId(newsId);
        verifyNoMoreInteractions(commentsDao);
    }
}
