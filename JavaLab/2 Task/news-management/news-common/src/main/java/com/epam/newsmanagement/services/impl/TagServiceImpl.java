package com.epam.newsmanagement.services.impl;

import com.epam.newsmanagement.dao.impl.TagDaoImpl;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.TagService;
import org.apache.log4j.Logger;

import java.util.List;

public class TagServiceImpl implements TagService {
    private static Logger logger = Logger.getLogger(TagServiceImpl.class);
    private TagDaoImpl tagDao;

    @Override
    public Long addTag(String tagName) throws ServiceException {
        Long tagId = null;
        try {
            if (tagDao.checkUniquenessTag(tagName)) {
                tagId = tagDao.create(tagName);
            }
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return tagId;
    }

    @Override
    public void addNewsTag(Long newsId, List<Long> tagsId) throws ServiceException {
        try {
            tagDao.addTagForNews(newsId, tagsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Tag> getTagsForNews(Long newsId) throws ServiceException {
        List<Tag> tags = null;
        try {
            tags = tagDao.getByNewsId(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return tags;
    }

    @Override
    public void editTag(Tag tag) throws ServiceException {
        try {
            tagDao.edit(tag);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteTag(Long tagId) throws ServiceException {
        try {
            tagDao.deleteDependencyWithNews(tagId);
            tagDao.delete(tagId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Tag getTagById(Long tagId) throws ServiceException {
        Tag tag = null;
        try {
            tag = tagDao.getTagById(tagId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return tag;
    }

    @Override
    public List<Tag> getAllTags() throws ServiceException {
        List<Tag> tags = null;
        try {
            tags = tagDao.getAllTags();
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return tags;
    }

    public void setTagDao(TagDaoImpl tagDao) {
        this.tagDao = tagDao;
    }
}
