package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

import java.util.List;

public interface TagDao {
    /**
     * Takes the input id of the tag
     * and with the help of the query finds out his name
     *
     * @param tagId
     * @return tag name
     * @throws DAOException
     */
    Tag getTagById(Long tagId) throws DAOException;

    /**
     * Takes the input name of the tag
     * and with the help of the query finds out his id
     *
     * @param tagName
     * @return id
     * @throws DAOException
     */
    boolean checkUniquenessTag(String tagName) throws DAOException;

    /**
     * Adds a new tag. Insert it into a table Tag. The tag's name
     * is passed to the method, and id is automatically generated and
     * returned after insertion
     *
     * @param tagName
     * @return insert id
     * @throws DAOException
     */
    Long create(String tagName) throws DAOException;

    /**
     * Takes the input id of the news
     * and with the help of the query finds out tags for this news
     *
     * @param newsId
     * @return list of tags for this news
     * @throws DAOException
     */
    List<Tag> getByNewsId(Long newsId) throws DAOException;

    /**
     * Removes the author of the Author table with a unique id.
     *
     * @param tagId
     * @throws DAOException
     */
    void delete(Long tagId) throws DAOException;

    /**
     * Removes dependency tag with news.
     *
     * @param tagId
     * @throws DAOException
     */
    void deleteDependencyWithNews(Long tagId) throws DAOException;

    /**
     * Skid updated by table News_tag. Making the connection between
     * the tag and the news. tag_id meets in Table Tag,
     * and news_id related news in the table News
     *
     * @param newsId
     * @param tagId
     * @throws DAOException
     */
    void addTagForNews(Long newsId, List<Long> tagId) throws DAOException;

    /**
     * Returns all tags
     *
     * @return
     * @throws DAOException
     */
    List<Tag> getAllTags() throws DAOException;

    /**
     * Changes the tag who came on the parameters
     *
     * @param tag
     * @throws DAOException
     */
    void edit(Tag tag) throws DAOException;
}
