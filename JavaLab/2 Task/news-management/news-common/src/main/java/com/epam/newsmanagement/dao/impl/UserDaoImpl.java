package com.epam.newsmanagement.dao.impl;


import com.epam.newsmanagement.dao.UserDao;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.CloseResources;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.newsmanagement.utils.ColumnNames.*;

public class UserDaoImpl implements UserDao {
    private static final String FIND_ALL = "SELECT user_id,user_name,login,password,expired FROM USERS";
    private static final String FIND_BY_LOGIN = "SELECT user_id,user_name,login,password,expired FROM USERS WHERE login = ?";
    private static final String FIND_BY_ID = "SELECT user_id,user_name,login,password,expired FROM USERS WHERE user_id = ?";
    private static final String DELETE_BY_ID = "DELETE FROM USERS WHERE user_id = ?";
    private static final String INSERT_BY_ENTITY = "INSERT INTO USERS(user_id,user_name,login,password,expired) " +
            "VALUES (TAG_SEQ.nextVal,?,?,?,?)";
    private static final String UPDATE_BY_ID = "UPDATE USERS SET user_name=?,login=?,password=?,expired=? WHERE user_id=?";
    private static final String FIND_USER_ROLE = "SELECT role_name FROM ROLES WHERE user_id=?";
    private DataSource dataSource;
    private CloseResources closeResources;

    @Override
    public List<User> findAll() throws DAOException {
        List<User> resultList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(FIND_ALL);
            rs = pst.executeQuery();
            while (rs.next()) {
                User user = buildUser(rs);
                resultList.add(user);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return resultList;
    }

    @Override
    public User findById(Long id) throws DAOException {
        PreparedStatement pst = null;
        Connection connection = null;
        ResultSet rs = null;
        User result = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(FIND_BY_ID);
            pst.setLong(1, id);
            rs = pst.executeQuery();
            if (rs.next()) {
                result = buildUser(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return result;
    }

    @Override
    public void delete(Long id) throws DAOException {
        PreparedStatement pst = null;
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(DELETE_BY_ID);
            pst.setLong(1, id);
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }
    }

    @Override
    public Long add(User user) throws DAOException {
        PreparedStatement pst = null;
        Connection connection = null;
        ResultSet rs = null;
        Long insertId = 0L;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            String[] id = {"user_id"};
            pst = connection.prepareStatement(INSERT_BY_ENTITY, id);
            pst.setString(1, user.getName());
            pst.setString(2, user.getLogin());
            pst.setString(3, user.getPassword());
            pst.setTimestamp(4, user.getExpired());
            pst.execute();
            rs = pst.getGeneratedKeys();
            if (rs.next()) {
                insertId = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return insertId;
    }

    @Override
    public User findByLogin(String login) throws DAOException {
        PreparedStatement pst = null;
        Connection connection = null;
        ResultSet rs = null;
        User user = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(FIND_BY_LOGIN);
            pst.setString(1, login);
            rs = pst.executeQuery();

            if (rs.next()) {
                user = buildUser(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return user;
    }

    @Override
    public void update(User user) throws DAOException {
        PreparedStatement pst = null;
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(UPDATE_BY_ID);
            pst.setString(1, user.getName());
            pst.setString(2, user.getLogin());
            pst.setString(3, user.getPassword());
            pst.setTimestamp(4, user.getExpired());
            pst.setLong(5, user.getId());
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }
    }

    @Override
    public String findUserRole(Long id) throws DAOException {
        String role = null;
        PreparedStatement pst = null;
        Connection connection = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(FIND_USER_ROLE);
            pst.setLong(1, id);
            rs = pst.executeQuery();
            if (rs.next()) {
                role = rs.getString(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }
        return role;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private User buildUser(ResultSet rs) throws SQLException {
        return new User(rs.getLong(USER_ID),
                rs.getString(USER_NAME),
                rs.getString(LOGIN),
                rs.getString(PASSWORD),
                rs.getTimestamp(EXPIRED));
    }

    public void setCloseResources(CloseResources closeResources) {
        this.closeResources = closeResources;
    }
}