package com.epam.newsmanagement.services;

import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.ServiceException;

public interface UserService {
    /**
     * Finds user by login
     *
     * @param login
     * @return
     * @throws ServiceException
     */
    User findUser(String login) throws ServiceException;

    /**
     * Finds user role by id
     *
     * @param id
     * @return
     * @throws ServiceException
     */
    String findUserRole(Long id) throws ServiceException;
}
