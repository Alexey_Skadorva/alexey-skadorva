package com.epam.newsmanagement.dao;


import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;

import java.util.List;

public interface UserDao {
    /**
     * Finds all users
     *
     * @return list of users
     * @throws DAOException
     */
    List<User> findAll() throws DAOException;

    /**
     * Finds user by id
     *
     * @param id
     * @return user
     * @throws DAOException
     */
    User findById(Long id) throws DAOException;

    /**
     * Deletes user by id
     *
     * @param id
     * @return true if delete is successful, returns false otherwise
     * @throws DAOException
     */
    void delete(Long id) throws DAOException;

    /**
     * Creates user with given ingo
     *
     * @param entity
     * @return
     * @throws DAOException
     */
    Long add(User entity) throws DAOException;

    /**
     * Updates user info by id
     *
     * @param entity
     * @return true if update is successful, returns false otherwise
     * @throws DAOException
     */
    void update(User entity) throws DAOException;

    /**
     * Finds user by login
     *
     * @param login
     * @return
     * @throws DAOException
     */
    User findByLogin(String login) throws DAOException;

    /**
     * Finds user role
     *
     * @param id
     * @return
     * @throws DAOException
     */
    String findUserRole(Long id) throws DAOException;
}
