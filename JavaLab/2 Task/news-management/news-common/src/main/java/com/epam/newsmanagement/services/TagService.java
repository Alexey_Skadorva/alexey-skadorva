package com.epam.newsmanagement.services;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

public interface TagService {
    /**
     * Adds the tag.If the tag does not exist, it is
     * created in the table Tag
     *
     * @param tagName
     * @return
     * @throws ServiceException
     */
    Long addTag(String tagName) throws ServiceException;

    /**
     * Adds an existing news to the tag.Set only the dependence of the
     * table News_Tag
     * the relationship with the news
     *
     * @param newsId,tagId
     * @throws ServiceException
     */
    void addNewsTag(Long newsId, List<Long> tagsId) throws ServiceException;

    /**
     * Takes the news id and returns all tags on this news
     *
     * @param newsId
     * @return name
     * @throws ServiceException
     */
    List<Tag> getTagsForNews(Long newsId) throws ServiceException;

    /**
     * Returns all tags
     *
     * @return tags
     * @throws ServiceException
     */
    List<Tag> getAllTags() throws ServiceException;

    /**
     * Changes came tag
     *
     * @param tag
     * @throws ServiceException
     */
    void editTag(Tag tag) throws ServiceException;

    /**
     * Deletes tag by id
     *
     * @param tagId
     * @throws ServiceException
     */
    void deleteTag(Long tagId) throws ServiceException;

    /**
     * Returns tag by id
     *
     * @param tagId
     * @return
     * @throws ServiceException
     */
    Tag getTagById(Long tagId) throws ServiceException;
}
