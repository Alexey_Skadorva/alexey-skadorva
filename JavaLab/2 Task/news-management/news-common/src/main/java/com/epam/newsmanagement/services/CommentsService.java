package com.epam.newsmanagement.services;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

public interface CommentsService {
    /**
     * Adds comment to the news.
     * Adds a comment in the table Comments.
     *
     * @return
     * @throws ServiceException
     */
    Long addComment(String commentText, Long newsId) throws ServiceException;

    /**
     * Removes the comment from the table
     * Comments, by a unique id
     *
     * @throws ServiceException
     */
    void deleteComment(Long commentId) throws ServiceException;

    /**
     * Removes the comment from the table
     * Comments, by a id of news
     *
     * @throws ServiceException
     */
    void deleteCommentByNewsId(Long newsId) throws ServiceException;

    /**
     * Takes the news id and returns all comments on this news
     *
     * @return name
     * @throws ServiceException
     */
    List<Comment> getCommentsForNews(Long newsId) throws ServiceException;
}
