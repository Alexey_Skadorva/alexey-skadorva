package com.epam.newsmanagement.entity;

import java.io.Serializable;

public class Tag implements Serializable {
    private static final long serialVersionUID = -1340917654956509396L;
    private Long tagId;
    private String tagName;

    public Tag(Long tagId, String tagName) {
        this.tagId = tagId;
        this.tagName = tagName;
    }

    public Tag() {

    }

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    @Override
    public String toString() {
        return tagId + " " + tagName;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = (int) (result + tagId);
        result = result + tagName.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Tag tag = (Tag) obj;
        if (tagId != tag.tagId)
            return false;
        if (!tagName.equals(tag.tagName))
            return false;
        return true;
    }
}
