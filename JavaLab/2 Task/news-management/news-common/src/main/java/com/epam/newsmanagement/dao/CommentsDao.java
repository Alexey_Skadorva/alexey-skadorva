package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;

import java.util.List;

public interface CommentsDao {
    /**
     * Takes the input id of the comment
     * and with the help of the query finds out his
     *
     * @param commentId
     * @return comment
     * @throws DAOException
     */
    Comment get(Long commentId) throws DAOException;


    /**
     * Adds a new comment. Insert it into a table Comments.
     *
     * @param comment
     * @return id
     * @throws DAOException
     */
    Long create(Comment comment) throws DAOException;

    /**
     * Removes the comment of the Comments table with a unique id.
     *
     * @param commentId
     * @throws DAOException
     */
    void delete(Long commentId) throws DAOException;

    /**
     * Deletes all comments on news
     *
     * @param newsId
     * @throws DAOException
     */
    void deleteByNewsId(Long newsId) throws DAOException;

    /**
     * Takes the input id of the news
     * and with the help of the query finds out comments for this news
     *
     * @param newsId
     * @return comments
     * @throws DAOException
     */
    List<Comment> getByNewsId(Long newsId) throws DAOException;
}
