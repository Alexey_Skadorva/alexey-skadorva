package com.epam.newsmanagement.services;

import com.epam.newsmanagement.entity.ComplexNews;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;

import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.util.List;

public interface MainService {

    /**
     * Adds a news table News. id news is generated
     * automatically, and all other fields  are passed to the method.
     * And adding author and tags for this news
     *
     * @param news
     * @throws ServiceException
     */
    Long addNews(News news, Long authorId, List<Long> tags, Date date) throws ServiceException;

    /**
     * Deletes a news table News takes on a unique id. Just
     * delete all comments on news and links in the tables
     * News_Author and News_Tag
     *
     * @param newsIds
     * @throws ServiceException
     */
    void deleteNews(List<Long> newsIds) throws ServiceException;

    /**
     * Edits a news table News takes on a unique id. Just
     * edit all information about news:�omments, author,news
     *
     * @param news
     * @throws ServiceException
     */
    void editNews(News news, Long authorId, List<Long> tags, Date date) throws ServiceException;

    /**
     * Takes the input id of the news
     * and return all information about this news
     *
     * @return news
     * @throws ServiceException
     */
    List<ComplexNews> findByFilters(SearchCriteria searchCriteria, int pageId, int numberNewsOnPage) throws ServiceException;

    /**
     * Returns all information about news
     *
     * @param newsId
     * @return complexNews
     * @throws ServiceException
     */
    ComplexNews getAllInfoAboutNews(Long newsId) throws ServiceException;

    /**
     * Finds previous news id
     *
     * @param newsId,session,numNewsOnPage
     * @return id
     * @throws ServiceException
     */
    Long findPreviousNewsId(Long newsId, HttpSession session, int numNewsOnPage) throws ServiceException;

    /**
     * Finds next news id
     *
     * @param newsId,session,numNewsOnPage
     * @return id
     * @throws ServiceException
     */
    Long findNextNewsId(Long newsId, HttpSession session, int numOfPages, int numNewsOnPage) throws ServiceException;

    /**
     * Save news
     *
     * @param news,authorId,tags,date
     * @return id
     * @throws ServiceException
     */
    Long saveNews(News news, Long authorId, List<Long> tags, Date date) throws ServiceException;
}
