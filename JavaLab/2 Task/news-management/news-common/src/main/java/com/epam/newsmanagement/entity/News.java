package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class News implements Serializable {

    private static final long serialVersionUID = -6891276437253024367L;
    private Long newsId;
    private String shortText;
    private String fullText;
    private String title;
    private Timestamp creationDate;
    private Date modificationDate;


    public News(Long newsId, String shortText, String fullText, String title, Timestamp creationDate, Date modificationDate) {
        this.newsId = newsId;
        this.shortText = shortText;
        this.fullText = fullText;
        this.title = title;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
    }


    public News() {

    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String getShortText() {
        return shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public String getTitle() {
        return title;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }


    @Override
    public String toString() {
        return newsId + " " + shortText + " " + fullText + " " + title + " "
                + creationDate + " " + modificationDate;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = (int) (result + newsId);
        result = result + shortText.hashCode();
        result = result + fullText.hashCode();
        result = result + title.hashCode();
        result = result + creationDate.hashCode();
        result = result + modificationDate.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        News news = (News) obj;
        if (fullText.equals(news.fullText))
            return false;
        if (shortText.equals(news.shortText))
            return false;
        if (!title.equals(news.title))
            return false;
        if (!creationDate.equals(news.creationDate))
            return false;
        if (!modificationDate.equals(news.modificationDate))
            return false;
        return true;
    }
}
