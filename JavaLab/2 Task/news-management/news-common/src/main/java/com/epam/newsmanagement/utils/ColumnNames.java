package com.epam.newsmanagement.utils;

public class ColumnNames {
    public final static String AUTHOR_ID = "author_id";
    public final static String AUTHOR_NAME = "author_name";

    public final static String NEWS_ID = "news_id";
    public final static String SHORT_TEXT = "short_text";
    public final static String FULL_TEXT = "full_text";
    public final static String TITLE = "title";
    public final static String CREATION_DATE = "creation_date";
    public final static String MODIFICATION_DATE = "modification_date";

    public final static String COMMENT_ID = "comment_id";
    public final static String COMMENT_TEXT = "comment_text";

    public final static String TAG_NAME = "tag_name";
    public final static String TAG_ID = "tag_id";

    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String EXPIRED = "expired";

}
