package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.CommentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

import static com.epam.newsmanagement.utils.RequestMappingNames.*;

/**
 * The controller is designed to interface with comments.
 * In the controller there are methods to add
 * and remove a comment.
 */
@Controller
public class CommentsController {
    @Autowired
    CommentsService commentsService;

    @RequestMapping(value = ADD_COMMENT)
    public ModelAndView addComment(
            @RequestParam(value = "newsId", required = false) Long newsId,
            @RequestParam(value = "commentText", required = false) String commentText) throws ServiceException {
        commentsService.addComment(commentText, newsId);
        return getCommentsList(newsId);
    }

    @RequestMapping(value = DELETE_COMMENT)
    public ModelAndView deleteComment(
            @RequestParam(value = "newsId", required = false) Long newsId,
            @RequestParam(value = "commentId", required = false) Long commentId) throws ServiceException {
        commentsService.deleteComment(commentId);
        return getCommentsList(newsId);
    }

    public ModelAndView getCommentsList(Long newsId) throws ServiceException {
        ModelAndView mv = new ModelAndView(NEWS_VIEW);
        List<Comment> comments = commentsService.getCommentsForNews(newsId);
        mv.addObject("comments", comments);
        return mv;
    }

    @ExceptionHandler(ServiceException.class)
    public ModelAndView serviceExceptionHandler() {
        return new ModelAndView(ERROR);
    }
}
