function switchEnableAuthorInput(inputId) {
        document.getElementById("text"+inputId).disabled = false;
        document.getElementById("edit"+inputId).style.visibility = "hidden";
        document.getElementById("update"+inputId).style.visibility = "visible";
        document.getElementById("expire"+inputId).style.visibility = "visible";
        document.getElementById("cancel"+inputId).style.visibility = "visible";
}
function cancelAuthorInput(inputId) {
    document.getElementById("text"+inputId).disabled = true;
    document.getElementById("edit"+inputId).style.visibility = "visible";
    document.getElementById("update"+inputId).style.visibility = "hidden";
    document.getElementById("expire"+inputId).style.visibility = "hidden";
    document.getElementById("cancel"+inputId).style.visibility = "hidden";
}
function switchEnableTagInput(inputId) {
    document.getElementById("text"+inputId).disabled = false;
    document.getElementById("edit"+inputId).style.visibility = "hidden";
    document.getElementById("update"+inputId).style.visibility = "visible";
    document.getElementById("delete"+inputId).style.visibility = "visible";
    document.getElementById("cancel"+inputId).style.visibility = "visible";
}
function cancelTagInput(inputId) {
    document.getElementById("text"+inputId).disabled = true;
    document.getElementById("edit"+inputId).style.visibility = "visible";
    document.getElementById("update"+inputId).style.visibility = "hidden";
    document.getElementById("delete"+inputId).style.visibility = "hidden";
    document.getElementById("cancel"+inputId).style.visibility = "hidden";
}
function updateFormLinkTag(inputId){
     var form = document.forms["updateForm"+inputId];
        if (form.elements['tagName'].value.length>30){
                 document.getElementById("validate").style.visibility = "visible";
            }else  if (form.elements['tagName'].value.length==0){
                                       document.getElementById("validateEmpty").style.visibility = "visible";
                                  }else{
                document.forms["updateForm"+inputId].submit();
                document.getElementById("validate").style.visibility = "hidden";
                return true;
            }
}

function updateFormLinkAuthor(inputId){
     var form = document.forms["updateForm"+inputId];
            if (form.elements['authorName'].value.length>30){
                 document.getElementById("validate").style.visibility = "visible";
             } else if (form.elements['authorName'].value.length==0){
                                   document.getElementById("validateEmpty").style.visibility = "visible";
                              }else{
                document.forms["updateForm"+inputId].submit();
                document.getElementById("validate").style.visibility = "hidden";
                return true;
            }
}

function saveFormTag(){
     var form = document.forms["add_tag"];
        if (form.elements['tagName'].value.length>30){
             document.getElementById("validate").style.visibility = "visible";
        }else
          if (form.elements['tagName'].value.length==0){
                     document.getElementById("validateEmpty").style.visibility = "visible";
                }
        else{
            document.forms["save"].submit();
            document.getElementById("validate").style.visibility = "hidden";
            return true;
        }
}

function saveFormAuthor(){
     var form = document.forms["add_author"];
        if (form.elements['authorName'].value.length>30){
             document.getElementById("validate").style.visibility = "visible";
        }  if (form.elements['authorName'].value.length==0){
                      document.getElementById("validateEmpty").style.visibility = "visible";
                 }else{
            document.forms["save"].submit();
            document.getElementById("validate").style.visibility = "hidden";
            return true;
        }
}
function editNews(inputId){
    document.forms["edit"+inputId].submit();
    return false;
}

function validateNews(){
    var form = document.forms["addNews"];
    var select = form.elements["authorId"];

    var validationFlag = true;
    var authorSelected=false;
    for (var i = 1; i < select.options.length; i++) {
      var option = select.options[i];
      if(option.selected&&option.selected.value!=-1) {
        authorSelected = true;
      }
    }
    validationFlag = authorSelected;//if we have author
    if (!authorSelected){
        validationFlag = false;
        document.getElementById("validateAuthor").style.visibility = "visible";
    }else{
        document.getElementById("validateAuthor").style.visibility = "hidden";
    }

     if ( form.elements['title'].value.length>30){
          validationFlag = false;
          document.getElementById("validateTitleSize").style.visibility = "visible";
     }else{
          document.getElementById("validateTitleSize").style.visibility = "hidden";
     }

     if ( form.elements['shortText'].value.length>100){
           validationFlag = false;
           document.getElementById("validateShortTextSize").style.visibility = "visible";
     }else{
           document.getElementById("validateShortTextSize").style.visibility = "hidden";
     }
     if ( form.elements['fullText'].value.length>2000){
             validationFlag = false;
             document.getElementById("validateFullTextSize").style.visibility = "visible";
     }else{
             document.getElementById("validateFullTextSize").style.visibility = "hidden";
     }

    return validationFlag;
}

function validateComment(){
    var form = document.forms["add_comment"];

    var validationFlag = true;

    if(form.elements['commentText'].value.length>100){
                 validationFlag = false;
             document.getElementById("validateCommentSize").style.visibility = "visible";
    }
     else{
       document.getElementById("validateCommentSize").style.visibility = "hidden";
    }
    return validationFlag;
}

function deleteNewsValidate(locale){
    var form = document.forms['deleteNews'];

    var newsToDelete = false;
    if (form.elements['newsIds'].checked == true){
        newsToDelete = true;
    }
    for (var i=0;i< form.elements['newsIds'].length;i++){
        if(form.elements['newsIds'][i].checked){
            newsToDelete = true;
            break;
        }
    }
    console.log(newsToDelete);
    if (!newsToDelete){
        if (locale == 'en_EN'){
            alert("Choose some news to delete");
        }else{
            alert("Выберите новость для удаления");
        }
        return false;
    }
    if (locale == 'en_EN'){
        if (confirm('Are you sure you want to delete news?')){
            return true;
        }
    }else{
        if (confirm('Вы уверены, что хотите удалить новость?')){
            return true;
        }
    }

    return false;
}



