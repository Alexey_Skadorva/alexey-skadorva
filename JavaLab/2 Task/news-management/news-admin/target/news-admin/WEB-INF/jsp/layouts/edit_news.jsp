<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="box">
    <form action="/edit_news" class="editNews">
        <div class="navigationEdit">
            <select id="mymenu"   name="authorId">
                  <c:if test="${authors != null}">
                    <c:forEach var="i" begin="0" end="${fn:length(authors)-1}">
                      <option  value="${authors.get(i).getAuthorId()}">${authors.get(i).getAuthorName()}</option>
                    </c:forEach>
                  </c:if>
                </select>
                <select name="tagId" id="multi-dropbox" class="multi-dropbox" multiple="multiple">
                <c:forEach items="${tags}" var="tag">
                    <c:set var="contains" value="false" />
                    <c:forEach var="item" items="${tagsId}">
                        <c:if test="${item == tag.tagId}">
                            <c:set var="contains" value="true" />
                        </c:if>
                    </c:forEach>
                    <c:if test="${contains == true}"> <option selected="selected" value="${tag.tagId}">${tag.tagName}</option></c:if>
                    <c:if test="${contains != true}"><option value="${tag.tagId}">${tag.tagName}</option></c:if>
                </c:forEach>
            </select>
        </div>
        <article>
            <fmt:message key="news.title" /><br />
            <input class="changeTitle" type="text" name="title" value="${news.getTitle()}" required size="50" /><br />
        </article>
        <article><fmt:message key="news.shortText" /> <br />
            <input class="changeTitle" type="text" name="shortText" value="${news.getShortText()}" required size="90" cols="2"/><br /></article>
        <article>
            <fmt:message key="news.fullText" /><br />
            <textarea class="changeFullText" name="fullText" rows="10" cols="50">${news.getFullText()}</textarea><br />
        </article>
        <input type="hidden" name="newsId" value="${news.getNewsId()}"/>
        <input type="submit" value="<fmt:message key="news.change.save"/> ">
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function() {
    $(".multi-dropbox").dropdownchecklist();
    });
</script>