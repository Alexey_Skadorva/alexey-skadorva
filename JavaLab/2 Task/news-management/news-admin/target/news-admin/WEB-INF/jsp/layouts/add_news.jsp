<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="now" value="<%=new java.util.Date()%>" />

<div class="box">
    <form method="POST" action="/add_news" name="addNews" id="addNews"  onsubmit="return validateNews()" >
        <article>
             <b><fmt:message key="news.title"/>:</b>
            <input class="infoNews" type="text" name="title"  value="${news.getTitle()}" />
        </article>
        <article >
            <b><fmt:message key="news.date"/>:</b>
        <c:if test="${news.modificationDate != null}">
            <input class="changeDate" type="date" name="date" value="<ctg:inputDateValue date="${news.modificationDate}"/>"/>
       </c:if>
        <c:if test="${news.modificationDate == null}">
            <input class="changeDate" type="date" name="date" value="<ctg:inputDateValue date="${now}"/>"/>
        </c:if>
        </article>
        <article >
            <b><fmt:message key="news.shortText"/>:</b>
            <textarea class="infoNews" type="text" name="shortText" rows="5"
            cols="5" ><c:out value="${news.getShortText()}"/></textarea>
        </article>
       <article class="area">
           <b> <fmt:message key="news.fullText"/>:</b>
           <textarea  class="infoNews" type="text" name="fullText"
           rows="10" cols="50" ><c:out value="${news.getFullText()}"/></textarea>
       </article>
        <input type="hidden" name="newsId" value="${news.getNewsId()}"/>
        <input class="addNewsSave" type="submit" value="<fmt:message key="news.change.save"/>"/>

    <div class="changeAuthor">
        <select name="authorId"  class="changeFromList">
            <option value="-1"><fmt:message key="select.author"/></option>
            <c:forEach var="author" items="${authors}" varStatus="status">
                <option value="${ author.authorId }" >
                    <c:out value="${ author.authorName }" />
                </option>
            </c:forEach>
        </select>
    <select name="tagId" id="multi-dropbox" class="multi-dropbox" multiple="multiple" >
        <c:forEach items="${tags}" var="tag">
            <c:set var="contains" value="false" />
            <c:forEach var="item" items="${tagsId}">
                <c:if test="${item == tag.tagId}">
                    <c:set var="contains" value="true" />z
                </c:if>
            </c:forEach>
            <c:if test="${contains == true}"> <option selected="selected" value="${tag.tagId}">${tag.tagName}</option></c:if>
            <c:if test="${contains != true}"><option value="${tag.tagId}">${tag.tagName}</option></c:if>
        </c:forEach>
    </select>
        </div>
    </form>
       <span id="validate" class="hidden-validate" ><fmt:message key="news.validate"/></span>

</div>

<script type="text/javascript">
    $(document).ready(function() {
                $(".multi-dropbox").dropdownchecklist();
            }
    );
</script>
