<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div class="box">
  <div class="changeTagsAndAuthors">
    <c:if test="${authors != null}">
      <c:forEach var="i" begin="0" end="${fn:length(authors)-1}">
      <form id="updateForm${authors.get(i).getAuthorId()}" action="/update_author" >
         <b> <fmt:message key="author.author"/>:</b>
        <input class="updateForm"  id="text${authors.get(i).getAuthorId()}"  type="text" name="authorName" value="${authors.get(i).getAuthorName()}" disabled/>
        <input type="hidden"  name="authorId" value="${authors.get(i).getAuthorId()}"/>
          <a id="edit${authors.get(i).getAuthorId()}" href="javascript:{}" onclick="switchEnableAuthorInput(${authors.get(i).getAuthorId()})">
            <fmt:message key="author.edit"/></a>
          <a id="update${authors.get(i).getAuthorId()}" href="javascript:{}" onclick="updateFormLink(${authors.get(i).getAuthorId()})" class="hidden-link">
            <fmt:message key="author.update"/> </a>
          <a id="expire${authors.get(i).getAuthorId()}" href="/expire_author/${authors.get(i).getAuthorId()}" class="hidden-link" >
            <fmt:message key="author.expire"/></a>
        <a class="hidden-link" id="cancel${authors.get(i).getAuthorId()}" href="javascript:{}" onclick="cancelAuthorInput(${authors.get(i).getAuthorId()})">
          <fmt:message key="author.cancel"/></a><br />  </form>
      </c:forEach>
    </c:if>
    <form  method="POST" id="save"  action="/add_author" class="addTagAndAuthor">
     <b> <fmt:message key="author.add"/>:</b>
      <input class="saveForm" type="text" name="authorName" />
      <a href="javascript:{}" onclick="saveFormAuthor()"><fmt:message key="author.save"/></a>
    </form>
  </div>
  <div class="error">
    <c:if test="${error != null}">
      <fmt:message key="error.empty"/>
    </c:if>
    </div>
</div>