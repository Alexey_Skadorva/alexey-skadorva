<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
  <head>
    <script src="<c:url value='/resources/js/script.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/resources/js/script.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/resources/js/jquery-1.11.2.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/resources/js/jquery-ui-1.11.2.min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/resources/js/dropdown.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/resources/js/ui.dropdownchecklist.js'/>"></script>
    <link href="<c:url value='/resources/css/style.css'/>" rel="stylesheet" />
    <title> <tiles:getAsString name="title" /></title>
  </head>
  <body>
    <tiles:insertAttribute name="header" />
    <tiles:insertAttribute name="menu"/>
    <tiles:insertAttribute name="content"/>
    <tiles:insertAttribute name="footer" />
  </body>
</html>
