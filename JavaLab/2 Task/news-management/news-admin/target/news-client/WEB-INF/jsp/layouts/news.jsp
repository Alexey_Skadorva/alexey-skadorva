<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div class="box">
  <a href="main">BACK</a>
  <div class="newsMessage">
    <div class="title">
      <b>${generalNews.getNews().getTitle()}</b>
      <span>(by${generalNews.getAuthor().getAuthorName()})</span>
    </div>
    <b>${generalNews.toStringTags()}</b>
    <div class="date">
      <u>${generalNews.getNews().getModificationDate()}</u>
    </div>
    <div class="shortText">${generalNews.getNews().getFullText()}</div>
    <c:forEach var="i" begin="0" end="${fn:length(generalNews.getComments())-1}">
      <div class="shortText">
        <u>${generalNews.getComments().get(i).getCreationDate()}</u>
      </div>
      <div class="commentText">${generalNews.getComments().get(i).getCommentText()}</div>
    </c:forEach>
    <form name="add_comment" class="addComment" action="addComment">
      <input type="hidden" name="newsId" class="form"
             value="${generalNews.getNews().getNewsId()}">
      <input type="text" id="commentText" name="commentText" /><br>
      <input type="submit" id="buttonAddComment" value="Post comment" />
    </form>
  </div>

  <div class="PreviousNextPages">
    <a href="mainnn">PREVIOUS</a>
    <div class="date">
      <a href="main">NEXT</a>
    </div>
  </div>
</div>
