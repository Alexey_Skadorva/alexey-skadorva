<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="box">
  <div class="navigation">
    <form action="news-filter">
      <select id="mymenu"   name="authorId">
        <c:if test="${authors != null}">
          <c:forEach var="i" begin="0" end="${fn:length(authors)-1}">
            <option  value="${authors.get(i).getAuthorId()}">${authors.get(i).getAuthorName()}</option>
          </c:forEach>
        </c:if>
      </select> <select id="mymenu">
      <c:if test="${tags != null}">
        <c:forEach var="i" begin="0" end="${fn:length(tags)-1}">
          <option>${tags.get(i).getTagName()}</option>
        </c:forEach>
      </c:if>
    </select>
      <input type="submit" class="buttom" value="Filter"/>
    </form>
    <form action="/reset-filter">
      <input type="submit" class="buttom" value="Reset" />
    </form>
  </div>
  <c:if test="${generalNews != null}">
    <c:forEach var="i" begin="0" end="${fn:length(generalNews)-1}">
      <div class="shortNews">
        <div class="title">
          <b>${generalNews.get(i).getNews().getTitle()}</b>
          <span>(by ${generalNews.get(i).getAuthor().getAuthorName()})</span>
        </div>
        <div class="date"><u>${generalNews.get(i).getNews().getModificationDate()}</u></div>
        <div class="shortText">${generalNews.get(i).getNews().getShortText()}</div>
        <div class="infoAdoutNews"><div class="tags">${generalNews.get(i).toStringTags()}</div>
          <div class="comments">Comments(${fn:length(generalNews.get(i).getComments())})  </div>
          <a href="viewNews?newsId=${generalNews.get(i).getNews().getNewsId()}">View</a>
        </div></div>
    </c:forEach>
  </c:if>

</div>
</div>
