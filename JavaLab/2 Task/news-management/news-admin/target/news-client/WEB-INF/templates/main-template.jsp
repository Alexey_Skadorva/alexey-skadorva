<!DOCTYPE HTML>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<html>
<head>
</head>
    <body>
        <tiles:insertAttribute name="header" />
        <tiles:insertAttribute name="menu"/>
        <tiles:insertAttribute name="content"/>
        <tiles:insertAttribute name="footer" />
    </body>
</html>
