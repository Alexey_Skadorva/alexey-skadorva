
$( document ).ready(function() {


var e = document.getElementById("ulDrop");
var numOfChanged = 0;

	
	if($('#UlDrop span').text()) {
		
		var tagNames = $('#UlDrop span').text();
		tagNames = tagNames.split(',');
		
		$.each($(".dropdownTags"), function(i, value){
			var tempTag = $($(".dropdownTags")[i]).text();
			$.each(tagNames, function(j, tagValue) {
				if($($(".dropdownTags")[i]).text() == tagNames[j]) {
					($($(".dropdownTags")[i]).children('input')).prop('checked', true);
					numOfChanged++;
				}
			})
		})
	}
	
	e.onclick = function(e) {

		var liElements = document.getElementsByClassName("dropdownTags");
		if(liElements[0].style.display == 'none') {
			for(var i = 0; i < liElements.length; i++) {
				liElements[i].style.display = "block";
			}
		} else {
			for(var i = 0; i < liElements.length; i++) {
				liElements[i].style.display = "none";
			}
		}
	}	
	
	$(".checkboxes-create-news").on("change", function() {
		
	    if(this.checked) {
	    	var tagName = $(this).next().text();
	    	$('#ulDropdownText').remove();
	    	$('#ulDrop').append("<span id='" + tagName + "'>" + tagName + ", </span> ")
	    	numOfChanged++;
	    } else {
	    	var tagName = $(this).next().text();
	    	$('#' + tagName).remove();
	    	numOfChanged--;
	    	if(numOfChanged == 0) {
	    		$('#ulDrop').append("<span id='ulDropdownText'>Plese select tags</span> ")
	    	}
	    }
	});
	
	
});