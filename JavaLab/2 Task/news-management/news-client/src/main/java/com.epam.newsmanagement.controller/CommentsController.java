package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.CommentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

import static com.epam.newsmanagement.utils.RequestMappingNames.ADD_COMMENT;
import static com.epam.newsmanagement.utils.RequestMappingNames.NEWS_VIEW;

//import org.apache.log4j.Logger;

@Controller
public class CommentsController {
    @Autowired
    CommentsService commentsService;

    @RequestMapping(value = ADD_COMMENT)
    public ModelAndView addComment(
            @RequestParam(value = "newsId", required = false) Long newsId,
            @RequestParam(value = "commentText", required = false) String commentText,
            ModelMap model, HttpSession session) throws ServiceException {
        commentsService.addComment(commentText, newsId);
        List<Comment> comments = commentsService.getCommentsForNews(newsId);
        model.addAttribute("comments", comments);
        return new ModelAndView(NEWS_VIEW);
    }
}
