<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<style>
	<%@include file='/resources/css/style.css' %>
</style>
<div class="header"><fmt:message key="news.header" />
	<div class="locale">
		<a href="?language=en_EN">EN</a>|<a href="?language=ru_RU">RU</a>
	</div>
</div>
