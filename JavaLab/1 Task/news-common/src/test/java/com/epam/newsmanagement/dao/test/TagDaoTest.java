package com.epam.newsmanagement.dao.test;

import com.epam.newsmanagement.dao.impl.TagDaoImpl;
import com.epam.newsmanagement.entity.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-configuration-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:news.xml")
public class TagDaoTest {
    private final static String TAG_NAME = "text";
    @Autowired
    TagDaoImpl tagDao;

    public TagDaoTest() {
        super();
    }

    @Test
    public void testCreateTag() throws Exception {
        Long insertId = tagDao.create(TAG_NAME);
        Tag expectedTag = tagDao.getTagById(insertId);
        Tag actualTag = new Tag(insertId, TAG_NAME);
        assertEquals(expectedTag, actualTag);
    }

    @Test
    public void testGet() throws Exception {
        Long tagId = 1L;
        Tag actualTag = tagDao.getTagById(tagId);
        Tag expectedTag = new Tag(tagId, TAG_NAME);
        assertEquals(actualTag, expectedTag);
    }

    @Test
    public void testCheckUniquenessTag() throws Exception {
        boolean unique = tagDao.checkUniquenessTag(TAG_NAME);
        assertEquals(unique, false);
    }

    @Test
    public void testGetByNewsId() throws Exception {
        Long newsId = 1L;
        List<Tag> tag = tagDao.getByNewsId(newsId);
        assertEquals(tag.size(), 1);
    }

    @Test
    public void testDeleteTag() throws Exception {
        Long tagId = 1L;
        tagDao.deleteDependencyWithNews(tagId);
        tagDao.delete(tagId);
        Tag tag = tagDao.getTagById(tagId);
        assertNull(tag);
    }

    @Test
    public void testDeleteDependencyWithNews() throws Exception {
        Long tagId = 1L;
        tagDao.deleteDependencyWithNews(tagId);
        List<Tag> tags = tagDao.getByNewsId(1L);
        assertTrue(tags.isEmpty());
    }

    @Test
    public void testAddTagForNews() throws Exception {
        Long newsId = 2L;
        List<Long> tagsId = new ArrayList<>();
        tagsId.add(1L);
        tagDao.addTagForNews(newsId, tagsId);
        List<Tag> tags = tagDao.getByNewsId(newsId);
        assertEquals(tags.size(), 1);
    }

    @Test
    public void testGetAllTags() throws Exception {
        List<Tag> tags = tagDao.getAllTags();
        assertEquals(tags.size(), 1);
    }

    @Test
    public void testEdit() throws Exception {
        long tagId = 1L;
        Tag tag = new Tag(tagId, TAG_NAME + TAG_NAME);
        tagDao.edit(tag);
        Tag actualTag = tagDao.getTagById(tagId);
        assertEquals(actualTag, tag);
    }

}