package com.epam.newsmanagement.services.impl;

import com.epam.newsmanagement.dao.impl.AuthorDaoImpl;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.AuthorService;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.util.List;

public class AuthorServiceImpl implements AuthorService {
    private static Logger logger = Logger.getLogger(AuthorServiceImpl.class);
    private AuthorDaoImpl authorDao;

    @Override
    public Long addAuthor(String authorName) throws ServiceException {
        Long authorId = null;
        try {
            if (authorDao.checkUniquenessAuthor(authorName)) {
                authorId = authorDao.create(authorName);
            }
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return authorId;
    }

    @Override
    public void addNewsAuthor(Long newsId, Long authorId) throws ServiceException {
        try {
            authorDao.addNewsAuthor(newsId, authorId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }


    @Override
    public Author getAuthorForNews(Long newsId) throws ServiceException {
        Author author = null;
        try {
            author = authorDao.getByNewsId(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return author;
    }

    @Override
    public List<Author> getAllAuthors() throws ServiceException {
        List<Author> authors = null;
        try {
            authors = authorDao.getAllAuthors();
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return authors;
    }

    @Override
    public void editAuthor(Author author) throws ServiceException {
        try {
            authorDao.edit(author);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteAuthor(Long authorId) throws ServiceException {
        try {
            authorDao.deleteDependencyWithNews(authorId);
            authorDao.delete(authorId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void expireAuthor(Long authorId) throws ServiceException {
        try {
            Timestamp expired = new Timestamp(new java.util.Date().getTime());
            authorDao.expire(authorId, expired);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Author getAuthorById(Long authorId) throws ServiceException {
        Author author = null;
        try {
            author = authorDao.getAuthorById(authorId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return author;
    }

    public void setAuthorDao(AuthorDaoImpl authorDao) {
        this.authorDao = authorDao;
    }
}
