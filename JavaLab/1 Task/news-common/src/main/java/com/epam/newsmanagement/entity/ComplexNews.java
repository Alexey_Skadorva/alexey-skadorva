package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.List;

public class ComplexNews implements Serializable {
    private static final long serialVersionUID = 1462432370486015638L;

    private News news;
    private Author author;
    private List<Tag> tags;
    private List<Comment> comments;

    public ComplexNews(News news, Author author, List<Tag> tags, List<Comment> comments) {
        this.news = news;
        this.author = author;
        this.tags = tags;
        this.comments = comments;
    }

    public ComplexNews() {

    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    @Override
    public String toString() {
        return news + " " + author + " " + tags + " " + comments;
    }

    public String toStringTags() {
        String tagsString = new String();
        for (int i = 0; i < tags.size(); i++) {
            if (i != tags.size() - 1) {
                tagsString += tags.get(i).getTagName() + ", ";
            } else {
                tagsString += tags.get(i).getTagName();
            }
        }
        return tagsString;
    }

    @Override
    public int hashCode() {
        int result = 31;
        result = result + news.hashCode();
        result = result + author.hashCode();
        result = result + comments.hashCode();
        result = result + tags.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ComplexNews complexNews = (ComplexNews) obj;
        if (news.equals(complexNews.news))
            return false;
        if (author.equals(complexNews.author))
            return false;
        if (!comments.equals(complexNews.comments))
            return false;
        if (!tags.equals(complexNews.tags))
            return false;
        return true;
    }
}
