package com.epam.newsmanagement.services.impl;

import com.epam.newsmanagement.dao.impl.CommentsDaoImpl;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.CommentsService;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.util.List;

public class CommentsServiceImpl implements CommentsService {
    private static Logger logger = Logger.getLogger(CommentsServiceImpl.class);
    private CommentsDaoImpl commentsDao = null;

    @Override
    public Long addComment(String commentText, Long newsId) throws ServiceException {
        Long commentId = null;
        try {
            Timestamp creationDate = new Timestamp(new java.util.Date().getTime());
            Comment comment = new Comment(commentText, newsId, creationDate);
            commentId = commentsDao.create(comment);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return commentId;
    }

    @Override
    public void deleteComment(Long commentId) throws ServiceException {
        try {
            commentsDao.delete(commentId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteCommentByNewsId(Long newsId) throws ServiceException {
        try {
            commentsDao.deleteByNewsId(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Comment> getCommentsForNews(Long newsId) throws ServiceException {
        List<Comment> comments = null;
        try {
            comments = commentsDao.getByNewsId(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return comments;
    }

    public void setCommentsDao(CommentsDaoImpl commentsDao) {
        this.commentsDao = commentsDao;
    }
}
