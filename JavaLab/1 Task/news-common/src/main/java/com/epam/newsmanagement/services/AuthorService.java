package com.epam.newsmanagement.services;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

public interface AuthorService {
    /**
     * Adds an existing news to the author.First create it in the table
     * Author, and then add the table News_Author relationship
     * between the author and news
     *
     * @throws ServiceException
     */
    void addNewsAuthor(Long newsId, Long authorId) throws ServiceException;

    /**
     * Adds author.If the author
     * of the same name does not exist, create it in the table
     * Author
     *
     * @throws ServiceException
     */
    Long addAuthor(String authorName) throws ServiceException;

    /**
     * Takes the news id and returns author
     * on this news
     *
     * @throws ServiceException
     */
    Author getAuthorForNews(Long newsId) throws ServiceException;

    /**
     * Returns all authors
     *
     * @return
     * @throws ServiceException
     */
    List<Author> getAllAuthors() throws ServiceException;

    /**
     * Changes the author who came on the parameters
     *
     * @param author
     * @throws ServiceException
     */
    void editAuthor(Author author) throws ServiceException;

    /**
     * Deletes author by id
     *
     * @param authorId
     * @throws ServiceException
     */
    void deleteAuthor(Long authorId) throws ServiceException;

    /**
     * Expire author by id
     *
     * @param authorId
     * @throws ServiceException
     */
    void expireAuthor(Long authorId) throws ServiceException;

    /**
     * Return author by id
     *
     * @param authorId
     * @return author
     * @throws ServiceException
     */
    Author getAuthorById(Long authorId) throws ServiceException;
}
