package com.epam.newsmanagement.utils;

import com.epam.newsmanagement.exception.DAOException;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CloseResources {
    public void close(PreparedStatement pst, ResultSet rs, Connection connection, DataSource dataSource) throws DAOException {
        try {

            if (rs != null) {
                rs.close();
            }
            close(pst, connection, dataSource);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public void close(PreparedStatement pst, Connection connection, DataSource dataSource)
            throws DAOException {
        try {
            if (pst != null) {
                pst.close();
            }
            if (connection != null) {
                DataSourceUtils.releaseConnection(connection, dataSource);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
