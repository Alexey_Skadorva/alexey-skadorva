package com.epam.newsmanagement.services.impl;

import com.epam.newsmanagement.dao.impl.NewsDaoImpl;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.NewsService;
import org.apache.log4j.Logger;

import java.util.List;


public class NewsServiceImpl implements NewsService {
    private static Logger logger = Logger.getLogger(NewsServiceImpl.class);
    private NewsDaoImpl newsDao = null;

    @Override
    public Long addNews(News news)
            throws ServiceException {
        Long insertId = null;
        try {
            insertId = newsDao.create(news);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return insertId;
    }

    @Override
    public void editNews(News news) throws ServiceException {
        try {
            newsDao.edit(news);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteNews(Long newsId) throws ServiceException {
        try {
            newsDao.delete(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteDependencyWithAuthor(Long newsId) throws ServiceException {
        try {
            newsDao.deleteDependencyWithAuthor(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteDependencyWithTags(Long newsId) throws ServiceException {
        try {
            newsDao.deleteDependencyWithTags(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }


    @Override
    public News get(Long newsId) throws ServiceException {
        News news = null;
        try {
            news = newsDao.get(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return news;
    }

    @Override
    public void editAuthorForNews(Long newsId, Long authorId) throws ServiceException {
        try {
            newsDao.editAuthorForNews(newsId, authorId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }


    public List<News> findByFilters(SearchCriteria searchCriteria, int pageId, int NUM_NEWS_ON_PAGE) throws ServiceException {
        List<News> news = null;
        try {
            news = newsDao.findNewsByFilters(searchCriteria, pageId, NUM_NEWS_ON_PAGE);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return news;
    }

    public Long countNumNews(SearchCriteria searchCriteria) throws ServiceException {
        Long number = null;
        try {
            number = newsDao.countNumNews(searchCriteria);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return number;
    }

    public void setNewsDao(NewsDaoImpl newsDao) {
        this.newsDao = newsDao;
    }
}
