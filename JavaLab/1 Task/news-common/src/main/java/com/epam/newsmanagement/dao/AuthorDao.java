package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;

import java.sql.Timestamp;
import java.util.List;

public interface AuthorDao {
    /**
     * Takes the input id of the author
     * and with the help of the query finds out his name
     *
     * @param authorId
     * @return name of author
     * @throws DAOException
     */
    Author getAuthorById(Long authorId) throws DAOException;

    /**
     * Takes the input name of the author
     * and with the help of the query finds out his id
     *
     * @param authorName
     * @return id of author
     * @throws DAOException
     */
    boolean checkUniquenessAuthor(String authorName) throws DAOException;

    /**
     * Takes the input id of the news
     * and with the help of the query finds out author for this news
     *
     * @param newsId
     * @return name of author
     * @throws DAOException
     */
    Author getByNewsId(Long newsId) throws DAOException;

    /**
     * Skid updated by table News_author. Making the connection between
     * the author and the author of news .author_id meets in Table Author,
     * and news_id related news in the table News
     *
     * @param newsId
     * @param authorId
     * @throws DAOException
     */
    void addNewsAuthor(Long newsId, Long authorId) throws DAOException;

    /**
     * Adds a new author. Insert it into a table Author. The author's name
     * is passed to the method, and id is automatically generated and
     * returned after insertion
     *
     * @param authorName
     * @return insert id
     * @throws DAOException
     */
    Long create(String authorName) throws DAOException;

    /**
     * Removesthe author of the Author table with a unique id.
     *
     * @param authorId
     * @throws DAOException
     */
    void delete(Long authorId) throws DAOException;

    /**
     * Removes dependency with author and news.
     *
     * @param authorId
     * @throws DAOException
     */
    void deleteDependencyWithNews(Long authorId) throws DAOException;

    /**
     * Returns all authors
     *
     * @return authors
     * @throws DAOException
     */
    List<Author> getAllAuthors() throws DAOException;

    /**
     * Changes the author who came on the parameters
     *
     * @param author
     * @throws DAOException
     */
    void edit(Author author) throws DAOException;

    /**
     * Expire author by id
     *
     * @param authorId
     * @throws DAOException
     */
    void expire(Long authorId, Timestamp expired) throws DAOException;
}
