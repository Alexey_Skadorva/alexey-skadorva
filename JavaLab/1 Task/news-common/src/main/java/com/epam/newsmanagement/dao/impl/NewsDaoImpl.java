package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.CloseResources;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.newsmanagement.utils.ColumnNames.*;

@Transactional
public class NewsDaoImpl implements NewsDao {
    private final static String ADD_NEWS = "INSERT INTO NEWS(NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,"
            + "MODIFICATION_DATE) VALUES(NEWS_SEQ.NEXTVAL,?,?,?,?,?)";
    private final static String EDIT_NEWS = "UPDATE NEWS SET TITLE=?,SHORT_TEXT=?,FULL_TEXT=?,"
            + "MODIFICATION_DATE=? WHERE NEWS_ID=?";
    private final static String EDIT_AUTHOR_FOR_NEWS = "UPDATE NEWS_AUTHOR SET AUTHOR_ID=? WHERE NEWS_ID=?";
    private final static String DELETE_NEWS = "DELETE FROM NEWS WHERE NEWS_ID=?";
    private final static String DELETE_DEPENDENCY_WITH_AUTHOR = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID=?";
    private final static String DELETE_DEPENDENCY_WITH_TAGS = "DELETE FROM NEWS_TAG WHERE NEWS_ID=?";
    private final static String GET_BY_ID = "SELECT NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE FROM NEWS"
            + " WHERE NEWS_ID=?";
    private DataSource dataSource;
    private CloseResources closeResources;

    @Override
    public Long create(News news) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        Long insertId = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            String[] id = {NEWS_ID};
            pst = connection.prepareStatement(ADD_NEWS, id);
            pst.setString(1, news.getTitle());
            pst.setString(2, news.getShortText());
            pst.setString(3, news.getFullText());
            pst.setTimestamp(4, news.getCreationDate());
            pst.setDate(5, news.getModificationDate());
            pst.executeUpdate();
            rs = pst.getGeneratedKeys();
            if (rs.next()) {
                insertId = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return insertId;
    }

    @Override
    public void edit(News news) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(EDIT_NEWS);
            pst.setLong(5, news.getNewsId());
            pst.setString(1, news.getTitle());
            pst.setString(2, news.getShortText());
            pst.setString(3, news.getFullText());
            pst.setDate(4, (Date) news.getModificationDate());
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }
    }

    @Override
    public void editAuthorForNews(Long newsId, Long authorId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(EDIT_AUTHOR_FOR_NEWS);
            pst.setLong(1, authorId);
            pst.setLong(2, newsId);
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }
    }

    @Override
    public void delete(Long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(DELETE_NEWS);
            pst.setLong(1, newsId);
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }

    }

    @Override
    public void deleteDependencyWithTags(Long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(DELETE_DEPENDENCY_WITH_TAGS);
            pst.setLong(1, newsId);
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }

    }

    @Override
    public void deleteDependencyWithAuthor(Long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(DELETE_DEPENDENCY_WITH_AUTHOR);
            pst.setLong(1, newsId);
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }

    }

    @Override
    public News get(Long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        News news = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(GET_BY_ID);
            pst.setLong(1, newsId);
            rs = pst.executeQuery();
            while (rs.next()) {
                news = buildNews(rs);
            }
        } catch (SQLException e) {
            System.out.print(e);
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return news;
    }

    public List<News> searchNewsByTags(List<Tag> tags) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        List<News> news = new ArrayList<>();
        if (tags.size() == 0) {
            return news;
        }
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT NEWS.NEWS_ID , NEWS.SHORT_TEXT ,  NEWS.FULL_TEXT , NEWS.TITLE , NEWS.CREATION_DATE , "
                    + "NEWS.MODIFICATION_DATE FROM NEWS WHERENEWS.news_id IN (SELECT NEWS_TAG.news_id "
                    + "FROM NEWS_TAG WHERE NEWS_TAG.tag_id IN (? ");
            for (int i = 0; i < tags.size() - 1; i++) {
                sql.append(",?");
            }
            sql.append(") GROUP BY NEWS_TAG.news_id HAVING COUNT(NEWS_TAG.tag_id) = ?)");
            pst = connection.prepareStatement(sql.toString());
            for (int i = 0; i < tags.size(); i++) {
                pst.setLong(i + 1, tags.get(i).getTagId());
            }
            pst.setInt(tags.size() + 1, tags.size());
            rs = pst.executeQuery();
            while (rs.next()) {
                news.add(buildNews(rs));
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);

        }
        return news;
    }

    @Override
    public List<News> findNewsByFilters(SearchCriteria searchCriteria, Integer pageIndex, int numNews)
            throws DAOException {
        PreparedStatement pst = null;
        Connection connection = null;
        ResultSet rs = null;
        List<News> news = new ArrayList<News>();
        Long startIndex, endIndex;
        if (pageIndex != 1) {
            startIndex = (long) ((pageIndex - 1) * numNews + 1);
            endIndex = startIndex + numNews - 1;
        } else {
            startIndex = 1L;
            endIndex = (long) numNews;
        }
        try {
            StringBuilder findNews = buildCriteria(searchCriteria);

            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(findNews.toString());
            int insertParam = 1;
            if (searchCriteria.getAuthorId() != null) {
                pst.setLong(insertParam, searchCriteria.getAuthorId());
                insertParam++;
            }
            if (searchCriteria.getTagsId() != null && !searchCriteria.getTagsId().isEmpty()) {
                for (int i = 0; i < searchCriteria.getTagsId().size(); i++) {
                    pst.setLong(insertParam, searchCriteria.getTagsId().get(i));
                    insertParam++;
                }
                pst.setLong(insertParam, searchCriteria.getTagsId().size());
                insertParam++;
            }
            pst.setLong(insertParam, startIndex);
            insertParam++;
            pst.setLong(insertParam, endIndex);
            rs = pst.executeQuery();
            while (rs.next()) {
                Long newsId = rs.getLong(1);
                String shortText = rs.getString(2);
                String title = rs.getString(3);
                Timestamp creationDate = rs.getTimestamp(4);
                Date modificationDate = rs.getDate(5);
                news.add(new News(newsId, shortText, null, title, creationDate, modificationDate));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return news;
    }

    @Override
    public Long countNumNews(SearchCriteria searchCriteria) throws DAOException {
        PreparedStatement pst = null;
        Connection connection = null;
        ResultSet rs = null;
        Long newsNum = null;
        try {
            StringBuilder countNumOfNews = new StringBuilder("SELECT COUNT(newsId) FROM ( "
                    + "SELECT newsId FROM ( "
                    + "SELECT news.news_id newsId FROM news ");

            if (searchCriteria.getAuthorId() != null) {
                countNumOfNews.append("INNER JOIN news_author ON news.news_id=news_author.news_id "
                        + "WHERE news_author.author_id=? ");
            }
            countNumOfNews.append(") ");
            if (searchCriteria.getTagsId() != null && !searchCriteria.getTagsId().isEmpty()) {

                countNumOfNews.append(" JOIN news_tag on news_tag.news_id = newsId WHERE news_tag.tag_id IN( ");
                for (int i = 0; i < searchCriteria.getTagsId().size() - 1; i++) {
                    countNumOfNews.append("?, ");
                }
                countNumOfNews.append("? ) GROUP BY newsId HAVING count(*)=? ");
            }
            countNumOfNews.append(") ");
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(countNumOfNews.toString());
            int insertParam = 1;
            if (searchCriteria.getAuthorId() != null) {
                pst.setLong(insertParam, searchCriteria.getAuthorId());
                insertParam++;
            }
            if (searchCriteria.getTagsId() != null && !searchCriteria.getTagsId().isEmpty()) {
                for (int i = 0; i < searchCriteria.getTagsId().size(); i++) {
                    pst.setLong(insertParam, searchCriteria.getTagsId().get(i));
                    insertParam++;
                }
                pst.setLong(insertParam, searchCriteria.getTagsId().size());
                insertParam++;
            }
            rs = pst.executeQuery();
            if (rs.next()) {
                newsNum = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return newsNum;
    }

    private News buildNews(ResultSet rs) throws SQLException {
        return new News(rs.getLong(NEWS_ID),
                rs.getString(SHORT_TEXT),
                rs.getString(FULL_TEXT),
                rs.getString(TITLE),
                rs.getTimestamp(CREATION_DATE),
                rs.getDate(MODIFICATION_DATE));
    }

    private StringBuilder buildCriteria(SearchCriteria searchCriteria) {
        StringBuilder findNews = new StringBuilder("SELECT newsId, newsShortText, newsTitle, newsCreationDate,"
                + " newsModificationDate FROM ( "
                + "SELECT newsId, total_message, newsShortText, newsTitle, newsCreationDate, "
                + "newsModificationDate, rownum rn FROM ( "
                + "SELECT newsId, newsShortText, newsTitle, newsCreationDate, newsModificationDate, "
                + "COUNT (COMMENTS.comment_id) as total_message  FROM ( "
                + "SELECT newsId, newsShortText, newsTitle, newsCreationDate, "
                + "newsModificationDate FROM ( "
                + "SELECT NEWS.news_id newsId, NEWS.short_text newsShortText, NEWS.title newsTitle, "
                + "NEWS.creation_date newsCreationDate, NEWS.modification_date newsModificationDate FROM NEWS ");

        if (searchCriteria.getAuthorId() != null) {
            findNews.append("INNER JOIN NEWS_AUTHOR ON NEWS.news_id=NEWS_AUTHOR.news_id "
                    + "WHERE NEWS_AUTHOR.author_id=? ");
        }

        findNews.append(" ) ");

        if (searchCriteria.getTagsId() != null && !searchCriteria.getTagsId().isEmpty()) {

            findNews.append(" JOIN NEWS_TAG on NEWS_TAG.news_id = newsId WHERE NEWS_TAG.tag_id IN( ");
            for (int i = 0; i < searchCriteria.getTagsId().size() - 1; i++) {
                findNews.append("?, ");
            }
            findNews.append("? ) GROUP BY newsId, newsShortText, newsTitle, newsCreationDate, newsModificationDate "
                    + "HAVING count(*)=?");
        }
        findNews.append(" ) ");
        findNews.append("LEFT JOIN COMMENTS ON COMMENTS.NEWS_ID= newsId "
                + "GROUP BY newsId, newsShortText, newsTitle, newsCreationDate, newsModificationDate "
                + "ORDER BY COUNT(COMMENTS.comment_id) DESC, newsModificationDate  DESC )   ) "
                + "WHERE rn BETWEEN ? AND ? ");
        return findNews;

    }

    public void setCloseResources(CloseResources closeResources) {
        this.closeResources = closeResources;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
