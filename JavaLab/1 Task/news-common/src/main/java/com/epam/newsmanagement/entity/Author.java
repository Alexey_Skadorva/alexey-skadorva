package com.epam.newsmanagement.entity;

import java.io.Serializable;

public class Author implements Serializable {
    private static final long serialVersionUID = -933563462559904791L;
    private Long authorId;
    private String authorName;

    public Author(Long authorId, String authorName) {
        this.authorId = authorId;
        this.authorName = authorName;
    }

    public Author() {

    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    @Override
    public String toString() {
        return authorId + " " + authorName;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = (int) (result + authorId);
        result = result + authorName.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Author author = (Author) obj;
        if (authorId != author.authorId)
            return false;
        if (!authorName.equals(author.authorName))
            return false;
        return true;
    }
}
