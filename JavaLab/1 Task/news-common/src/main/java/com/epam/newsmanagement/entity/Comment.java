package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.sql.Timestamp;

public class Comment implements Serializable {
    private static final long serialVersionUID = -7647808599920180493L;
    private Long commentId;
    private Long newsId;
    private String commentText;
    private Timestamp creationDate;

    public Comment(Long commentId, Long newsId, String commentText,
                   Timestamp creationDate) {
        this.commentId = commentId;
        this.newsId = newsId;
        this.commentText = commentText;
        this.creationDate = creationDate;
    }

    public Comment(String commentText, Long newsId, Timestamp creationDate) {
        this.newsId = newsId;
        this.commentText = commentText;
        this.creationDate = creationDate;
    }


    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return commentId + " " + newsId + " " + commentText + " "
                + creationDate;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = (int) (result + commentId);
        result = (int) (result + newsId);
        result = result + commentText.hashCode();
        result = result + creationDate.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Comment comment = (Comment) obj;
        if (commentId != comment.commentId)
            return false;
        if (newsId != comment.newsId)
            return false;
        if (!commentText.equals(comment.commentText))
            return false;
        if (!comment.creationDate.equals(creationDate))
            return false;
        return true;
    }
}
