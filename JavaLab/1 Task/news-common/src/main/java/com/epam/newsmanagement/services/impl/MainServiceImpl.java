package com.epam.newsmanagement.services.impl;

import com.epam.newsmanagement.entity.*;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.MainService;

import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class MainServiceImpl implements MainService {
    private NewsServiceImpl newsService = null;
    private AuthorServiceImpl authorService = null;
    private TagServiceImpl tagService = null;
    private CommentsServiceImpl commentsService = null;

    @Override
    public Long addNews(News news, Long authorId, List<Long> tags, Date date) throws ServiceException {
        Timestamp creationDate = new Timestamp(date.getTime());
        Date modificationDate = new java.sql.Date(date.getTime());
        news.setModificationDate(modificationDate);
        news.setCreationDate(creationDate);
        Long insertId = newsService.addNews(news);
        authorService.addNewsAuthor(insertId, authorId);
        if (tags != null) {
            tagService.addNewsTag(insertId, tags);
        }
        return insertId;
    }

    @Override
    public void deleteNews(List<Long> newsIds) throws ServiceException {
        for (Long id : newsIds) {
            commentsService.deleteCommentByNewsId(id);
            newsService.deleteDependencyWithAuthor(id);
            newsService.deleteDependencyWithTags(id);
            newsService.deleteNews(id);
        }
    }

    @Override
    public Long saveNews(News news, Long authorId, List<Long> tags, Date date) throws ServiceException {
        Long newsId = news.getNewsId();
        if (newsId == null) {
            newsId = addNews(news, authorId, tags, date);
        } else {
            editNews(news, authorId, tags, date);
        }
        return newsId;
    }

    @Override
    public void editNews(News news, Long authorId, List<Long> tags, Date date) throws ServiceException {
        Date modificationDate = new java.sql.Date(date.getTime());
        news.setModificationDate(modificationDate);
        newsService.editNews(news);
        newsService.deleteDependencyWithTags(news.getNewsId());
        if (tags != null) {
            tagService.addNewsTag(news.getNewsId(), tags);
        }
        newsService.editAuthorForNews(news.getNewsId(), authorId);
    }

    @Override
    public List<ComplexNews> findByFilters(SearchCriteria criteria, int pageId, int numberNewsOnPage) throws ServiceException {
        List<ComplexNews> complexNews = new ArrayList<>();
        List<News> news = newsService.findByFilters(criteria, pageId, numberNewsOnPage);
        for (int i = 0; i < news.size(); i++) {
            List<Comment> comments = commentsService.getCommentsForNews(news.get(i).getNewsId());
            List<Tag> tags = tagService.getTagsForNews(news.get(i).getNewsId());
            Author author = authorService.getAuthorForNews(news.get(i).getNewsId());
            complexNews.add(new ComplexNews(news.get(i), author, tags, comments));
        }
        return complexNews;
    }

    @Override
    public ComplexNews getAllInfoAboutNews(Long newsId) throws ServiceException {
        News news = newsService.get(newsId);
        List<Comment> comments = commentsService.getCommentsForNews(newsId);
        List<Tag> tags = tagService.getTagsForNews(newsId);
        Author author = authorService.getAuthorForNews(newsId);
        ComplexNews complexNews = new ComplexNews(news, author, tags, comments);
        return complexNews;
    }

    public Long findPreviousNewsId(Long currNewsId, HttpSession session, int newsOnPage) throws ServiceException {

        List<Long> newsIds = (List<Long>) session.getAttribute("newsIds");
        Integer pageId = (Integer) session.getAttribute("pageId");
        if (pageId == null || newsIds == null) {
            return null;
        }
        int currIndex = newsIds.indexOf(currNewsId);
        Long previousNewsIndex = null;

        if (currIndex - 1 >= 0) {
            previousNewsIndex = newsIds.get(currIndex - 1);
        } else if (currIndex - 1 < 0 && pageId != 1) {
            SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
            List<News> news = newsService.findByFilters(searchCriteria, pageId - 1, newsOnPage);
            for (int i = 0; i < news.size(); i++) {
                newsIds.add(i, news.get(i).getNewsId());
                currIndex++;
            }
            session.setAttribute("newsIds", newsIds);
            previousNewsIndex = newsIds.get(currIndex - 1);
        }
        return previousNewsIndex;
    }

    public Long findNextNewsId(Long currNewsId, HttpSession session, int numOfPages, int newsOnPage) throws ServiceException {
        List<Long> newsIds = (List<Long>) session.getAttribute("newsIds");
        Integer pageId = (Integer) session.getAttribute("pageId");
        if (pageId == null || newsIds == null) {
            return null;
        }
        int currIndex = newsIds.indexOf(currNewsId);
        Long nextNewsIndex = null;

        if (currIndex + 1 < newsIds.size()) {
            nextNewsIndex = newsIds.get(currIndex + 1);
        } else if (currIndex + 1 == newsIds.size() && pageId != numOfPages) {
            SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");

            List<News> news = newsService.findByFilters(searchCriteria, pageId + 1, newsOnPage);
            for (int i = 0; i < news.size(); i++) {
                newsIds.add(news.get(i).getNewsId());
            }
            session.setAttribute("newsIds", newsIds);
            nextNewsIndex = newsIds.get(currIndex + 1);
        }
        return nextNewsIndex;
    }

    public void setAuthorService(AuthorServiceImpl authorService) {
        this.authorService = authorService;
    }

    public void setTagService(TagServiceImpl tagService) {
        this.tagService = tagService;
    }

    public void setCommentsService(CommentsServiceImpl commentsService) {
        this.commentsService = commentsService;
    }

    public void setNewsService(NewsServiceImpl newsService) {
        this.newsService = newsService;
    }
}
