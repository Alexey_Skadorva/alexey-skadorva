package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.CommentsDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.CloseResources;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.newsmanagement.utils.ColumnNames.*;

@Transactional
public class CommentsDaoImpl implements CommentsDao {
    private final static String ADD_COMMENT = "INSERT INTO COMMENTS(COMMENT_ID,"
            + "COMMENT_TEXT,CREATION_DATE,NEWS_ID) VALUES(COMMENTS_SEQ.NEXTVAL,?,?,?)";
    private final static String DELETE_COMMENT = "DELETE FROM COMMENTS WHERE COMMENT_ID=?";
    private final static String GET_BY_ID = "SELECT COMMENT_TEXT,CREATION_DATE,NEWS_ID FROM COMMENTS"
            + " WHERE COMMENT_ID=?";
    private final static String GET_BY_NEWS_ID = "SELECT COMMENT_TEXT,CREATION_DATE,COMMENT_ID FROM COMMENTS"
            + " WHERE NEWS_ID=?";
    private final static String DELETE_COMMENTS_OF_NEWS = "DELETE FROM COMMENTS WHERE NEWS_ID=?";
    private DataSource dataSource;
    private CloseResources closeResources;

    @Override
    public Long create(Comment comment) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        Long insertId = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            String[] id = {COMMENT_ID};
            pst = connection.prepareStatement(ADD_COMMENT, id);
            pst.setString(1, comment.getCommentText());
            pst.setTimestamp(2, comment.getCreationDate());
            pst.setLong(3, comment.getNewsId());
            pst.executeUpdate();
            rs = pst.getGeneratedKeys();
            if (rs.next()) {
                insertId = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return insertId;
    }

    @Override
    public void delete(Long commentId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(DELETE_COMMENT);
            pst.setLong(1, commentId);
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }
    }

    @Override
    public Comment get(Long commentId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        Comment comment = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(GET_BY_ID);
            pst.setLong(1, commentId);
            rs = pst.executeQuery();
            while (rs.next()) {
                Long newsId = rs.getLong(NEWS_ID);
                String commentText = rs.getString(COMMENT_TEXT);
                Timestamp creationDate = rs.getTimestamp(CREATION_DATE);
                comment = new Comment(commentId, newsId, commentText, creationDate);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return comment;
    }

    @Override
    public List<Comment> getByNewsId(Long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        List<Comment> comments = new ArrayList<Comment>();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(GET_BY_NEWS_ID);
            pst.setLong(1, newsId);
            rs = pst.executeQuery();
            while (rs.next()) {
                Long commentId = rs.getLong(COMMENT_ID);
                String commentText = rs.getString(COMMENT_TEXT);
                Timestamp creationDate = rs.getTimestamp(CREATION_DATE);
                comments.add(new Comment(commentId, newsId, commentText, creationDate));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return comments;
    }

    @Override
    public void deleteByNewsId(Long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(DELETE_COMMENTS_OF_NEWS);
            pst.setLong(1, newsId);
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }

    }

    public void setCloseResources(CloseResources closeResources) {
        this.closeResources = closeResources;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
