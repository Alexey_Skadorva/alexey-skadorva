package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;

public interface CommentsDao {
    /**
     * Takes the input id of the comment
     * and with the help of the query finds out his
     *
     * @param commentId
     * @return comment
     * @throws DAOException
     */
    Comment get(Long commentId) throws DAOException;


    /**
     * Adds a new comment. Insert it into a table Comments.
     *
     * @param comment
     * @return id
     * @throws DAOException
     */
    Long create(Comment comment) throws DAOException;

    /**
     * Removes the comment of the Comments table with a unique id.
     *
     * @param commentId
     * @throws DAOException
     */
    void delete(Long commentId) throws DAOException;
}
