package com.epam.newsmanagement.dao.hibernate;


import com.epam.newsmanagement.dao.UserDao;
import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class UserDaoImpl implements UserDao {

    @Override
    public User findByLogin(String login) throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        User user = null;
        try {
            Criteria criteria = session.createCriteria(User.class);
            user = (User) criteria.add(Restrictions.eq("login", login)).uniqueResult();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
        return user;
    }

    @Override
    public String findUserRole(Long userId) throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Role role = null;
        try {
            role = (Role) session.get(Role.class, userId);
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
        return role.getRoleName();
    }

    private void closeSession(Session session) {
        if (session.isOpen()) {
            session.close();
        }
    }
}