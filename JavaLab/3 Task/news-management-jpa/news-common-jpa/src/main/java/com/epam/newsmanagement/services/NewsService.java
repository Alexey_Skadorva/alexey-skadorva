package com.epam.newsmanagement.services;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;

import java.sql.Date;
import java.util.List;

public interface NewsService {
    /**
     * Adds a news table News. id news is generated
     * automatically, and all other fields  are passed to the method
     *
     * @param news
     * @throws ServiceException
     */
    Long addNews(News news) throws ServiceException;

    /**
     * Adds a news table News. id news is generated
     * automatically, and all other fields  are passed to the method
     *
     * @param news
     * @throws ServiceException
     */
    Long saveNews(News news, Date date, List<Long> tags, Long authorId) throws ServiceException;

    /**
     * Changes all fields news. Changes occur on the
     * unique id, and all other fields are changed, except for the
     * field creation_date
     *
     * @param news
     * @throws ServiceException
     */
    void editNews(News news) throws ServiceException;

    /**
     * Deletes a news table News takes on a unique id
     *
     * @param newsId
     * @throws ServiceException
     */
    void deleteNews(List<Long> newsId) throws ServiceException;

    /**
     * Takes the input id of the news and finds out his
     *
     * @param newsId
     * @return news
     * @throws ServiceException
     */
    News getNews(Long newsId) throws ServiceException;

    /**
     * Searches for news on the parameters
     *
     * @param searchCriteria,pageId
     * @param NUM_NEWS_ON_PAGE
     * @return news
     * @throws ServiceException
     */
    List<News> findByFilters(SearchCriteria searchCriteria, int pageId, int NUM_NEWS_ON_PAGE)
            throws ServiceException;

    /**
     * Count the news that within the parameters
     *
     * @param searchCriteria
     * @return number
     * @throws ServiceException
     */
    int countNumNews(SearchCriteria searchCriteria) throws ServiceException;
}
