package com.epam.newsmanagement.services;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

public interface AuthorService {
    /**
     * Adds author.If the author
     * of the same name does not exist, create it in the table
     * Author
     *
     * @throws ServiceException
     */
    Long addAuthor(String authorName) throws ServiceException;

    /**
     * Returns all authors
     *
     * @return
     * @throws ServiceException
     */
    List<Author> getAllAuthors() throws ServiceException;


    void expireAuthor(Long authorId) throws ServiceException;

    /**
     * Changes the author who came on the parameters
     *
     * @param author
     * @throws ServiceException
     */
    void editAuthor(Author author) throws ServiceException;

    /**
     * Return author by id
     *
     * @param authorId
     * @return author
     * @throws ServiceException
     */
    Author getAuthor(Long authorId) throws ServiceException;
}
