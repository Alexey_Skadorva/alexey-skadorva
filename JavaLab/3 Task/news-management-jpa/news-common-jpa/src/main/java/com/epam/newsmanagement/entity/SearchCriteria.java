package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Alexey_Skadorva on 7/9/2015.
 */
public class SearchCriteria implements Serializable {
    private static final long serialVersionUID = 1462432370486015638L;

    private Long authorId;
    private List<Long> tagsId;

    public SearchCriteria(Long authorId, List<Long> tagsId) {
        this.authorId = authorId;
        this.tagsId = tagsId;
    }

    public SearchCriteria() {

    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public List<Long> getTagsId() {
        return tagsId;
    }

    public void setTagsId(List<Long> tagsId) {
        this.tagsId = tagsId;
    }

    @Override
    public int hashCode() {
        int result = 31;
        result += authorId;
        result += tagsId.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SearchCriteria searchCriteria = (SearchCriteria) obj;
        if (authorId != searchCriteria.authorId)
            return false;
        if (!tagsId.equals(searchCriteria.tagsId))
            return false;
        return true;
    }
}
