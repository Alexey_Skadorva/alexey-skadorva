package com.epam.newsmanagement.utils;

import com.epam.newsmanagement.exception.DAOException;

import javax.persistence.EntityManager;

public class DaoUtil {
    public static void closeManager(EntityManager manager) throws DAOException {
        if ((manager != null) && manager.isOpen()) {
            try {
                manager.close();
            } catch (IllegalStateException e) {
                throw new DAOException(e);
            }
        }
    }
}
