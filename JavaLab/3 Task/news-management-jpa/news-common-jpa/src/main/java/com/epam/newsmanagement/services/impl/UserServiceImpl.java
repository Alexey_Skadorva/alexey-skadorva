package com.epam.newsmanagement.services.impl;

import com.epam.newsmanagement.dao.UserDao;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.UserService;
import org.apache.log4j.Logger;

public class UserServiceImpl implements UserService {
    private static Logger logger = Logger.getLogger(UserServiceImpl.class);
    private UserDao userDao;

    @Override
    public User findUser(String login) throws ServiceException {
        User user = null;
        try {
            user = userDao.findByLogin(login);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return user;
    }

    @Override
    public String findUserRole(Long id) throws ServiceException {
        String role = null;
        try {
            role = userDao.findUserRole(id);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return role;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
