package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;

import java.util.List;

public interface AuthorDao {
    /**
     * Takes the input id of the author
     * and with the help of the query finds out his name
     *
     * @param authorId
     * @return name of author
     * @throws DAOException
     */
    Author get(Long authorId) throws DAOException;

    /**
     * Adds a new author. Insert it into a table Author. The author's name
     * is passed to the method, and id is automatically generated and
     * returned after insertion
     *
     * @param authorName
     * @return insert id
     * @throws DAOException
     */
    Long create(String authorName) throws DAOException;

    /**
     * Returns all authors
     *
     * @return authors
     * @throws DAOException
     */
    List<Author> getAllAuthors() throws DAOException;

    /**
     * Changes the author who came on the parameters
     *
     * @param author
     * @throws DAOException
     */
    void edit(Author author) throws DAOException;
}
