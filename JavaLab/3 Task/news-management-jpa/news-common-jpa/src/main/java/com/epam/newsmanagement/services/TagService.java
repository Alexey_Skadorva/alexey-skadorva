package com.epam.newsmanagement.services;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

public interface TagService {
    /**
     * Adds the tag.If the tag does not exist, it is
     * created in the table Tag
     *
     * @param tagName
     * @return
     * @throws ServiceException
     */
    Long addTag(String tagName) throws ServiceException;

    /**
     * Returns all tags
     *
     * @return tags
     * @throws ServiceException
     */
    List<Tag> getAllTags() throws ServiceException;

    /**
     * Changes came tag
     *
     * @param tag
     * @throws ServiceException
     */
    void editTag(Tag tag) throws ServiceException;

    /**
     * Deletes tag by id
     *
     * @param tagId
     * @throws ServiceException
     */
    void deleteTag(Long tagId) throws ServiceException;

    /**
     * Returns tag by id
     *
     * @param tagId
     * @return
     * @throws ServiceException
     */
    Tag getTag(Long tagId) throws ServiceException;
}
