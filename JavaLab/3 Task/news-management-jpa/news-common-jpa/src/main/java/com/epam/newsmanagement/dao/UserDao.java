package com.epam.newsmanagement.dao;


import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;

public interface UserDao {

    /**
     * Finds user by login
     *
     * @param login
     * @return
     * @throws DAOException
     */
    User findByLogin(String login) throws DAOException;

    /**
     * Finds user role
     *
     * @param id
     * @return
     * @throws DAOException
     */
    String findUserRole(Long id) throws DAOException;
}
