package com.epam.newsmanagement.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "ROLES")
public class Role implements Serializable {
    @Id
    @Column(name = "USER_ID")
    Long userId;
    @Column(name = "ROLE_NAME")
    String roleName;

    public Role() {
    }

    public Role(Long userId, String roleName) {
        this.userId = userId;
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Role role = (Role) obj;
        if (userId != role.userId)
            return false;
        if (!roleName.equals(role.roleName))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = 31;
        result += userId;
        result += roleName.hashCode();
        return result;
    }
}