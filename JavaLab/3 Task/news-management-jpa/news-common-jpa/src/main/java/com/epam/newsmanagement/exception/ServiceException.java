package com.epam.newsmanagement.exception;

public class ServiceException extends Exception {
    private static final long serialVersionUID = 1L;

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(Throwable e) {
        super(e.getMessage(), e);
    }

    public ServiceException(String message, Throwable e) {
        super(message, e);
    }
}
