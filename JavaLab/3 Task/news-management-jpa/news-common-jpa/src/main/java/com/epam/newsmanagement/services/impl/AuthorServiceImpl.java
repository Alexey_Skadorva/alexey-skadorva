package com.epam.newsmanagement.services.impl;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.AuthorService;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.util.List;

public class AuthorServiceImpl implements AuthorService {
    private static Logger logger = Logger.getLogger(AuthorServiceImpl.class);
    private AuthorDao authorDao;

    @Override
    public Long addAuthor(String authorName) throws ServiceException {
        Long authorId = null;
        try {
            authorId = authorDao.create(authorName);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return authorId;
    }

    @Override
    public List<Author> getAllAuthors() throws ServiceException {
        List<Author> authors = null;
        try {
            authors = authorDao.getAllAuthors();
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return authors;
    }

    @Override
    public void expireAuthor(Long authorId) throws ServiceException {
        try {
            Author author = authorDao.get(authorId);
            Timestamp expired = new Timestamp(new java.util.Date().getTime());
            author.setExpired(expired);
            authorDao.edit(author);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void editAuthor(Author author) throws ServiceException {
        try {
            authorDao.edit(author);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Author getAuthor(Long authorId) throws ServiceException {
        Author author = null;
        try {
            author = authorDao.get(authorId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return author;
    }

    public void setAuthorDao(AuthorDao authorDao) {
        this.authorDao = authorDao;
    }
}
