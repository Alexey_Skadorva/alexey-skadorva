package com.epam.newsmanagement.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "USERS")
public class User implements Serializable {
    private static final long serialVersionUID = -1340917654956509396L;

    @Id
    @GeneratedValue(generator = "user_seq")
    @SequenceGenerator(name = "user_seq", sequenceName = "USER_SEQ", allocationSize = 1)
    @Column(name = "USER_ID", nullable = false, unique = true)
    private Long id;

    @Column(name = "USER_NAME")
    private String name;

    @Column(name = "LOGIN")
    private String login;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "EXPIRED")
    private Timestamp expired;

    public User(Long id, String name, String login, String password, Timestamp expired) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.password = password;
        this.expired = expired;
    }

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Timestamp getExpired() {
        return expired;
    }

    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User user = (User) obj;
        if (id != user.id)
            return false;
        if (!name.equals(user.name))
            return false;
        if (!login.equals(user.login))
            return false;
        if (!password.equals(user.password))
            return false;
        if (!expired.equals(user.expired))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = 31;
        result += id;
        result += login.hashCode();
        result += password.hashCode();
        result += name.hashCode();
        result += expired.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return id + name + login + password + expired;
    }
}
