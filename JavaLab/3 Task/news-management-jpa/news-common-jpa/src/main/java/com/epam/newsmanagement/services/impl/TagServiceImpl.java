package com.epam.newsmanagement.services.impl;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.TagService;
import org.apache.log4j.Logger;

import java.util.List;

public class TagServiceImpl implements TagService {
    private static Logger logger = Logger.getLogger(TagServiceImpl.class);
    private TagDao tagDao;

    @Override
    public Long addTag(String tagName) throws ServiceException {
        Long tagId = null;
        try {
            tagId = tagDao.create(tagName);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return tagId;
    }

    @Override
    public void editTag(Tag tag) throws ServiceException {
        try {
            tagDao.edit(tag);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteTag(Long tagId) throws ServiceException {
        try {
            tagDao.delete(tagId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Tag getTag(Long tagId) throws ServiceException {
        Tag tag = null;
        try {
            tag = tagDao.get(tagId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return tag;
    }

    @Override
    public List<Tag> getAllTags() throws ServiceException {
        List<Tag> tags = null;
        try {
            tags = tagDao.getAllTags();
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return tags;
    }

    public void setTagDao(TagDao tagDao) {
        this.tagDao = tagDao;
    }
}
