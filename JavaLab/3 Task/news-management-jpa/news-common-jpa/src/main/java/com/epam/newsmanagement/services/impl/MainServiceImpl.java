package com.epam.newsmanagement.services.impl;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.MainService;

import javax.servlet.http.HttpSession;
import java.util.List;

public class MainServiceImpl implements MainService {
    private NewsServiceImpl newsService = null;

    public Long findPreviousNewsId(Long currNewsId, HttpSession session, int newsOnPage) throws ServiceException {

        List<Long> newsIds = (List<Long>) session.getAttribute("newsIds");
        Integer pageId = (Integer) session.getAttribute("pageId");
        if (pageId == null || newsIds == null) {
            return null;
        }
        int currIndex = newsIds.indexOf(currNewsId);
        Long previousNewsIndex = null;

        if (currIndex - 1 >= 0) {
            previousNewsIndex = newsIds.get(currIndex - 1);
        } else if (currIndex - 1 < 0 && pageId != 1) {
            SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
            List<News> news = newsService.findByFilters(searchCriteria, pageId - 1, newsOnPage);
            for (int i = 0; i < news.size(); i++) {
                newsIds.add(i, news.get(i).getNewsId());
                currIndex++;
            }
            session.setAttribute("newsIds", newsIds);
            previousNewsIndex = newsIds.get(currIndex - 1);
        }
        return previousNewsIndex;
    }

    public Long findNextNewsId(Long currNewsId, HttpSession session, int numOfPages, int newsOnPage) throws ServiceException {
        List<Long> newsIds = (List<Long>) session.getAttribute("newsIds");
        Integer pageId = (Integer) session.getAttribute("pageId");
        if (pageId == null || newsIds == null) {
            return null;
        }
        int currIndex = newsIds.indexOf(currNewsId);
        Long nextNewsIndex = null;

        if (currIndex + 1 < newsIds.size()) {
            nextNewsIndex = newsIds.get(currIndex + 1);
        } else if (currIndex + 1 == newsIds.size() && pageId != numOfPages) {
            SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");

            List<News> news = newsService.findByFilters(searchCriteria, pageId + 1, newsOnPage);
            for (int i = 0; i < news.size(); i++) {
                newsIds.add(news.get(i).getNewsId());
            }
            session.setAttribute("newsIds", newsIds);
            nextNewsIndex = newsIds.get(currIndex + 1);
        }
        return nextNewsIndex;
    }

    public void setNewsService(NewsServiceImpl newsService) {
        this.newsService = newsService;
    }
}

