package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DAOException;

import java.util.List;

public interface NewsDao {
    /**
     * Adds a new news in table News
     *
     * @param news
     * @return insert id
     * @throws DAOException
     */
    Long create(News news) throws DAOException;

    /**
     * Changes the news table News. Change fields:
     * title, short_text, full_text and modification_date
     *
     * @param news
     * @throws DAOException
     */
    void edit(News news) throws DAOException;

    /**
     * Removes the news of the News table with a unique id.
     *
     * @param newsId of news
     * @throws DAOException
     */
    void delete(Long newsId) throws DAOException;

    /**
     * Takes the input id of the news
     * and with the help of the query finds out his
     *
     * @param newsId
     * @return news
     * @throws DAOException
     */
    News get(Long newsId) throws DAOException;

    /**
     * Searches for news on the parameters
     *
     * @param searchCriteria,pageIndex,numNews
     * @return news
     * @throws DAOException
     */
    List<News> findNewsByFilters(SearchCriteria searchCriteria, Integer pageIndex, int numNews)
            throws DAOException;

    /**
     * Count the news that within the parameters
     *
     * @param searchCriteria
     * @return number
     * @throws DAOException
     */
    int countNumNews(SearchCriteria searchCriteria) throws DAOException;
}
