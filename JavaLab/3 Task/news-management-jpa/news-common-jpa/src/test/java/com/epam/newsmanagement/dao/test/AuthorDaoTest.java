package com.epam.newsmanagement.dao.test;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-configuration-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:news.xml")
public class AuthorDaoTest {
    private final static String AUTHOR_NAME = "text";

    @Autowired
    private AuthorDao authorDao;

    public AuthorDaoTest() {
        super();
    }

    @Test
    public void testCreateAuthor() throws Exception {
        Long insertId = authorDao.create(AUTHOR_NAME);
        System.out.print(insertId);
        Author author = authorDao.get(insertId);
        assertEquals(author, new Author(insertId, AUTHOR_NAME, null));
    }

    @Test
    public void testGetAllAuthors() throws Exception {
        List<Author> authors = authorDao.getAllAuthors();
        assertEquals(authors.size(), 1);
    }

    @Test
    public void testGetAuthor() throws Exception {
        Long authorId = 1L;
        Author author = authorDao.get(authorId);
        System.out.print(author.toString());
        Author actualAuthor = new Author(authorId, AUTHOR_NAME, null);
        assertEquals(author, actualAuthor);
    }

    @Test
    public void testEditAuthor() throws Exception {
        Long authorId = 1L;
        Author author = new Author(authorId, AUTHOR_NAME + AUTHOR_NAME, null);
        authorDao.edit(author);
        Author actualAuthor = authorDao.get(authorId);
        assertEquals(author, actualAuthor);
    }
}