package com.epam.newsmanagement.dao.test;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.dao.CommentsDao;
import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-configuration-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:news.xml")
public class NewsDaoTest {
    private final static String TEXT = "text";
    @Autowired
    NewsDao newsDao;
    @Autowired
    CommentsDao commentsDao;
    @Autowired
    TagDao tagDao;
    @Autowired
    AuthorDao authorDao;

    public NewsDaoTest() {
        super();
    }


    @Test
    public void testAddNews() throws Exception {
        Long newsId = 1L;
        Author author = authorDao.get(1L);
        List<Tag> tags = new ArrayList<>();
        tags.add(tagDao.get(1L));
        News news = new News(null, TEXT, TEXT, TEXT, new Timestamp(1427369733), new Date(1427369733), author, tags, null);
        Long insertId = newsDao.create(news);
        News actualNews = newsDao.get(insertId);
        actualNews.setNewsId(insertId);
        assertEquals(actualNews.toString(), news.toString());
    }

    @Test
    public void testEditNews() throws Exception {
        Long newsId = 1L;
        Author author = authorDao.get(1L);
        List<Tag> tags = new ArrayList<>();
        tags.add(tagDao.get(1L));
        News news = new News(newsId, "www", "www", "www", new Timestamp(1427369733), new Date(1427369733), author, tags, null);
        newsDao.edit(news);
        News actualNews = newsDao.get(newsId);
        assertEquals(actualNews.toString(), news.toString());
    }

    @Test
    public void testDeleteNews() throws Exception {
        Long newsId = 1L;
        newsDao.delete(newsId);
        News news = newsDao.get(newsId);
        assertNull(news);
    }

    @Test
    public void testGet() throws Exception {
        Long newsId = 1L;
        News actualNews = newsDao.get(newsId);
        assertNotNull(actualNews);
    }


    @Test
    public void testFilter() throws Exception {
        SearchCriteria searchCriteria = new SearchCriteria(1L, null);
        List<News> news = newsDao.findNewsByFilters(searchCriteria, 1, 4);
        assertEquals(news.size(), 1);
    }

    @Test
    public void testCountNews() throws Exception {
        SearchCriteria searchCriteria = new SearchCriteria(1L, null);
        int number = newsDao.countNumNews(searchCriteria);
        assertEquals(number, 1);
    }
}
