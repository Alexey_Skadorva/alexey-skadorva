package com.epam.newsmanagement.services.test;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.impl.NewsServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {
    private final static String TEXT = "text";
    @InjectMocks
    NewsServiceImpl newsService;
    @Mock
    NewsDao newsDao;
    @Mock
    News news;

    @Test
    public void testGetNewsMessage() throws ServiceException, DAOException {
        News news = new News(1L, null, null, null, null, null, null, null, null);
        when(newsDao.get(news.getNewsId())).thenReturn(news);
        newsService.getNews(news.getNewsId());
        verify(newsDao).get(news.getNewsId());
        verifyNoMoreInteractions(newsDao);
    }

    @Test
    public void testAddNews() throws ServiceException, DAOException {
        News news = new News(1L, null, null, null, null, null, null, null, null);
        when(newsDao.create(news)).thenReturn(news.getNewsId());
        newsService.addNews(news);
        verify(newsDao).create(news);
        verifyNoMoreInteractions(newsDao);
    }

    @Test
    public void testEditNews() throws ServiceException, DAOException {
        doNothing().when(newsDao).edit(news);
        newsService.editNews(news);
        verify(newsDao).edit(news);
        verifyNoMoreInteractions(newsDao);
    }


    @Test
    public void testDeleteNews() throws ServiceException, DAOException {
        List<Long> newsIds = new ArrayList<>();
        newsIds.add(1L);
        doNothing().when(newsDao).delete(newsIds.get(0));
        newsService.deleteNews(newsIds);
        verify(newsDao).delete(newsIds.get(0));
        verifyNoMoreInteractions(newsDao);
    }

    @Test
    public void testFindByFilters() throws ServiceException, DAOException {
        SearchCriteria searchCriteria = null;
        List<News> news = null;
        when(newsDao.findNewsByFilters(searchCriteria, 1, 1)).thenReturn(news);
        newsService.findByFilters(searchCriteria, 1, 1);
        verify(newsDao).findNewsByFilters(searchCriteria, 1, 1);
        verifyNoMoreInteractions(newsDao);
    }

    @Test
    public void testCountNumNews() throws ServiceException, DAOException {
        SearchCriteria searchCriteria = null;
        when(newsDao.countNumNews(searchCriteria)).thenReturn(1);
        newsService.countNumNews(searchCriteria);
        verify(newsDao).countNumNews(searchCriteria);
        verifyNoMoreInteractions(newsDao);
    }
}
