package com.epam.newsmanagement.services.test;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.impl.TagServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {
    private final static String TAG_NAME = "tag_name";
    @InjectMocks
    TagServiceImpl tagService;
    @Mock
    TagDao tagDao;
    @Mock
    Tag tag;

    @Test
    public void addTag() throws ServiceException, DAOException {
        Long id = 1L;
        when(tagDao.create(TAG_NAME)).thenReturn(id);
        tagService.addTag(TAG_NAME);
        verify(tagDao).create(TAG_NAME);
        verifyNoMoreInteractions(tagDao);
    }


    @Test
    public void testGetAllTags() throws ServiceException, DAOException {
        when(tagDao.getAllTags()).thenReturn(null);
        tagService.getAllTags();
        verify(tagDao).getAllTags();
        verifyNoMoreInteractions(tagDao);
    }

    @Test
    public void testEditTag() throws ServiceException, DAOException {
        doNothing().when(tagDao).edit(tag);
        tagService.editTag(tag);
        verify(tagDao).edit(tag);
        verifyNoMoreInteractions(tagDao);
    }

    @Test
    public void testDeleteTag() throws ServiceException, DAOException {
        Long tagId = 1L;
        doNothing().when(tagDao).delete(tagId);
        tagService.deleteTag(tagId);
        verify(tagDao).delete(tagId);
        verifyNoMoreInteractions(tagDao);
    }

    @Test
    public void testGetTagById() throws ServiceException, DAOException {
        Long tagId = 1L;
        when(tagDao.get(tagId)).thenReturn(null);
        tagService.getTag(tagId);
        verify(tagDao).get(tagId);
        verifyNoMoreInteractions(tagDao);
    }
}
