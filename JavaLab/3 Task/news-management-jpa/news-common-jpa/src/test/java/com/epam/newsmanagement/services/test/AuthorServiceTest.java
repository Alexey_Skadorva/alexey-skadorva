package com.epam.newsmanagement.services.test;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.impl.AuthorServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {
    private final static String TEXT = "author_name";
    @InjectMocks
    AuthorServiceImpl authorService;
    @Mock
    AuthorDao authorDao;
    @Mock
    Author author;

    @Test
    public void testGetAllAuthors() throws ServiceException, DAOException {
        when(authorDao.getAllAuthors()).thenReturn(null);
        authorService.getAllAuthors();
        verify(authorDao).getAllAuthors();
        verifyNoMoreInteractions(authorDao);
    }

    @Test
    public void testAddAuthor() throws ServiceException, DAOException {
        Author author = new Author(1L, TEXT, null);
        when(authorDao.create(author.getAuthorName())).thenReturn(author.getAuthorId());
        authorService.addAuthor(author.getAuthorName());
        verify(authorDao).create(author.getAuthorName());
        verifyNoMoreInteractions(authorDao);
    }

    @Test
    public void testEditAuthor() throws ServiceException, DAOException {
        doNothing().when(authorDao).edit(author);
        authorService.editAuthor(author);
        verify(authorDao).edit(author);
        verifyNoMoreInteractions(authorDao);
    }

    @Test
    public void testGetAuthorById() throws ServiceException, DAOException {
        Author author = new Author(1L, TEXT, null);
        when(authorDao.get(author.getAuthorId())).thenReturn(author);
        authorService.getAuthor(author.getAuthorId());
        verify(authorDao).get(author.getAuthorId());
        verifyNoMoreInteractions(authorDao);
    }
}
