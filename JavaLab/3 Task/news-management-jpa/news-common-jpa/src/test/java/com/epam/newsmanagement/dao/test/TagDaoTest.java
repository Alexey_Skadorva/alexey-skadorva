package com.epam.newsmanagement.dao.test;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-configuration-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:news.xml")
public class TagDaoTest {
    private final static String TAG_NAME = "text";
    @Autowired
    TagDao tagDao;

    public TagDaoTest() {
        super();
    }

    @Test
    public void testCreateTag() throws Exception {
        Long insertId = tagDao.create(TAG_NAME);
        Tag expectedTag = tagDao.get(insertId);
        Tag actualTag = new Tag(insertId, TAG_NAME, null);
        assertEquals(expectedTag, actualTag);
    }

    @Test
    public void testGetTag() throws Exception {
        Long tagId = 1L;
        Tag actualTag = tagDao.get(tagId);
        Tag expectedTag = new Tag(tagId, TAG_NAME, null);
        assertEquals(actualTag, expectedTag);
    }

    @Test
    public void testDeleteTag() throws Exception {
        Long tagId = 1L;
        tagDao.delete(tagId);
        Tag tag = tagDao.get(tagId);
        assertNull(tag);
    }

    @Test
    public void testGetAllTags() throws Exception {
        List<Tag> tags = tagDao.getAllTags();
        assertEquals(tags.size(), 1);
    }

    @Test
    public void testEditTag() throws Exception {
        long tagId = 1L;
        Tag tag = new Tag(tagId, TAG_NAME + TAG_NAME, null);
        tagDao.edit(tag);
        Tag actualTag = tagDao.get(tagId);
        assertEquals(actualTag, tag);
    }
}