/**
 * Create table News.Table contains fields such as
 * news_id(PK),short_text,full_text,title,
 * creation_date,modification_date.This table will
 *  store information about the news
 */
CREATE TABLE News (
  news_id NUMBER(20),
  short_text NVARCHAR2(100),
  full_text NVARCHAR2(2000),
  title NVARCHAR2(30),
  creation_date TIMESTAMP,
  modification_date DATE,
  CONSTRAINT not_null NOT NULL(news_id)) 
  CONSTRAINT not_null NOT NULL(short_text)) 
  CONSTRAINT not_null NOT NULL(full_text)) 
  CONSTRAINT not_null NOT NULL(title)) 
  CONSTRAINT not_null NOT NULL(creation_date)) 
  CONSTRAINT not_null NOT NULL(modification_date))
  constraint News_PK PRIMARY KEY (news_id)
);

/**
 * Create sequence for news_id
 */
CREATE sequence News_SEQ START WITH 100 INCREMENT BY  1;

/**
 * Create table News_Author.Table contains fields such as
 * news_id(FK) and author_id(FK).This table will store the 
 * relationship between the news and the author
 */
CREATE TABLE News_Author (
  news_id NUMBER(20),
  author_id NUMBER(20)
  CONSTRAINT not_null NOT NULL(news_id)) 
  CONSTRAINT not_null NOT NULL(author_id)) 
);

/**
 * Create table Author.Table contains fields such as
 * author_id(PK) and author_name.This table will 
 * store information about the author
 */
CREATE TABLE Author (
  author_id NUMBER(20),
  author_name NVARCHAR2(30),
  CONSTRAINT not_null NOT NULL(author_id)) 
  CONSTRAINT not_null NOT NULL(author_name)) 
  constraint Author_PK PRIMARY KEY (author_id)
);

/**
 * Create sequence for author_id
 */
CREATE sequence Author_SEQ  START WITH 100 INCREMENT BY  1;

/**
 * Create table Comments.Table contains fields such as
 * comment_id(PK),comment_text,creation_date,news_id(FK).
 * This table will store information about the comment
 */
CREATE TABLE Comments (
  comment_id NUMBER(20),
  comment_text NVARCHAR2(100),
  creation_date TIMESTAMP,
  news_id NUMBER(20),
  CONSTRAINT not_null NOT NULL(comment_id)) 
  CONSTRAINT not_null NOT NULL(comment_text)) 
  CONSTRAINT not_null NOT NULL(creation_date)) 
  CONSTRAINT not_null NOT NULL(news_id)) 
  constraint Comments_PK PRIMARY KEY (comment_id)
);

/**
 * Create sequence for comment_id
 */
CREATE sequence Comments_SEQ START WITH 100 INCREMENT BY  1;

/**
 * Create table Comments.Table contains fields such as
 * news_id(FK) and tag_id(FK).This table will store the 
 * relationship between the news and the tags
 */
CREATE TABLE News_Tag (
  news_id NUMBER(20),
  tag_id NUMBER(20)
  CONSTRAINT not_null NOT NULL(news_id)) 
  CONSTRAINT not_null NOT NULL(tag_id)) 
);

/**
 * Create table Tag.Table contains fields such as
 * tag_id(PK),tag_name. This table will store information 
 * about the tag
 */
CREATE TABLE Tag (
  tag_id NUMBER(20),
  tag_name NVARCHAR2(30),
  CONSTRAINT not_null NOT NULL(tag_id)) 
  CONSTRAINT not_null NOT NULL(tag_name)) 
  constraint Tag_PK PRIMARY KEY (tag_id)
);

/**
 * Create sequence for tag_id
 */
CREATE sequence Tag_SEQ START WITH 100 INCREMENT BY  1;

/**
 * Create table User.Table contains fields such as
 * user_id(PK),user_name,login,password,expired. This table will store information 
 * about the user
 */
CREATE TABLE User (
  user_id NUMBER(20),
  user_name NVARCHAR2(50),
  login NVARCHAR2(30),
  password NVARCHAR2(30),
  expired NVARCHAR2,
  CONSTRAINT not_null NOT NULL(user_id)) 
  CONSTRAINT not_null NOT NULL(user_name)) 
  CONSTRAINT not_null NOT NULL(login)) 
  CONSTRAINT not_null NOT NULL(password)) 
  CONSTRAINT not_null NOT NULL(expired)) 
  constraint User_PK PRIMARY KEY (user_id)
);

/**
 * Create sequence for user_id
 */
CREATE sequence User_SEQ START WITH 100 INCREMENT BY  1;

/**
  *Create table Roles.Table contains fields such as
 * user_id(PK),role_name. This table will store roles 
 * of users
 */
CREATE TABLE Roles (
  user_id NUMBER(20),
  role_name NVARCHAR2(50)
  CONSTRAINT not_null NOT NULL(user_id)) 
  CONSTRAINT not_null NOT NULL(role_name)) 
);

/**
 * Add all foreign keys
 */
ALTER TABLE News_Author ADD CONSTRAINT News_Author_fk1 FOREIGN KEY (news_id) REFERENCES News(news_id);

ALTER TABLE News_Author ADD CONSTRAINT News_Author_fk2 FOREIGN KEY (author_id) REFERENCES Author(author_id);

ALTER TABLE Comments ADD CONSTRAINT Comments_fk1 FOREIGN KEY (news_id) REFERENCES News(news_id);

ALTER TABLE News_Tag ADD CONSTRAINT News_Tag_fk1 FOREIGN KEY (news_id) REFERENCES News(news_id);

ALTER TABLE News_Tag ADD CONSTRAINT News_Tag_fk2 FOREIGN KEY (tag_id) REFERENCES Tag(tag_id);

ALTER TABLE Roles ADD CONSTRAINT Roles_fk1 FOREIGN KEY (user_id) REFERENCES User(user_id);
