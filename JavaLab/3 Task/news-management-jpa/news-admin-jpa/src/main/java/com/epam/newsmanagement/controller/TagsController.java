package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

import static com.epam.newsmanagement.utils.RequestMappingNames.*;

/**
 * The controller is designed to interface with comments.
 * In the controller there are methods of
 * removing, adding and changing tags.
 */

@Controller
public class TagsController {
    @Autowired
    TagService tagService;

    @RequestMapping(value = CHANGE_TAGS)
    public ModelAndView changeTags() throws ServiceException {
        ModelAndView mv = new ModelAndView(CHANGE_TAGS);
        List<Tag> tags = tagService.getAllTags();
        mv.addObject("tags", tags);
        return mv;
    }

    @RequestMapping(value = UPDATE_TAG)
    public ModelAndView updateTag(@RequestParam(value = "tagId", required = false) Long tagId,
                                  @RequestParam(value = "tagName", required = false) String tagName) throws ServiceException {
        Tag tag = new Tag(tagId, tagName, null);
        tagService.editTag(tag);
        return changeTags();
    }


    @RequestMapping(value = DELETE_TAG)
    public ModelAndView deleteTag(@PathVariable Long tagId) throws ServiceException {
        tagService.deleteTag(tagId);
        return changeTags();
    }

    @RequestMapping(value = ADD_TAG)
    public ModelAndView addTag(@RequestParam(value = "tagName", required = false) String tagName)
            throws ServiceException {
        tagService.addTag(tagName);
        return changeTags();
    }

    @ExceptionHandler(ServiceException.class)
    public ModelAndView serviceExceptionHandler() {
        return new ModelAndView(ERROR);
    }
}
