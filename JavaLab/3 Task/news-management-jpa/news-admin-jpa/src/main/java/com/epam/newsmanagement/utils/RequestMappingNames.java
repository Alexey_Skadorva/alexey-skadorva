package com.epam.newsmanagement.utils;

public class RequestMappingNames {
    public static final String PAGE = "/page{pageId}";

    public static final String MAIN = "main";
    public static final String ADD_COMMENT = "add_comment";
    public static final String DELETE_COMMENT = "delete_comment";

    public static final String CHANGE_TAGS = "change_tags";
    public static final String UPDATE_TAG = "update_tag";
    public static final String DELETE_TAG = "delete_tag/{tagId}";
    public static final String ADD_TAG = "add_tag";

    public static final String CHANGE_AUTHORS = "change_authors";
    public static final String UPDATE_AUTHOR = "update_author";
    public static final String EXPIRE_AUTHOR = "expire_author/{authorId}";
    public static final String ADD_AUTHOR = "add_author";

    public static final String NEWS = "news/{newsId}";
    public static final String NEWS_VIEW = "news";
    public static final String ADD_NEWS_VIEW = "change_news";
    public static final String ADD_NEWS = "add_news";
    public static final String ADD_NEWS_LINK = "add_news_link{newsId}";
    public static final String DELETE_NEWS = "delete_news";

    public static final String RESET_FILTER = "reset_filter";

    public static final String LOGIN = "login";
    public static final String ERROR = "error";
}
