package com.epam.newsmanagement.utils;

public class RequestMappingNames {
    public static final String PAGE = "page{pageId}";
    public static final String ADD_COMMENT = "add_comment";
    public static final String NEWS = "news/{newsId}";
    public static final String NEWS_VIEW = "news";
    public static final String MAIN = "main";
    public static final String RESET_FILTER = "reset_filter";
    public static final String ERROR = "error";
}
